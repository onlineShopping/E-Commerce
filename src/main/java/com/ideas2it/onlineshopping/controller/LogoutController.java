package com.ideas2it.onlineshopping.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.onlineshopping.common.Constant;

/**
 * <p>
 * Logout the user from the application.
 * </p>
 *
 * @author Ajaykkumar R
 */
@Controller
@RequestMapping(Constant.LOGOUT_CONTROLLER)
public class LogoutController extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * Close the session and logout the user.
     *
     * @param request - The request action from the user
     * @param response - The response to the given request from the user.
     */
    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView logoutPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final HttpSession session = request.getSession();
        Cookie loginCookie = null;
        final Cookie[] cookies = request.getCookies();
        if (null != cookies) {
            for (final Cookie cookie: cookies) {
                if (cookie.getName().equals(session.getAttribute(Constant.USERNAME))) {
                    loginCookie = cookie;
                    break;
                }
            }
        }
        if (null != loginCookie) {
            loginCookie.setMaxAge(Constant.ZERO);
            response.addCookie(loginCookie);
        }
        session.invalidate();
        return new ModelAndView(Constant.HOME); 
    }

    /**
     * <p>
     * Close the session and logout the user.
     * </p>
     *
     * @param request - The request action from the user
     * @param response - The response to the given request from the user.
     */
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView logout(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final HttpSession session = request.getSession();
        Cookie loginCookie = null;
        final Cookie[] cookies = request.getCookies();
        if (null != cookies) {
            for (final Cookie cookie: cookies) {
                if (cookie.getName().equals(session.getAttribute(Constant.USERNAME))) {
                    loginCookie = cookie;
                    break;
                }
            }
        }
        if (null != loginCookie) {
            loginCookie.setMaxAge(Constant.ZERO);
            response.addCookie(loginCookie);
        }
        session.invalidate();
        return new ModelAndView(Constant.HOME); 
    }
}
