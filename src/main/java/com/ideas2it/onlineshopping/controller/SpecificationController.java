package com.ideas2it.onlineshopping.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.logger.ApplicationLogger;
import com.ideas2it.onlineshopping.model.Specification;
import com.ideas2it.onlineshopping.service.SpecificationService;
import com.ideas2it.onlineshopping.model.Product;
import com.ideas2it.onlineshopping.model.ProductSeller;
import com.ideas2it.onlineshopping.model.Seller;
import com.ideas2it.onlineshopping.service.ProductService;
import com.ideas2it.onlineshopping.service.SellerService;

/**
 * Implements the Specification Management with the insert, retrieve and update
 * of the specification.
 *
 * @author Durgadevi Ananthakrishnan
 */
@Controller
@RequestMapping(Constant.SPECIFICATION_CONTROLLER)
public class SpecificationController {

    private final ApplicationLogger applicationLogger = new ApplicationLogger(SpecificationController.class.getName());

    @Autowired
    private SpecificationService specificationService;

    @Autowired
    private SellerService sellerService;
    

    /**
     * Create the specification by getting the request from which the specification
     * with the specification details like the id, brand name, model id, model name,
     * size, warranty, color and type with the product were obtained and were added
     * to the specification.
     *
     * @param specification
     *            - the specification with the details to be added to the
     *            specification.
     * @param bindingResult
     *            -Holds the result of the validation and binding and contains
     *            errors that may have occurred.
     * @return ModelAndView - redirects to the view with the added attributes.
     */
    @RequestMapping(params = Constant.CREATE, method=RequestMethod.POST)
    public ModelAndView createSpecification(@ModelAttribute(Constant.SPECIFICATION) final Specification specification,
            final BindingResult bindingResult) throws IOException {
        try {
            specificationService.addSpecification(specification);
            return getAllSpecifications();
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
        }
    }

    /**
     * Creates a new specification object for the empty form and stuffs it into the
     * model
     *
     * @model - the model, in which the new specification is added.
     */
    @ModelAttribute
    public void addSpecification(final Model model) {
        model.addAttribute(Constant.SPECIFICATION, new Specification());
    }

    /**
     * Get the specification by getting the request from which the id is obtained
     * and sending the response to the method depending on the request which may be
     * view or update.
     *
     * @param id
     *            - id of the specification, to retrieve the specification with the
     *            details.
     * @param methodName
     *            - method name, depending on it, redirects to the
     *            corresponding view.
     * @return ModelAndView - redirects to the view with the added attributes.
     * @throws IOException
     *             - throws if the exception caught while inserting the input.
     */
    @RequestMapping(params = Constant.GET, method=RequestMethod.GET)
    public ModelAndView getSpecificationById(@RequestParam(Constant.ID) final Long id,
            @RequestParam(Constant.METHOD_NAME) final String methodName) {
        try {
            final Specification specification = specificationService.getSpecificationById(id);
            if (methodName.equals(Constant.VIEW)) {
                return new ModelAndView(Constant.VIEW_SPECIFICATION_JSP, Constant.SPECIFICATION, specification);
            } else {
                return new ModelAndView(Constant.UPDATE_SPECIFICATION_JSP, Constant.SPECIFICATION, specification);
            }
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
        }
    }

    /**
     * Update the specification by getting the request from which the specification
     * with the specification details like the id, size, brand name, model id, model
     * name, color, type and warranty with the product were obtained and were
     * updated to the specification.
     *
     * @param specification
     *            - the specification with the details to be updated to the
     *            specification.
     * @param bindingResult
     *            -Holds the result of the validation and binding and contains
     *            errors that may have occurred.
     * @return ModelAndView - redirects to the view with the added attributes.
     * @throws IOException
     *             - throws if the exception caught while inserting the input.
     */
    @RequestMapping(params = Constant.UPDATE, method=RequestMethod.POST)
    public ModelAndView updateSpecification(@ModelAttribute(Constant.SPECIFICATION) final Specification specification,
            @RequestParam("sellerId") Long sellerId, final BindingResult bindingResult) throws IOException {
        ModelAndView modelAndView = new ModelAndView();
        try {
            specificationService.updateSpecification(specification);
            Seller seller = sellerService.getSellerById(sellerId);
            final List<ProductSeller> productSellers = seller.getProductSellers();
                final List<Product> products = new ArrayList<>();
                for(final ProductSeller productSeller : productSellers) {
                    final Product product = productSeller.getProduct();
                    products.add(product);
                }
                modelAndView.addObject(Constant.SELLER, seller);
                modelAndView.addObject(Constant.PRODUCTS, products);
                modelAndView.addObject(Constant.MESSAGE, "Specification updated successfully!");
                modelAndView.setViewName(Constant.VIEW_ALL_PRODUCTS_JSP);
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
        }
        return modelAndView;
    }

    /**
     * Get all the specifications by getting the request and all the specifications
     * with
     * their details were retrieved from the specification.
     *
     * @return ModelAndView - redirects to the view with the added attributes.
     */
    @RequestMapping(params = Constant.GET_ALL, method=RequestMethod.GET)
    private ModelAndView getAllSpecifications() {
        List<Specification> specifications = new ArrayList<>();
        try {
            specifications = specificationService.getAllSpecifications();
            return new ModelAndView(Constant.VIEW_ALL_SPECIFICATIONS_JSP, Constant.SPECIFICATIONS, specifications);
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
        }
    }
}
