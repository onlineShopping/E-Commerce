package com.ideas2it.onlineshopping.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.logger.ApplicationLogger;
import com.ideas2it.onlineshopping.model.Product;
import com.ideas2it.onlineshopping.model.ProductSeller;
import com.ideas2it.onlineshopping.model.Seller;
import com.ideas2it.onlineshopping.model.User;
import com.ideas2it.onlineshopping.service.ProductService;
import com.ideas2it.onlineshopping.service.SellerService;
import com.ideas2it.onlineshopping.service.UserService;

/**
 * <p>
 * Performs seller CRUD operations.
 * </p>
 *
 * @author Ajaykkumar R
 */
@Controller
@RequestMapping(Constant.SELLER_CONTROLLER)
public class SellerController {

    @Autowired
    private SellerService sellerService;

    @Autowired
    private UserService userService;

    @Autowired
    private ProductService productService;

    private final ApplicationLogger applicationLogger = new ApplicationLogger(SellerController.class.getName());

    /**
     * <p>
     * Creates a new user object for the empty form and stuffs it into the
     * model.
     * </p>
     *
     * @model
     *      The model, in which the new category is added.
     */
    @ModelAttribute
    public void addUser(final Model model) {
        model.addAttribute(Constant.USER, new User());
    }

    /**
     * <p>
     * Create the seller details and store it in the database.
     * </p>
     *
     * @param user
     *            Details of the user.
     * @param result
     *            Holds the result of the validation and binding and contains
     *            errors that may have occurred.
     * @return modelAndView
     *            Redirects to the corresponding page.
     */
    @RequestMapping(params = Constant.CREATE, method = RequestMethod.POST)
    private ModelAndView createSeller(@ModelAttribute(Constant.USER) final User user,
            final BindingResult result) {
        ModelAndView modelAndView = new ModelAndView();
        try {
            if (!userService.isValidInput(user)) {
                modelAndView = new ModelAndView("loginsignup", Constant.MESSAGE, Constant.VALID_INPUT);
            } else if (userService.isValidInput(user) && !userService.isDetailExists(user)) {
                modelAndView = new ModelAndView("loginsignup", Constant.MESSAGE, Constant.DETAIL_EXISTED);
            } else {
                sellerService.createSeller(user);
                modelAndView = new ModelAndView("loginsignup", Constant.MESSAGE, Constant.SUCCESS_LOGIN);
            }
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            modelAndView = new ModelAndView(Constant.ERROR);
        }
        return modelAndView;
    }

    /**
     * <p>
     * Update the details of the seller and store in the database.
     * </p>
     *
     * @param id
     *            Id of the seller.
     * @param name
     *            Name of the seller to be updated.
     * @param emailId
     *            Emailid of the seller to be updated.
     * @param phone
     *            Phone number of the seller to be updated.
     * @return modelAndView
     *            Redirects to the corresponding page.
     */
    @RequestMapping(params = Constant.UPDATE, method = RequestMethod.POST)
    private ModelAndView updateSeller(@RequestParam(Constant.ID) final Long id,
            @RequestParam(Constant.NAME) final String name, @RequestParam(Constant.EMAIL_ID) final String emailId,
            @RequestParam(Constant.PHONE_NUMBER) final Long phoneNumber) {
        ModelAndView modelAndView = new ModelAndView();
        try {
            sellerService.updateSeller(id, name, emailId, phoneNumber);
            final List<ProductSeller> productSellers = productService.getProductSellersBySellerId(id);
            final List<Product> products = new ArrayList<>();
            for(final ProductSeller productSeller : productSellers) {
                final Product product = productSeller.getProduct();
                products.add(product);
            }
            modelAndView.addObject(Constant.PRODUCTS, products);
            modelAndView.addObject(Constant.MESSAGE, Constant.SELLER_UPDATED);
            modelAndView.setViewName(Constant.VIEW_ALL_PRODUCTS_JSP);
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            modelAndView = new ModelAndView(Constant.ERROR);
        }
        return modelAndView;
    }

    /**
     * <p>
     * Display all the sellers stored in the database.
     * </p>
     *
     * @return modelAndView
     *         Redirects to the corresponding page.
     */
    @RequestMapping(params = Constant.DISPLAY, method = RequestMethod.GET)
    private ModelAndView displaySellers() {
        ModelAndView modelAndView = null;
        try {
            final List<Seller> sellers = sellerService.getAllSellers();
            modelAndView = new ModelAndView(Constant.DISPLAY_SELLER, Constant.SELLERS, sellers);
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            modelAndView = new ModelAndView(Constant.ERROR);
        }
        return modelAndView;
    }

    /**
     * <P>
     * Get the seller details from the database by id.
     * </p>
     *
     * @param id
     *            Id of the seller.
     * @param methodName
     *            Update or search.
     * @return modelAndView
     *            Redirects to the corresponding page.
     */
    @RequestMapping(params = Constant.GET, method = RequestMethod.GET)
    private ModelAndView getSeller(@RequestParam(Constant.ID) final Long id,
            @RequestParam(Constant.METHOD) final String methodName) {
        ModelAndView modelAndView = new ModelAndView();
        try {
            final Seller seller = sellerService.getSellerById(id);
            modelAndView.addObject(Constant.SELLER, seller);
            if (methodName.equals(Constant.METHOD_UPDATE)) {
                modelAndView.setViewName(Constant.UPDATE_SELLER);
            } else if (methodName.equals(Constant.METHOD_SEARCH)) {
                modelAndView.setViewName(Constant.SEARCH_SELLER);
            }
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            modelAndView = new ModelAndView(Constant.ERROR);
        }
        return modelAndView;
    }

    /**
     * <p>
     * Deactivate the seller stored in the database.
     * </p>
     *
     * @param id
     *            Id of the seller to be deactivated.
     * @return modelAndView
     *            Redirects to the corresponding page.
     */
    @RequestMapping(params = Constant.DELETE, method = RequestMethod.GET)
    private ModelAndView deleteSeller(@RequestParam(Constant.ID) final Long id) {
        ModelAndView modelAndView = null;
        try {
            sellerService.deActivateSeller(id);
            modelAndView = new ModelAndView(Constant.HOME, Constant.MESSAGE, Constant.SELLER_DELETED);
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            modelAndView = new ModelAndView(Constant.ERROR);
        }
        return modelAndView;
    }

    /**
     * <p>
     * Redirect to the corresponding jsp pages.
     * </p>
     *
     * @return modelAndView
     *            Redirects to the corresponding page.
     */
    @RequestMapping(params = Constant.REDIRECT, method = RequestMethod.GET)
    private ModelAndView redirectToViewPage(@RequestParam(Constant.NAME) final String name,
            final HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();
        try {
            if(name.equals(Constant.CREATE_PAGE)) {
                modelAndView.setViewName(Constant.CREATE_SELLER);
            } else if(name.equals(Constant.PRODUCT)) {
                final String sellerId = request.getParameter(Constant.ID);
                final List<ProductSeller> productSellers = productService.getProductSellersBySellerId(Long.parseLong(sellerId));
                final List<Product> products = new ArrayList<>();
                for(final ProductSeller productSeller : productSellers) {
                    final Product product = productSeller.getProduct();
                    products.add(product);
                }
                return new ModelAndView(Constant.VIEW_ALL_PRODUCTS_JSP, Constant.PRODUCTS, products);
            } else if(name.equals(Constant.METHOD_UPDATE)) {
                final String sellerId = request.getParameter(Constant.ID);
                final Seller seller = sellerService.getSellerById(Long.parseLong(sellerId));
                final User user = seller.getUser();
                return new ModelAndView(Constant.UPDATE_SELLER, Constant.USER, user);
            }
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            modelAndView = new ModelAndView(Constant.ERROR);
        }
        return modelAndView;
    }
}
