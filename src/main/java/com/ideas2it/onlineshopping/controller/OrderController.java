package com.ideas2it.onlineshopping.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.logger.ApplicationLogger;
import com.ideas2it.onlineshopping.model.Order;
import com.ideas2it.onlineshopping.service.OrderDetailService;
import com.ideas2it.onlineshopping.service.OrderService;

@Controller
@RequestMapping(Constant.ORDER_CONTROLLER)
public class OrderController {

    @Autowired
    private OrderDetailService orderDetailService;
    @Autowired
    private OrderService orderService;
    private final ApplicationLogger applicationLogger = new ApplicationLogger(OrderController.class.getName());

    /**
     * Implemented to insert orderDetail by requesting details from user interface details
     * like orderDetail, cart id, customer id from user interface
     *
     * @param orderDetail
     *            - OrderDetail contains details like id, amount, customer details
     * @param cartId
     *            - Id of cart to retrieve and insert in orderDetail
     * @parma shipmentId
     *        - Id of the shipment
     *  @parma customerId
     *        - Id of the customer
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    @RequestMapping(params = Constant.INSERT_ORDER, method=RequestMethod.POST)
    private ModelAndView insertOrderDetail(@RequestParam(Constant.CART_ID) final Long cartId,
            @RequestParam(Constant.SHIPMENT_ID) final Long shipmentId,
            @RequestParam(Constant.CUSTOMER_ID) final Long customerId,
            @RequestParam(Constant.PAYMENT_ID) final Long paymentId) {
        final ModelAndView modelAndView = new ModelAndView();
        try {
            orderDetailService.createOrderDetail(cartId, shipmentId, customerId, paymentId);
        } catch (final ApplicationException exception) {
            applicationLogger.error(exception.getMessage(), exception);
            return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
        }
        return modelAndView;
    }

    /**
     * Implemented to display order to user interface by fetching the
     * order
     * details corresponding to the customer
     *
     * @param customerId
     *            - id of customer to retrieve orderDetail detail
     * @return ModelAndView - redirects to the view with the added attributes.
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    @RequestMapping(params = Constant.DISPLAY_ORDER_DETAIL, method=RequestMethod.GET)
    private ModelAndView displayOrders(@RequestParam(Constant.CUSTOMER_ID) final Long customerId) {
        final ModelAndView modelAndView = new ModelAndView("myOrder");
        try {
            final List<Order> orders = orderService.getOrders(customerId);
            modelAndView.addObject(Constant.ORDERS, orders);
            modelAndView.setViewName("myOrder");
        } catch (final ApplicationException exception) {
            applicationLogger.error(exception.getMessage(), exception);
            return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
        }
        return modelAndView;
    }
}
