package com.ideas2it.onlineshopping.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.logger.ApplicationLogger;
import com.ideas2it.onlineshopping.model.Invoice;
import com.ideas2it.onlineshopping.service.InvoiceService;

/**
 * Implements the Invoice Management with the retrieve data of invoice.
 */
@Controller
@RequestMapping(Constant.INVOICE_CONTROLLER)
public class InvoiceController {

    @Autowired
    private InvoiceService invoiceService;
    private final ApplicationLogger applicationLogger = new ApplicationLogger(InvoiceController.class.getName());

    /**
     * Get the invoice by getting the invoice id from the request and sending
     * the response by passing the invoice to the view.
     *
     * @param id
     *            - id of the invoice, to retrieve the invoice with the details.
     * @return ModelAndView - redirects to the view with the added attributes.
     */
    @RequestMapping(params = Constant.GET_INVOICE, method = RequestMethod.GET)
    private ModelAndView displayInvoice(@RequestParam(Constant.ID) final Long id) {
        final ModelAndView modelAndView = new ModelAndView();
        try {
            final Invoice invoice = invoiceService.getInvoiceById(id);
        } catch (final ApplicationException exception) {
            applicationLogger.error(exception.getMessage(), exception);
            return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
        }
        return modelAndView;
    }
}
