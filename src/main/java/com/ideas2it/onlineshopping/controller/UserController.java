package com.ideas2it.onlineshopping.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.logger.ApplicationLogger;
import com.ideas2it.onlineshopping.model.Cart;
import com.ideas2it.onlineshopping.model.Customer;
import com.ideas2it.onlineshopping.model.Product;
import com.ideas2it.onlineshopping.model.ProductSeller;
import com.ideas2it.onlineshopping.model.Seller;
import com.ideas2it.onlineshopping.model.User;
import com.ideas2it.onlineshopping.service.CustomerService;
import com.ideas2it.onlineshopping.service.ProductService;
import com.ideas2it.onlineshopping.service.SellerService;
import com.ideas2it.onlineshopping.service.UserService;

/**
 * <p>
 * Authenticate the login credentials and forwards to the corresponding
 * operation
 * </p>
 *
 * @author Ajaykkumar R
 */
@Controller
@SessionAttributes(Constant.USER)
@RequestMapping(Constant.USER_CONTROLLER)
public class UserController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private SellerService sellerService;

    @Autowired
    private UserService userService;

    @Autowired
    private ProductService productService;

    private final ApplicationLogger applicationLogger = new ApplicationLogger(UserController.class.getName());

    /**
     * <p>
     * Update the password of the customer or seller.
     * </p>
     *
     * @param user
     *         Details of the user.
     * @return modelAndView
     *         Redirects to the corresponding page.
     */
    @RequestMapping(params = Constant.UPDATE, method = RequestMethod.POST)
    private ModelAndView updatePassword(@ModelAttribute final User user) {
        final ModelAndView modelAndView = new ModelAndView();
        try {
            final User userDetail = userService.updatePassword(user);
            if (null != userDetail) {
                modelAndView.addObject(Constant.MESSAGE, Constant.PASSWORD_UPDATED);
            } else {
                modelAndView.addObject(Constant.MESSAGE, Constant.PASSWORD_NOTUPDATED);
            }
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
        }
        return modelAndView;
    }

    /**
     * <p>
     * Autenticate the user and return response based on their role.
     * </p>
     *
     * @param userName
     *         Username given by the user.
     * @param password
     *         Password given by the user.
     * @param request
     *         The request action from the user.
     * @param response
     *         The response to the given request from the user.
     * @return modelAndView
     *         Redirects to the corresponding page.
     * @throws ServletException
     *         When servlet is failed to excute the particular operation.
     * @throws IOException
     *         Whenever an input or output operation is failed or interpreted.
     */
    @RequestMapping(params = Constant.LOGIN, method=RequestMethod.POST)
    private ModelAndView userLogin(@RequestParam(Constant.USERNAME) final String userName,
            @RequestParam(Constant.PASSWORD) final String password, final HttpServletRequest request, final HttpServletResponse response)
                    throws ServletException, IOException {
        ModelAndView modelAndView = new ModelAndView();
        try {
            final User user = userService.authenticateUser(userName, password);
            if (null != user) {
                final HttpSession session = request.getSession();
                session.setAttribute(Constant.USERNAME, user.getUserName());
                session.setAttribute(Constant.USERID, user.getId());
                final String role = user.getRole();
                if (role.equals(Constant.ADMIN)) {
                    modelAndView = new ModelAndView(Constant.HOME_ADMIN);
                } else if (role.equals(Constant.SELLER)) {
                    final Seller seller = user.getSeller();
                    session.setAttribute(Constant.SELLER_ID, seller.getId());
                    final List<ProductSeller> productSellers = seller.getProductSellers();
                    final List<Product> products = new ArrayList<>();
                    for(final ProductSeller productSeller : productSellers) {
                        final Product product = productSeller.getProduct();
                        products.add(product);
                    }
                    modelAndView.addObject(Constant.SELLER, seller);
                    modelAndView.addObject(Constant.PRODUCTS, products);
                    modelAndView.setViewName(Constant.VIEW_ALL_PRODUCTS_JSP);
                }
            } else {
                modelAndView = new ModelAndView("loginsignup", Constant.MESSAGE, Constant.INVALID_CREDENTIAL);
            }
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
        }
        return modelAndView;
    }

    /**
     * <p>
     * Autenticate the user and return response based on their role.
     * </p>
     *
     * @param userName
     *         Username given by the user.
     * @param password
     *         Password given by the user.
     * @param request
     *         The request action from the user.
     * @param response
     *         The response to the given request from the user.
     * @return modelAndView
     *         Redirects to the corresponding page.
     * @throws ServletException
     *         When servlet is failed to excute the particular operation.
     * @throws IOException
     *         Whenever an input or output operation is failed or interpreted.
     */
    @RequestMapping(params = "action=customerlogin", method=RequestMethod.POST)
    private ModelAndView customerLogin(@RequestParam(Constant.USERNAME) final String userName,
            @RequestParam(Constant.PASSWORD) final String password, final HttpServletRequest request, final HttpServletResponse response)
                    throws ServletException, IOException {
        ModelAndView modelAndView = new ModelAndView();
        try {
            final User user = userService.authenticateUser(userName, password);
            if (null != user) {
                final HttpSession session = request.getSession();
                session.setAttribute(Constant.USERNAME, user.getUserName());
                session.setAttribute(Constant.USERID, user.getId());
                final Customer customer = customerService.getCustomerByUserId(user.getId());
                session.setAttribute(Constant.CUSTOMER_ID, customer.getId());
                if (null != customer.getCart()) {
                    final Cart cart = customer.getCart();
                    final Long id = cart.getId();
                    session.setAttribute(Constant.CART_ID, id);
                }
                modelAndView = new ModelAndView(Constant.HOME);
            } else {
                modelAndView = new ModelAndView(Constant.HOME, Constant.MESSAGE, Constant.INVALID_CREDENTIAL);
            }
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
        }
        return modelAndView;
    }


    /**
     * <p>
     * Deactivate user by user id.
     * </p>
     *
     * @param id
     *        Id of the user.
     * @return modelAndView
     *        Redirects to the corresponding page.
     */
    @RequestMapping(params = Constant.DELETE, method = RequestMethod.GET)
    private ModelAndView deleteUser(@RequestParam(Constant.ID) final Long id) {
        ModelAndView modelAndView = new ModelAndView();
        try {
            userService.deActivateUser(id);
            modelAndView = displayAllUsers();
        } catch(final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            modelAndView = new ModelAndView(Constant.ERROR);
        }
        return modelAndView;
    }

    /**
     * <p>
     * Get the user details by emailid.
     * </p>
     *
     * @param emailId
     *         Email id of the user.
     * @return modelAndView
     *         Redirects to the corresponding page.
     */
    @RequestMapping(params = Constant.GET, method = RequestMethod.GET)
    private ModelAndView getUser(@RequestParam(Constant.EMAIL_ID) final String emailId) {
        final ModelAndView modelAndView = null;
        try {
            final User user = userService.getUserDetailByEmail(emailId);
            modelAndView.addObject(Constant.USER, user);
            modelAndView.setViewName(Constant.HOME);
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
        }
        return modelAndView;
    }

    /**
     * <p>
     * Get all users from the database.
     * </p>
     *
     * @return modelAndView
     *         Redirects to the corresponding page.
     */
    @RequestMapping(value = "/display", method = RequestMethod.GET)
    private ModelAndView displayAllUsers() {
        final ModelAndView modelAndView = new ModelAndView();
        try {
            final List<User> users = userService.getAllUsers();
            modelAndView.addObject(Constant.USERS, users);
            modelAndView.setViewName(Constant.VIEW_ALL_USERS);
        } catch(final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
        }
        return modelAndView;
    }

    /**
     * Redirects to the corresponding page depending on the form name.
     *
     * @param name - represents the method depend on that it redirects to
     *                   the corresponding page.
     * @return ModelAndView - redirects to the view with the added attributes.
     */
    @RequestMapping(params = Constant.REDIRECT, method=RequestMethod.GET)
    public ModelAndView redirectToView(@RequestParam(Constant.NAME) final String name) {
        switch (name) {
            case Constant.CASE_CREATE :
                return new ModelAndView(Constant.CREATE_SELLER);
            case Constant.HOME :
                return new ModelAndView(Constant.HOME);
        }
        return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
    }
}
