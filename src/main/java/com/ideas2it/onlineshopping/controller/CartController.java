package com.ideas2it.onlineshopping.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.logger.ApplicationLogger;
import com.ideas2it.onlineshopping.model.Cart;
import com.ideas2it.onlineshopping.model.CartDetail;
import com.ideas2it.onlineshopping.model.Customer;
import com.ideas2it.onlineshopping.model.Product;
import com.ideas2it.onlineshopping.service.CartDetailService;
import com.ideas2it.onlineshopping.service.CartService;
import com.ideas2it.onlineshopping.service.CustomerService;
import com.ideas2it.onlineshopping.service.ProductService;

/**
 * Implements the Cart Management with the insert, delete, retrieve
 * and update of the cart.
 */
@Controller
@RequestMapping(Constant.CART_CONTROLLER)
public class CartController {

    @Autowired
    private CartService cartService;
    @Autowired
    private CartDetailService cartDetailService;
    @Autowired
    private ProductService productService;
    @Autowired
    private CustomerService customerService;

    private final ApplicationLogger applicationLogger = new ApplicationLogger(CartController.class.getName());

    /**
     * Create cart by getting the request data from the user interface with details
     * like cart, customer id, cartDetail id.
     *
     * @param cart
     *            - cart contains details like id, date, customer, cartDetail and
     *            order
     * @param customerId
     *            - Id of customer.
     * @param productId
     *            - Id of product.
     * @return ModelAndView
     *         - Redirects to the view with the added attributes.
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    @RequestMapping(params = Constant.INSERT_CART, method = RequestMethod.POST)
    private ModelAndView insertCart(@RequestParam(Constant.CART_ID) final Long cartId,
            @RequestParam(Constant.CUSTOMER_ID) final Long customerId,
            @RequestParam(Constant.PRODUCT_ID) final Long productId) {
        final ModelAndView modelAndView = new ModelAndView();
        Cart cart = new Cart();
        try {
            if (null == cartId) {
                cartService.createCart(productId, customerId);
                final Cart cartdetail = cartService.getCartByCustomer(customerId);
                modelAndView.addObject(Constant.CART_ID, cartdetail.getId());
            } else {
                cart = cartService.getCart(cartId);
                final CartDetail cartDetail = cartDetailService.getCartDetail(cartId, productId);
                if(null != cartDetail) {
                    final int quantity = cartDetail.getQuantity();
                    cartDetailService.updateCartDetail(cartId, productId, quantity+1);
                } else {
                    final Product product = productService.getProductById(productId, Boolean.TRUE);
                    final Customer customer = customerService.getCustomerById(customerId);
                    cartService.updateCart(cart, product, customer);
                }
            }
            final Cart cartDetail = cartService.getCartByCustomer(customerId);
            modelAndView.addObject(Constant.CART, cartDetail);
            final List<CartDetail> cartDetails = cartDetailService.getCartDetailByCartId(cartDetail.getId());
            modelAndView.addObject(Constant.CART_DETAILS, cartDetails);
            modelAndView.setViewName(Constant.CART_JSP);
        } catch (final ApplicationException exception) {
            applicationLogger.error(exception.getMessage(), exception.getCause());
            // return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
        }
        return modelAndView;
    }

    /**
     * Display cart to the user by fetching the cart details corresponding to the
     * customer
     *
     * @param customerId
     *            - Id of customer.
     * @return ModelAndView
     *         - Redirects to the view with the added attributes.
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    @RequestMapping(params = Constant.DISPLAY_CART, method = RequestMethod.GET)
    private ModelAndView displayCart(@RequestParam(Constant.CUSTOMER_ID) final Long customerId) {
        final ModelAndView modelAndView = new ModelAndView();
        try {
            final Cart cart = cartService.getCartByCustomer(customerId);
            if (null != cart) {
                modelAndView.addObject(Constant.CART, cart);
                final List<CartDetail> cartDetails = cart.getCartDetails();
                modelAndView.addObject(Constant.CART_DETAILS, cartDetails);
            }
            modelAndView.setViewName(Constant.CART_JSP);
        } catch (final ApplicationException exception) {
            applicationLogger.error(exception.getMessage(), exception.getCause());
            return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
        }
        return modelAndView;
    }

    /**
     * Implemented to delete cart after checking out the cart by user from user
     * interface
     *
     * @param cartId
     *            - Id of the cart.
     * @return ModelAndView
     *         - Redirects to the view with the added attributes.
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    @RequestMapping(params = Constant.DELETE_CART, method = RequestMethod.DELETE)
    private ModelAndView deleteCart(@RequestParam(Constant.CART_ID) final Long cartId) {
        final ModelAndView modelAndView = new ModelAndView();
        try {
            cartService.deleteCart(cartId);
            // TODO direct to a jsp page after operation.
        } catch (final ApplicationException exception) {
            applicationLogger.error(exception.getMessage(), exception.getCause());
            return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
        }
        return modelAndView;
    }

    /**
     * Implemented to updated the quantity of the customer's product from the cart
     *
     * @param productId
     *            - Id of the product.
     * @param cartId
     *            - Id of the cart.
     * @param quantity
     *            - Value of quantity is to be updated
     * @return ModelAndView
     *         - Redirects to the view with the added attributes.
     */
    @RequestMapping(params = Constant.UPDATE_CART_DETAIL, method = RequestMethod.GET)
    private ModelAndView updateQuantity(@RequestParam(Constant.PRODUCT_ID) final Long productId,
            @RequestParam(Constant.CART_ID) final Long cartId, @RequestParam(Constant.QUANTITY) final int quantity) {
        final ModelAndView modelAndView = new ModelAndView();
        try {
            cartDetailService.updateCartDetail(cartId, productId, quantity);
            final Cart cart = cartService.getCart(cartId);
            final List<CartDetail> cartDetails = cart.getCartDetails();
            modelAndView.addObject(Constant.CART_DETAILS, cartDetails);
            modelAndView.setViewName(Constant.CART_JSP);
        } catch (final ApplicationException exception) {
            applicationLogger.error(exception.getMessage(), exception.getCause());
            return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
        }
        return modelAndView;
    }

    /**
     * Implemented to updated the quantity of the customer's product from the cart
     *
     * @param productId
     *            - Id of the product.
     * @param cartId
     *            - Id of the cart.
     * @param isActive
     *            - Value of quantity is to be updated
     * @return ModelAndView
     *         - Redirects to the view with the added attributes.
     */
    @RequestMapping(params = Constant.DELETE_CART_DETAIL, method = RequestMethod.GET)
    private ModelAndView deleteCartDetail(@RequestParam(Constant.PRODUCT_ID) final Long productId,
            @RequestParam(Constant.CART_ID) final Long cartId) {
        final ModelAndView modelAndView = new ModelAndView();
        try {
            cartDetailService.deleteCartDetail(cartId, productId);
            final List<CartDetail> cartDetails = cartDetailService.getCartDetailByCartId(cartId);
            modelAndView.addObject(Constant.CART_DETAILS, cartDetails);
            modelAndView.setViewName(Constant.CART_JSP);
        } catch (final ApplicationException exception) {
            applicationLogger.error(exception.getMessage(), exception.getCause());
            return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
        }
        return modelAndView;
    }

    /**
     * Display cart to the user for order summary by fetching the cart details
     * corresponding to the
     * customer
     *
     * @param customerId
     *            - Id of customer.
     * @return ModelAndView
     *         - Redirects to the view with the added attributes.
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    @RequestMapping(params = Constant.ORDER_SUMMARY, method = RequestMethod.GET)
    private ModelAndView displayCartForOrderSummary(@RequestParam(Constant.CUSTOMER_ID) final Long customerId) {
        final ModelAndView modelAndView = new ModelAndView();
        try {
            final Cart cart = cartService.getCartByCustomer(customerId);
            final List<CartDetail> cartDetails = cartDetailService.getCartDetailByCartId(cart.getId());
            modelAndView.addObject(Constant.CART_DETAILS, cartDetails);
            modelAndView.setViewName(Constant.ORDER_SUMMARY_JSP);
        } catch (final ApplicationException exception) {
            applicationLogger.error(exception.getMessage(), exception.getCause());
            return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
        }
        return modelAndView;
    }
}
