package com.ideas2it.onlineshopping.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.logger.ApplicationLogger;
import com.ideas2it.onlineshopping.model.Address;
import com.ideas2it.onlineshopping.model.Customer;
import com.ideas2it.onlineshopping.model.Seller;
import com.ideas2it.onlineshopping.service.AddressService;
import com.ideas2it.onlineshopping.service.CustomerService;
import com.ideas2it.onlineshopping.service.SellerService;

/**
 * <p>
 * Performs CRUD operations on address of the seller and customer.
 * </p>
 *
 * @author Ajaykkumar.R
 */
@Controller
@RequestMapping(Constant.ADDRESS_CONTROLLER)
public class AddressController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private SellerService sellerService;

    @Autowired
    private AddressService addressService;

    private final ApplicationLogger applicationLogger = new ApplicationLogger(AddressController.class.getName());

    /**
     * <p>
     * Create the address details and store in the database.
     * </p>
     *
     * @param address
     *            Address details given by the user.
     * @param personId
     *            Id of the customer or seller.
     * @param person
     *            Seller or customer.
     * @return modelAndView
     *         Redirects to the corresponding page.
     */
    @RequestMapping(params = Constant.CREATE, method=RequestMethod.POST)
    private ModelAndView createAddress(@ModelAttribute final Address address,
            @RequestParam(Constant.PERSON_ID) final Long id, @RequestParam(Constant.ROLE) final String role) {
        final ModelAndView modelAndView = new ModelAndView();
        try {
            if (role.equals(Constant.SELLER)) {
                final Seller seller = sellerService.getSellerById(id);
                address.setSeller(seller);
                addressService.createAddress(address);
            } else if (role.equals(Constant.CUSTOMER)) {
                final Customer customer = customerService.getCustomerById(id);
                address.setCustomer(customer);
                addressService.createAddress(address);
                // final customerDetail = customerService.getCustomerById(id);
                final List<Address> addresses = customer.getAddresses();
                modelAndView.addObject(Constant.ADDRESSES, addresses);
                //modelAndView.addObject(Constant.TOTAL_AMOUNT, totalAmount);
                modelAndView.setViewName(Constant.SHIPMENT_JSP);
            }

            // TODO: Attribute will be added whiling designing JSP.
            //modelAndView = new ModelAndView(Constant.MESSAGE, Constant.SUCCESS);
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            // TODO: Attribute will be added whiling designing JSP.
            //modelAndView = new ModelAndView(Constant.MESSAGE, Constant.FAILED);
        }
        return modelAndView;
    }

    /**
     * <p>
     * Update the address details stored in the database.
     * </p>
     *
     * @param address
     *            Address detail to be updated.
     * @return modelAndView
     *         Redirects to the corresponding page.
     */
    @RequestMapping(params = Constant.UPDATE, method = RequestMethod.PUT)
    private ModelAndView updateAddress(@ModelAttribute final Address address) {
        final ModelAndView modelAndView = null;
        try {
            addressService.updateAddress(address);
            // TODO: Attribute will be added whiling designing JSP.
            //modelAndView = new ModelAndView(Constant.MESSAGE, Constant.SUCCESS);
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            // TODO: Attribute will be added whiling designing JSP.
            //modelAndView = new ModelAndView(Constant.MESSAGE, Constant.FAILED);
        }
        return modelAndView;
    }

    /**
     * <p>
     * Delete the address detail stored in the database.
     * </p>
     *
     * @param addressId
     *            Address id to be deleted.
     * @return modelAndView
     *         Redirects to the corresponding page.
     */
    @RequestMapping(params = Constant.DELETE, method = RequestMethod.DELETE)
    private ModelAndView deleteAddress(@RequestParam(Constant.ADDRESS_ID) final Long id) {
        final ModelAndView modelAndView = null;
        try {
            addressService.deActivateAddress(id);
            // TODO: Attribute will be added whiling designing JSP.
            //modelAndView = new ModelAndView(Constant.MESSAGE, Constant.SUCCESS);
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            // TODO: Attribute will be added whiling designing JSP.
            //modelAndView = new ModelAndView(Constant.MESSAGE, Constant.FAILED);
        }
        return modelAndView;
    }

    /**
     * <p>
     * Redirect the request to the create address page.
     * </p>
     *
     * @param id
     *            Id of the seller or customer.
     * @param person
     *            Seller or customer.
     * @return modelAndView
     *         Redirects to the corresponding page.
     */
    @RequestMapping(params = Constant.REDIRECTCREATE, method = RequestMethod.GET)
    private ModelAndView redirectToAddressCreatePage(@RequestParam(Constant.ID) final Long id,
            @RequestParam(Constant.ROLE) final String role) {
        // TODO: Attribute will be added whiling designing JSP.
        final ModelAndView modelAndView = new ModelAndView();
        if (role.equals(Constant.SELLER)) {
            modelAndView.addObject(Constant.SELLER_ID, id);
        } else if (role.equals(Constant.CUSTOMER)) {
            modelAndView.addObject(Constant.CUSTOMER_ID, id);
        }
        modelAndView.addObject(Constant.ROLE, role);
        return modelAndView;
    }

    /**
     * <p>
     * Redirect the request to the update address page.
     * </p>
     *
     * @param addressId
     *            Address id to be updated.
     * @return modelAndView
     *         Redirects to the corresponding page.
     */
    @RequestMapping(params = Constant.REDIRECTUPDATE, method = RequestMethod.GET)
    private ModelAndView redirectToAddressUpdatePage(@RequestParam(Constant.ADDRESS_ID) final Long id) {
        ModelAndView modelAndView = null;
        try {
            final Address address = addressService.getAddressById(id);
            // TODO: Attribute will be added whiling designing JSP.
            modelAndView = new ModelAndView();
            modelAndView.addObject(Constant.ADDRESS, address);
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            // TODO: Attribute will be added whiling designing JSP.
            //modelAndView = new ModelAndView(Constant.MESSAGE, Constant.FAILED);
        }
        return modelAndView;
    }
}
