package com.ideas2it.onlineshopping.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.logger.ApplicationLogger;
import com.ideas2it.onlineshopping.model.Category;
import com.ideas2it.onlineshopping.model.Product;
import com.ideas2it.onlineshopping.model.SubCategory;
import com.ideas2it.onlineshopping.service.CategoryService;
import com.ideas2it.onlineshopping.service.SubCategoryService;

/**
 * Implements the Sub Category Management with the insert, in activate,
 * retrieve and update of the sub category.
 *
 * @author Durgadevi Ananthakrishnan
 */
@Controller
@RequestMapping(Constant.SUB_CATEGORY_CONTROLLER)
public class SubCategoryController {

    private final ApplicationLogger applicationLogger = new ApplicationLogger(SubCategoryController.class.getName());

    @Autowired
    private SubCategoryService subCategoryService;

    @Autowired
    private CategoryService categoryService;

    /**
     * Redirects to the corresponding page depending on the form name.
     *
     * @param formName
     *            - represents the method depend on that it redirects to
     *            the corresponding page.
     * @param name
     *            - sub category name to search.
     * @return ModelAndView - redirects to the view with the added attributes.
     */
    @RequestMapping(params = Constant.REDIRECT, method = RequestMethod.GET)
    public ModelAndView redirectToView(@RequestParam(Constant.FORMNAME) final String formName,
            @RequestParam(Constant.NAME) final String name) {
        switch (formName) {
            case Constant.CASE_CREATE:
                try {
                    final List<Category> categories = categoryService.getAllCategories(Boolean.TRUE);
                    return new ModelAndView(Constant.CREATE_SUBCATEGORY, Constant.CATEGORIES, categories);
                } catch (final ApplicationException e) {
                    applicationLogger.error(e.getMessage(), e.getCause());
                    return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
                }
            case Constant.CASE_DISPLAY:
                try {
                    final SubCategory subCategory = subCategoryService.getSubCategoryByName(name);
                    final List<Product> products = subCategory.getProducts();
                    return new ModelAndView(Constant.DISPLAY_PRODUCTS, Constant.PRODUCTS, products);
                } catch (final ApplicationException e) {
                    applicationLogger.error(e.getMessage(), e.getCause());
                    return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
                }
            case Constant.CASE_REDIRECT:
                return new ModelAndView(Constant.YET_TO_BE_DESIGNED);

        }
        return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
    }

    /**
     * Create the subCategory by getting the request from which the details of the
     * subCategory were obtained and added to the subCategory.
     *
     * @param subCategory-
     *            sub category, contains the sub category with the details like id,
     *            name and description with the sub categories to be added.
     * @param bindingResult
     *            -Holds the result of the validation and binding and contains
     *            errors that may have occurred.
     * @param isNeededAll
     *            - true if all the sub categories are needed else false.
     * @return ModelAndView - redirects to the view with the added attributes.
     */
    @RequestMapping(params = Constant.CREATE, method = RequestMethod.POST)
    private ModelAndView createSubCategory(@ModelAttribute(Constant.SUB_CATEGORY) final SubCategory subCategory,
            final BindingResult bindingResult, @RequestParam(Constant.IS_NEEDED_ALL) final boolean isNeededAll,
            @RequestParam(Constant.CATEGORY_ID) final Long categoryId) {
        try {
            if (subCategoryService.isValidSubCategory(subCategory)) {
                final Category category = categoryService.getCategoryById(categoryId, Boolean.FALSE);
                subCategory.setCategory(category);
                subCategoryService.addSubCategory(subCategory);
                return getAllSubCategories(isNeededAll);
            } else {
                return new ModelAndView(Constant.CREATE_SUBCATEGORY, Constant.MESSAGE, Constant.INVALID);
            }
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            return new ModelAndView(Constant.ERROR);
        }
    }

    /**
     * Creates a new sub category object for the empty form and stuffs it into the
     * model
     *
     * @model - the model, in which the new sub category is added.
     */
    @ModelAttribute
    public void addSubCategory(final Model model) {
        model.addAttribute(Constant.SUB_CATEGORY, new SubCategory());
    }

    /**
     * Get the sub category by getting the sub category id from the request and
     * sending
     * the response to the method depending on the request which may be view or
     * update.
     *
     * @param id
     *            - id of the sub category, to retrieve the sub category with the
     *            details.
     * @param methodName
     *            - method name, depending on it, redirects to the
     *            corresponding view.
     * @param isNeededActive
     *            - true if active sub category is needed else false.
     * @return ModelAndView - redirects to the view with the added attributes.
     */
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    private ModelAndView getSubCategory(@RequestParam(Constant.ID) final Long id,
            @RequestParam(Constant.METHOD_NAME) final String methodName,
            @RequestParam(Constant.IS_NEEDED_ACTIVE) final boolean isNeededActive) {
        try {
            final SubCategory subCategory = subCategoryService.getSubCategoryById(id, isNeededActive);
            if (methodName.equals(Constant.VIEW)) {
                return new ModelAndView(Constant.VIEW_SUB_CATEGORY_JSP, Constant.SUB_CATEGORY, subCategory);
            } else {
                return new ModelAndView(Constant.UPDATE_SUB_CATEGORY_JSP, Constant.SUB_CATEGORY, subCategory);
            }
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
        }
    }

    /**
     * Update the sub category by getting the request from which the updated
     * parameters of the sub category were obtained and updated to the sub category.
     *
     * @param id
     *            - id of the sub category, by which the sub category is retrieved
     *            to update.
     * @param name
     *            - name of the sub category to be updated.
     * @param description
     *            - description of the sub category to be updated.
     * @param isNeededActive
     *            - true if active sub category is needed else false.
     * @return ModelAndView - redirects to the view with the added attributes.
     */
    @RequestMapping(params = Constant.UPDATE, method = RequestMethod.POST)
    private ModelAndView updateSubCategory(@ModelAttribute(Constant.SUB_CATEGORY) final SubCategory subCategory,
            @RequestParam(Constant.IS_NEEDED_ACTIVE) final boolean isNeededActive,
            @RequestParam(Constant.IS_NEEDED_ALL) final boolean isNeededAll) {
        try {
            subCategoryService.updateSubCategory(subCategory, isNeededActive);
            return getAllSubCategories(isNeededAll);
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
        }
    }

    /**
     * Activate the sub category by getting the request from which the id of the sub
     * category to be activated is obtained and updated to the sub category.
     *
     * @param id
     *            - id of the sub category by which the category will be retrieved
     *            to activate.
     * @return ModelAndView - redirects to the view with the added attributes.
     */
    @RequestMapping(params = Constant.ACTIVATE, method = RequestMethod.GET)
    private ModelAndView activateSubCategory(@RequestParam(Constant.ID) final Long id) {
        final ModelAndView modelAndView = new ModelAndView();
        try {
            subCategoryService.activateSubCategory(id);
            final List<SubCategory> subCategories = subCategoryService.getAllSubCategories(Boolean.TRUE);
            modelAndView.addObject(Constant.SUB_CATEGORIES, subCategories);
            modelAndView.addObject(Constant.MESSAGE, Constant.SUCCESS);
            modelAndView.setViewName(Constant.VIEW_ALL_SUB_CATEGORIES_JSP);
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            return new ModelAndView(Constant.VIEW_ALL_SUB_CATEGORIES_JSP, Constant.MESSAGE, Constant.FAILED);
        }
        return modelAndView;
    }

    /**
     * Deactivate the sub category by getting the request from which the id of the
     * sub
     * category to be deactivated is obtained and updated to the sub category.
     *
     * @param id
     *            - id of the sub category by which the category will be retrieved
     *            to deactivate.
     * @return ModelAndView - redirects to the view with the added attributes.
     */
    @RequestMapping(params = Constant.IN_ACTIVATE, method = RequestMethod.GET)
    private ModelAndView deactivateSubCategory(@RequestParam(Constant.ID) final Long id) {
        final ModelAndView modelAndView = new ModelAndView();
        try {
            subCategoryService.deactivateSubCategory(id);
            final List<SubCategory> subCategories = subCategoryService.getAllSubCategories(Boolean.TRUE);
            modelAndView.addObject(Constant.SUB_CATEGORIES, subCategories);
            modelAndView.addObject(Constant.MESSAGE, Constant.SUCCESS);
            modelAndView.setViewName(Constant.VIEW_ALL_SUB_CATEGORIES_JSP);
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            return new ModelAndView(Constant.VIEW_ALL_SUB_CATEGORIES_JSP, Constant.MESSAGE, Constant.FAILED);
        }
        return modelAndView;
    }

    /**
     * Get all the categories by getting the request and all the categories with
     * their details were retrieved from the subCategory.
     *
     * @param isNeededAll
     *            - true if all the categories are needed else false.
     * @return ModelAndView - redirects to the view with the added attributes.
     */
    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    private ModelAndView getAllSubCategories(@RequestParam(Constant.IS_NEEDED_ALL) final boolean isNeededAll) {
        List<SubCategory> subCategories = new ArrayList<>();
        try {
            subCategories = subCategoryService.getAllSubCategories(isNeededAll);
            return new ModelAndView(Constant.VIEW_ALL_SUB_CATEGORIES_JSP, Constant.SUB_CATEGORIES, subCategories);
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
        }
    }

}
