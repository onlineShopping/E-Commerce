package com.ideas2it.onlineshopping.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.logger.ApplicationLogger;
import com.ideas2it.onlineshopping.model.Cart;
import com.ideas2it.onlineshopping.model.Customer;
import com.ideas2it.onlineshopping.model.User;
import com.ideas2it.onlineshopping.service.CustomerService;
import com.ideas2it.onlineshopping.service.UserService;

/**
 * <p>
 * Performs customer CRUD operations.
 * </p>
 *
 * @author Ajaykkumar R
 */
@Controller
@RequestMapping(Constant.CUSTOMER_CONTROLLER)
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private UserService userService;

    private final ApplicationLogger applicationLogger = new ApplicationLogger(CustomerController.class.getName());

    /**
     * <p>
     * Creates a new user object for the empty form and stuffs it into the
     * model.
     * </p>
     *
     * @model
     *      The model, in which the new category is added.
     */
    @ModelAttribute
    public void addUser(final Model model) {
        model.addAttribute(Constant.USER, new User());
    }

    /**
     * <p>
     * Create the customer details and store it in the database.
     * </p>
     *
     * @param user
     *        Details of the user.
     * @param address
     *        Address details of the customer given by the user.
     * @param result
     *        Holds the result of the validation and binding and contains
     *        errors that may have occurred.
     * @return modelAndView
     *        Redirects to the corresponding page.
     */
    @RequestMapping(params = Constant.CREATE, method = RequestMethod.POST)
    private ModelAndView createCustomer(@ModelAttribute(Constant.USER) final User user,
            final BindingResult result, final HttpServletRequest request, final HttpServletResponse response)
                    throws ServletException, IOException {
        ModelAndView modelAndView = new ModelAndView();
        try {
            if(!userService.isValidInput(user)) {
                modelAndView = new ModelAndView(Constant.HOME, Constant.MESSAGE, Constant.VALID_INPUT);
            } else if(userService.isValidInput(user) && !userService.isDetailExists(user)) {
                modelAndView = new ModelAndView(Constant.HOME, Constant.MESSAGE, Constant.DETAIL_EXISTED);
            } else {
                customerService.createCustomer(user);
                final HttpSession session = request.getSession();
                session.setAttribute(Constant.USERNAME, user.getUserName());
                session.setAttribute(Constant.USERID, user.getId());
                final Customer customer = customerService.getCustomerByUserId(user.getId());
                session.setAttribute(Constant.CUSTOMER_ID, customer.getId());
                if (null != customer.getCart()) {
                    final Cart cart = customer.getCart();
                    final Long id = cart.getId();
                    session.setAttribute(Constant.CART_ID, id);
                }
                modelAndView = new ModelAndView(Constant.HOME);
            }
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            modelAndView = new ModelAndView(Constant.ERROR);
        }
        return modelAndView;
    }

    /**
     * <p>
     * Update the details of the customer and store it in the database.
     * </p>
     *
     * @param id
     *        Id of the customer.
     * @param name
     *        Name of the customer to be updated.
     * @param emailId
     *        Emailid of the customer to be updated.
     * @param phone
     *        Phone number of the customer to be updated.
     * @return modelAndView
     *        Redirects to the corresponding page.
     */
    @RequestMapping(params = Constant.UPDATE, method = RequestMethod.PUT)
    private ModelAndView updateCustomer(@RequestParam(Constant.ID) final Long id, @RequestParam(Constant.NAME) final String name,
            @RequestParam(Constant.EMAIL_ID) final String emailId, @RequestParam(Constant.PHONE_NUMBER) final Long phoneNumber) {
        ModelAndView modelAndView = null;
        try {
            customerService.updateCustomer(id, name, emailId, phoneNumber);
            modelAndView = new ModelAndView(Constant.HOME, Constant.MESSAGE, Constant.CUSTOMER_UPDATED);
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            modelAndView = new ModelAndView(Constant.ERROR);
        }
        return modelAndView;
    }

    /**
     * <p>
     * Display all the customers stored in the database.
     * </p>
     *
     * @return modelAndView
     *        Redirects to the corresponding page.
     */
    @RequestMapping(params = Constant.DISPLAY, method = RequestMethod.GET)
    private ModelAndView displayAllCustomers() {
        ModelAndView modelAndView = null;
        try {
            final List<Customer> customers = customerService.getAllCustomers();
            modelAndView = new ModelAndView(Constant.DISPLAY_CUSTOMER, Constant.CUSTOMERS, customers);
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            modelAndView = new ModelAndView(Constant.ERROR);
        }
        return modelAndView;
    }

    /**
     * <P>
     * Get the customer details from the database by id.
     * </p>
     *
     * @param id
     *        Id of the customer.
     * @param methodName
     *        Update or search.
     * @return modelAndView
     *        Redirects to the corresponding page.
     */
    @RequestMapping(params = Constant.GET, method = RequestMethod.GET)
    private ModelAndView getCustomer(@RequestParam(Constant.ID) final Long id,
            @RequestParam(Constant.METHOD) final String methodName) {
        ModelAndView modelAndView = new ModelAndView();
        try {
            final Customer customer = customerService.getCustomerById(id);
            modelAndView.addObject(Constant.CUSTOMER, customer);
            if(methodName.equals(Constant.METHOD_UPDATE)) {
                modelAndView.setViewName(Constant.UPDATE_CUSTOMER);
            } else if(methodName.equals(Constant.METHOD_SEARCH)) {
                modelAndView.setViewName(Constant.SEARCH_CUSTOMER);
            }
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            modelAndView = new ModelAndView(Constant.ERROR);
        }
        return modelAndView;
    }

    /**
     * <p>
     * Deactivate the customer stored in the database.
     * </p>
     *
     * @param id
     *        Id of the customer to be deactivated.
     * @return modelAndView
     *        Redirects to the corresponding page.
     */
    @RequestMapping(params = Constant.DELETE, method = RequestMethod.DELETE)
    private ModelAndView deleteCustomer(@RequestParam(Constant.ID) final Long id) {
        ModelAndView modelAndView = null;
        try {
            customerService.deActivateCustomer(id);
            modelAndView = new ModelAndView(Constant.HOME, Constant.MESSAGE, Constant.CUSTOMER_DELETED);
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            modelAndView = new ModelAndView(Constant.ERROR);
        }
        return modelAndView;
    }
}
