package com.ideas2it.onlineshopping.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.logger.ApplicationLogger;
import com.ideas2it.onlineshopping.model.Cart;
import com.ideas2it.onlineshopping.model.CartDetail;
import com.ideas2it.onlineshopping.model.Payment;
import com.ideas2it.onlineshopping.service.CartDetailService;
import com.ideas2it.onlineshopping.service.CartService;
import com.ideas2it.onlineshopping.service.CustomerService;
import com.ideas2it.onlineshopping.service.OrderDetailService;
import com.ideas2it.onlineshopping.service.PaymentService;

/**
 * Implements the payment management with the insert operation of the cart.
 */
@Controller
@RequestMapping(Constant.PAYMENT_CONTROLLER)
public class PaymentController {

    @Autowired
    private PaymentService paymentService;
    @Autowired
    private OrderDetailService orderDetailService;
    @Autowired
    private CartService cartService;
    @Autowired
    private CustomerService customerService;

    @Autowired
    private CartDetailService cartDetailService;
    private final ApplicationLogger applicationLogger = new ApplicationLogger(PaymentController.class.getName());

    /**
     * Implemented to insert payment by requesting details from user interface
     * details like payment, shipment id from user interface
     *
     * @param payment
     *            - contains details like id, payment type and shipment details
     * @param shipmentId
     *            - id of shipment to retrieve and add in shipment detail
     * @return modelAndView - returns the object of ModelAndView
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    @RequestMapping(params = Constant.INSERT_PAYMENT_DETAIL, method = RequestMethod.POST)
    private ModelAndView createPayment(@ModelAttribute final Payment payment,
            @RequestParam(Constant.SHIPMENT_ID) final Long shipmentId,
            @RequestParam(Constant.CUSTOMER_ID) final Long customerId,
            @RequestParam(Constant.CART_ID) final Long cartId) {
        final ModelAndView modelAndView = new ModelAndView();
        try {
            // final Payment payment = new Payment();
            // payment.setAmount(totalAmount);
            final Payment newPayment = paymentService.insertPayment(payment);
            orderDetailService.createOrderDetail(cartId, shipmentId, customerId, newPayment.getId());
            final Cart cart = cartService.getCart(cartId);
            final List<CartDetail> cartDetails = cart.getCartDetails();
            for (final CartDetail cartDetail : cartDetails) {
                cartDetailService.deleteCartDetail(cartDetail);
            }
            cartService.deleteCart(cartId);
            modelAndView.setViewName(Constant.ORDER_PLACED_SUCESS_JSP);
        } catch (final ApplicationException exception) {
            applicationLogger.error(exception.getMessage(), exception);
            return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
        }
        return modelAndView;
    }
}
