package com.ideas2it.onlineshopping.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.logger.ApplicationLogger;
import com.ideas2it.onlineshopping.model.Address;
import com.ideas2it.onlineshopping.model.Customer;
import com.ideas2it.onlineshopping.model.Shipment;
import com.ideas2it.onlineshopping.service.AddressService;
import com.ideas2it.onlineshopping.service.CustomerService;
import com.ideas2it.onlineshopping.service.ShipmentService;

@Controller
@RequestMapping(Constant.SHIPMENT_CONTROLLER)
public class ShipmentController {

    @Autowired
    private ShipmentService shipmentService;
    @Autowired
    private AddressService addressService;
    @Autowired
    private CustomerService customerService;
    private final ApplicationLogger applicationLogger = new ApplicationLogger(ShipmentController.class.getName());


    /**
     * Implemented to insert shipment by requesting details from user interface details
     * like shipment, and address , order id from user interface
     *
     * @param shipment - contains shipmnent details like id, delivery date, delivery status,
     *                   delivery address, order.
     * @param addressId - id of address to retrieve and add in shipment detail
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    @RequestMapping(params = Constant.INSERT_SHIPMENT, method=RequestMethod.POST)
    private ModelAndView insertShipment(@RequestParam(Constant.ADDRESS_ID) final Long addressId,
            @RequestParam(Constant.PRICE) final double price,
            @RequestParam(Constant.TOTAL_AMOUNT) final double totalAmount) {
        final ModelAndView modelAndView = new ModelAndView();
        try {
            final Address address = addressService.getAddressById(addressId);
            final Shipment shipment = shipmentService.insertShipment(price, address);
            modelAndView.addObject(Constant.TOTAL_AMOUNT, totalAmount);
            modelAndView.addObject(Constant.SHIPMENT_ID, shipment.getId());
            modelAndView.setViewName(Constant.PAYMENT_JSP);
        } catch (final ApplicationException exception) {
            applicationLogger.error(exception.getMessage(), exception);
            return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
        }
        return modelAndView;
    }

    /**
     * Implemented to redirect current page to create address
     *
     * @return ModelAndView - redirects to the view with the added attributes.
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    @RequestMapping(params = Constant.GOTO_ADDRESS, method=RequestMethod.GET)
    private ModelAndView gotoAddress() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject(Constant.PAGE, Constant.SHIPMENT);
        return modelAndView;
    }

    /**
     * Implemented to redirect to shipment page
     *
     * @param totalAmount
     *            - total price of the order
     * @return modelAndView - returns the object of ModelAndView
     */
    @RequestMapping(params=Constant.GOTO_SHIPMENT, method=RequestMethod.POST)
    private ModelAndView goToShipment(@RequestParam(Constant.TOTAL_AMOUNT) final double totalAmount,
            @RequestParam(Constant.CUSTOMER_ID) final Long customerId) {
        final ModelAndView modelAndView = new ModelAndView();
        try {
            final Customer customer = customerService.getCustomerById(customerId);
            final List<Address> addresses = addressService.getAddressesByCustomerId(customerId);
            modelAndView.addObject(Constant.ADDRESSES, addresses);
            modelAndView.addObject(Constant.TOTAL_AMOUNT, totalAmount);
            modelAndView.setViewName(Constant.SHIPMENT_JSP);
        } catch (final ApplicationException exception) {
            applicationLogger.error(exception.getMessage(), exception);
            return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
        }
        return modelAndView;
    }
}
