package com.ideas2it.onlineshopping.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.logger.ApplicationLogger;
import com.ideas2it.onlineshopping.model.Category;
import com.ideas2it.onlineshopping.model.Product;
import com.ideas2it.onlineshopping.model.ProductSeller;
import com.ideas2it.onlineshopping.model.Seller;
import com.ideas2it.onlineshopping.model.SubCategory;
import com.ideas2it.onlineshopping.service.CategoryService;
import com.ideas2it.onlineshopping.service.ProductService;
import com.ideas2it.onlineshopping.service.SellerService;
import com.ideas2it.onlineshopping.service.SubCategoryService;

/**
 * Implements the Product Management with the insert, in activate,
 * retrieve and update of the product.
 *
 * @author Durgadevi Ananthakrishnan
 */
@Controller
@RequestMapping(Constant.PRODUCT_CONTROLLER)
public class ProductController {

    private final ApplicationLogger applicationLogger = new ApplicationLogger(ProductController.class.getName());

    @Autowired
    private ProductService productService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private SubCategoryService subCategoryService;

    @Autowired
    private SellerService sellerService;

    /**
     * Create the product by getting the request from which the product with the
     * product details like the id, price, image, name, description with the sub
     * product and the carts were obtained and were added to the product.
     *
     * @param product
     *            - the product with the details to be added to the product.
     * @param bindingResult
     *            -Holds the result of the validation and binding and contains
     *            errors that may have occurred.
     * @param isNeededAll
     *            - true if all the products are needed else false.
     * @return ModelAndView - redirects to the view with the added attributes.
     */
    @RequestMapping(params = Constant.CREATE, method=RequestMethod.POST)
    public ModelAndView createProduct(@ModelAttribute(Constant.PRODUCT) final Product product,
            final BindingResult bindingResult, @RequestParam(Constant.IS_NEEDED_ALL) final boolean isNeededAll,
            @RequestParam(Constant.SELLER_ID) final Long sellerId, @RequestParam(Constant.CATEGORY_ID) final Long categoryId,
            @RequestParam(Constant.SUB_CATEGORY_ID) final Long subCategoryId, @RequestParam(Constant.QUANTITY) final int quantity) {
        final ModelAndView modelAndView = new ModelAndView();
        try {
            if (productService.isValidProduct(product)) {
                final Category category = categoryService.getCategoryById(categoryId, Boolean.FALSE);
                final SubCategory subCategory = subCategoryService.getSubCategoryById(subCategoryId, Boolean.FALSE);
                product.setCategory(category);
                product.setSubCategory(subCategory);
                final Seller seller = sellerService.getSellerById(sellerId);
                final List<ProductSeller> productSellers = new ArrayList<>();
                final ProductSeller productSellerDetail = new ProductSeller(product, seller, quantity);
                productSellers.add(productSellerDetail);
                product.setProductSellers(productSellers);
                productService.addProduct(product);

                final List<ProductSeller> productSellerDetails = seller.getProductSellers();
                final List<Product> products = new ArrayList<>();
                for(final ProductSeller productSeller : productSellerDetails) {
                    final Product productDetail = productSeller.getProduct();
                    products.add(productDetail);
                }
                modelAndView.addObject(Constant.SELLER, seller);
                modelAndView.addObject(Constant.PRODUCTS, products);
                modelAndView.setViewName(Constant.VIEW_ALL_PRODUCTS_JSP);
            } else {
                return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
            }
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
        }
        return modelAndView;
    }

    /**
     * Creates a new product object for the empty form and stuffs it into the model
     *
     * @model - the model, in which the new product is added.
     */
    @ModelAttribute
    public void addProduct(final Model model) {
        final Product product = new Product();
        model.addAttribute(Constant.PRODUCT, product);
    }

    /**
     * Redirects to the corresponding page depending on the form name.
     *
     * @param id - represents the method depend on that it redirects to
     *                   the corresponding page.
     * @return ModelAndView - redirects to the view with the added attributes.
     */
    @RequestMapping(params = Constant.REDIRECT, method=RequestMethod.GET)
    public ModelAndView redirectToView(@RequestParam(Constant.ID) final Long id) {
        try {
            final ModelAndView modelAndView = new ModelAndView(Constant.CREATE_PRODUCT);
            final List<Category> categories = categoryService.getAllCategories(Boolean.TRUE);
            final List<SubCategory> subCategories = subCategoryService.getAllSubCategories(Boolean.TRUE);
            modelAndView.addObject(Constant.CATEGORIES, categories);
            modelAndView.addObject(Constant.SUB_CATEGORIES, subCategories);
            modelAndView.addObject(Constant.ID, id);
            return modelAndView;
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
        }
    }

    /**
     * Get the product by getting the request from which the id is obtained and
     * sending the response to the method depending on the request which may be view
     * or update.
     *
     * @param id
     *            - id of the product, to retrieve the product with the details.
     * @param methodName
     *            - method name, depending on it, redirects to the
     *            corresponding view.
     * @param isNeededActive
     *            - true if active product is needed else false.
     * @return ModelAndView - redirects to the view with the added attributes.
     */
    @RequestMapping(params = Constant.GET, method=RequestMethod.GET)
    public ModelAndView getProductById(@RequestParam(Constant.ID) final Long id,
            @RequestParam(Constant.METHOD_NAME) final String methodName,
            @RequestParam(Constant.IS_NEEDED_ACTIVE) final boolean isNeededActive) {
        try {
            final Product product = productService.getProductById(id, isNeededActive);
            if (methodName.equals(Constant.VIEW)) {
                return new ModelAndView(Constant.VIEW_PRODUCT_JSP, Constant.PRODUCT, product);
            } else {
                return new ModelAndView(Constant.UPDATE_PRODUCT_JSP, Constant.PRODUCT, product);
            }
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
        }
    }

    /**
     * Update the product by getting the request from which the product with the
     * product details like the id, price, image, name, description with the sub
     * product and the carts were obtained and were updated to the product.
     *
     * @param product
     *            - the product with the details to be updated to the product.
     * @param bindingResult
     *            -Holds the result of the validation and binding and contains
     *            errors that may have occurred.
     * @param isNeededActive
     *            - true if active product is needed else false.
     * @param isNeededAll
     *            - true if all the products are needed else false.
     * @return ModelAndView - redirects to the view with the added attributes.
     */
    @RequestMapping(params = Constant.UPDATE, method=RequestMethod.POST)
    public ModelAndView updateProduct(@ModelAttribute(Constant.PRODUCT) final Product product,
            final BindingResult bindingResult, @RequestParam(Constant.IS_NEEDED_ACTIVE) final boolean isNeededActive,
            @RequestParam(Constant.IS_NEEDED_ALL) final boolean isNeededAll, @RequestParam(Constant.SELLER_ID) final Long sellerId) {
        final ModelAndView modelAndView = new ModelAndView();
        try {
            productService.updateProduct(product, isNeededActive);
            final Seller seller = sellerService.getSellerById(sellerId);
            final List<ProductSeller> productSellers = seller.getProductSellers();
            final List<Product> products = new ArrayList<>();
            for(final ProductSeller productSeller : productSellers) {
                final Product productDetail = productSeller.getProduct();
                products.add(productDetail);
            }
            modelAndView.addObject(Constant.PRODUCTS, products);
            modelAndView.addObject(Constant.MESSAGE, Constant.SUCCESS);
            modelAndView.setViewName(Constant.VIEW_ALL_PRODUCTS_JSP);
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
        }
        return modelAndView;
    }

    /**
     * Activate the product by getting the request from which the id of the
     * product to be activated is obtained and updated to the product.
     *
     * @param id
     *            - id of the product by which the product will be retrieved
     *            to activate.
     * @return ModelAndView - redirects to the view with the added attributes.
     */
    @RequestMapping(params = Constant.ACTIVATE, method=RequestMethod.GET)
    private ModelAndView activateProduct(@RequestParam(Constant.ID) final Long id,
            @RequestParam(Constant.SELLER_ID) final Long sellerId) {
        final ModelAndView modelAndView = new ModelAndView();
        try {
            productService.activateProduct(id);
            final Seller seller = sellerService.getSellerById(sellerId);
            final List<ProductSeller> productSellers = seller.getProductSellers();
            final List<Product> products = new ArrayList<>();
            for(final ProductSeller productSeller : productSellers) {
                final Product product = productSeller.getProduct();
                products.add(product);
            }
            modelAndView.addObject(Constant.PRODUCTS, products);
            modelAndView.addObject(Constant.MESSAGE, Constant.SUCCESS);
            modelAndView.setViewName(Constant.VIEW_ALL_PRODUCTS_JSP);
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
        }
        return modelAndView;
    }

    /**
     * Deactivate the product by getting the request from which the id of the
     * product to be deactivated is obtained and updated to the product.
     *
     * @param id
     *            - id of the product by which the product will be retrieved
     *            to deactivate.
     * @return ModelAndView - redirects to the view with the added attributes.
     */
    @RequestMapping(params = Constant.IN_ACTIVATE, method=RequestMethod.GET)
    private ModelAndView deactivateProduct(@RequestParam(Constant.ID) final Long id,
            @RequestParam(Constant.SELLER_ID) final Long sellerId) {
        final ModelAndView modelAndView = new ModelAndView();
        try {
            productService.deactivateProduct(id);
            final Seller seller = sellerService.getSellerById(sellerId);
            final List<ProductSeller> productSellers = seller.getProductSellers();
            final List<Product> products = new ArrayList<>();
            for(final ProductSeller productSeller : productSellers) {
                final Product product = productSeller.getProduct();
                products.add(product);
            }
            modelAndView.addObject(Constant.PRODUCTS, products);
            modelAndView.addObject(Constant.MESSAGE, Constant.SUCCESS);
            modelAndView.setViewName(Constant.VIEW_ALL_PRODUCTS_JSP);
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            return new ModelAndView(Constant.VIEW_ALL_PRODUCTS_JSP, Constant.MESSAGE, Constant.FAILED);
        }
        return modelAndView;
    }

    /**
     * Get all the products by getting the request and all the products with
     * their details were retrieved from the product.
     *
     * @param isNeededAll
     *            - true if all the products are needed else false.
     * @return ModelAndView - redirects to the view with the added attributes.
     */
    @RequestMapping(params = Constant.GET_ALL, method=RequestMethod.GET)
    private ModelAndView getAllProducts(@RequestParam(Constant.IS_NEEDED_ALL) final boolean isNeededAll) {
        List<Product> products = new ArrayList<>();
        try {
            products = productService.getAllProducts(isNeededAll);
            return new ModelAndView(Constant.VIEW_ALL_PRODUCTS_JSP, Constant.PRODUCTS, products);
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
        }
    }

    /**
     * Get all the products by getting the request and all the products with
     * their details were retrieved from the product.
     *
     * @param isNeededAll
     *            - true if all the products are needed else false.
     * @return ModelAndView - redirects to the view with the added attributes.
     */
    @RequestMapping(params = Constant.SEARCH, method=RequestMethod.GET)
    private ModelAndView getAllSearchProducts(@RequestParam(Constant.NAME) final String name) {
        List<Product> products = new ArrayList<>();
        try {
            products = productService.getAllSearchProducts(name);
            return new ModelAndView("displayProducts", Constant.PRODUCTS, products);
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
        }
    }
}
