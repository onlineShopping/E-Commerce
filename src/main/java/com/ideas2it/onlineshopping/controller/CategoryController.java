package com.ideas2it.onlineshopping.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.logger.ApplicationLogger;
import com.ideas2it.onlineshopping.model.Category;
import com.ideas2it.onlineshopping.service.CategoryService;

/**
 * Implements the Category Management with the insert, in activate,
 * retrieve and update of the category.
 *
 * @author Durgadevi Ananthakrishnan
 */
@Controller
@RequestMapping(Constant.CATEGORY_CONTROLLER)
public class CategoryController {

    private final ApplicationLogger applicationLogger = new ApplicationLogger(CategoryController.class.getName());

    @Autowired
    private CategoryService categoryService;

    /**
     * Redirects to the corresponding page depending on the form name.
     *
     * @param formname
     *            - represents the method depend on that it redirects to
     *            the corresponding page.
     * @return ModelAndView - redirects to the view with the added attributes.
     */
    @RequestMapping(params = Constant.REDIRECT, method = RequestMethod.GET)
    public ModelAndView redirectToView(@RequestParam(Constant.FORMNAME) final String formName) {
        switch (formName) {
            case Constant.CASE_CREATE:
                return new ModelAndView(Constant.CREATE_CATEGORY);
        }
        return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
    }

    /**
     * Redirects to the corresponding page depending on the form name.
     *
     * @param formname
     *            - represents the method depend on that it redirects to
     *            the corresponding page.
     * @return ModelAndView - redirects to the view with the added attributes.
     */
    @RequestMapping("*")
    public ModelAndView retrieveSubCategory() {
        List<Category> categories = new ArrayList<>();
        try {
            categories = categoryService.getAllCategories(Boolean.FALSE);
            return new ModelAndView("home", Constant.CATEGORIES, categories);
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            return new ModelAndView("home");
        }
    }


    /**
     * Create the category by getting the request from which the details of the
     * category were obtained and added to the category.
     *
     * @param category-
     *            category, contains the category with the details like id, name and
     *            description with the sub categories to be added.
     * @param bindingResult
     *            -Holds the result of the validation and binding and contains
     *            errors that may have occurred. -
     * @param isNeededAll
     *            - true if all the categories are needed else false.
     * @return ModelAndView - redirects to the view with the added attributes.
     */
    @RequestMapping(params = Constant.CREATE, method = RequestMethod.POST)
    private ModelAndView createCategory(@ModelAttribute(Constant.CATEGORY) final Category category,
            final BindingResult bindingResult, @RequestParam(Constant.IS_NEEDED_ALL) final boolean isNeededAll) {
        try {
            if (categoryService.isValidCategory(category)
                    && !categoryService.isCategoryDetailExists(Constant.NAME, category.getName())) {
                categoryService.addCategory(category);
                return getAllCategories(isNeededAll);
            } else {
                return new ModelAndView(Constant.CREATE_CATEGORY, Constant.MESSAGE, Constant.INVALID);
            }
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            return new ModelAndView(Constant.ERROR);
        }
    }

    /**
     * Creates a new category object for the empty form and stuffs it into the
     * model.
     *
     * @model - the model, in which the new category is added.
     */
    @ModelAttribute
    public void addCategory(final Model model) {
        model.addAttribute(Constant.CATEGORY, new Category());
    }

    /**
     * Get the category by getting the category id from the request and sending
     * the response to the method depending on the request which may be view or
     * update.
     *
     * @param id
     *            - id of the category, to retrieve the category with the details.
     * @param methodName
     *            - method name, depending on it, redirects to the
     *            corresponding view.
     * @param isNeededActive
     *            - true if active category is needed else false.
     * @return ModelAndView - redirects to the view with the added attributes.
     */
    @RequestMapping(params = Constant.GET, method = RequestMethod.GET)
    private ModelAndView getCategory(@RequestParam(Constant.ID) final Long id,
            @RequestParam(Constant.METHOD_NAME) final String methodName,
            @RequestParam(Constant.IS_NEEDED_ACTIVE) final boolean isNeededActive) {
        try {
            final Category category = categoryService.getCategoryById(id, isNeededActive);
            if (methodName.equals(Constant.VIEW)) {
                return new ModelAndView(Constant.VIEW_CATEGORY_JSP, Constant.CATEGORY, category);
            } else {
                return new ModelAndView(Constant.UPDATE_CATEGORY_JSP, Constant.CATEGORY, category);
            }
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
        }
    }

    /**
     * Update the category by getting the request from which the updated parameters
     * of the category were obtained and updated to the category.
     *
     * @param category-
     *            category, contains the category with the details like id, name and
     *            description with the sub categories to be updated.
     * @param bindingResult
     *            -Holds the result of the validation and binding and contains
     *            errors that may have occurred. -
     * @param isNeededActive
     *            - true if active category is needed else false.
     * @return ModelAndView - redirects to the view with the added attributes.
     */
    @RequestMapping(params = Constant.UPDATE, method = RequestMethod.POST)
    private ModelAndView updateCategory(@ModelAttribute(Constant.CATEGORY) final Category category,
            final BindingResult bindingResult, @RequestParam(Constant.IS_NEEDED_ACTIVE) final boolean isNeededActive,
            @RequestParam(Constant.IS_NEEDED_ALL) final boolean isNeededAll) {
        try {
            categoryService.updateCategory(category, isNeededActive);
            return getAllCategories(isNeededAll);
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
        }
    }

    /**
     * Activate the category by getting the request from which the id of the
     * category to be activated is obtained and updated to the category.
     *
     * @param id
     *            - id of the category by which the category will be retrieved
     *            to activate.
     * @return ModelAndView - redirects to the view with the added attributes.
     */
    @RequestMapping(params = Constant.ACTIVATE, method = RequestMethod.GET)
    private ModelAndView activateCategory(@RequestParam(Constant.ID) final Long id) {
        final ModelAndView modelAndView = new ModelAndView();
        try {
            categoryService.activateCategory(id);
            final List<Category> categories = categoryService.getAllCategories(Boolean.TRUE);
            modelAndView.addObject(Constant.CATEGORIES, categories);
            modelAndView.addObject(Constant.MESSAGE, Constant.SUCCESS);
            modelAndView.setViewName(Constant.VIEW_ALL_CATEGORIES_JSP);
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            return new ModelAndView(Constant.VIEW_ALL_CATEGORIES_JSP, Constant.MESSAGE, Constant.FAILED);
        }
        return modelAndView;
    }

    /**
     * Deactivate the category by getting the request from which the id of the
     * category to be deactivated is obtained and updated to the category.
     *
     * @param id
     *            - id of the category by which the category will be retrieved
     *            to deactivate.
     * @return ModelAndView - redirects to the view with the added attributes.
     */
    @RequestMapping(params = Constant.IN_ACTIVATE, method = RequestMethod.GET)
    private ModelAndView deactivateCategory(@RequestParam(Constant.ID) final Long id) {
        final ModelAndView modelAndView = new ModelAndView();
        try {
            categoryService.deactivateCategory(id);
            final List<Category> categories = categoryService.getAllCategories(Boolean.TRUE);
            modelAndView.addObject(Constant.CATEGORIES, categories);
            modelAndView.addObject(Constant.MESSAGE, Constant.SUCCESS);
            modelAndView.setViewName(Constant.VIEW_ALL_CATEGORIES_JSP);
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            return new ModelAndView(Constant.VIEW_ALL_CATEGORIES_JSP, Constant.MESSAGE, Constant.FAILED);
        }
        return modelAndView;
    }

    /**
     * Get all the categories by getting the request and all the categories with
     * their details were retrieved from the category.
     *
     * @param isNeededAll
     *            - true if all the categories are needed else false.
     * @return ModelAndView - redirects to the view with the added attributes.
     */
    @RequestMapping(params = Constant.GET_ALL, method = RequestMethod.GET)
    private ModelAndView getAllCategories(@RequestParam(Constant.IS_NEEDED_ALL) final boolean isNeededAll) {
        List<Category> categories = new ArrayList<>();
        try {
            categories = categoryService.getAllCategories(isNeededAll);
            return new ModelAndView(Constant.VIEW_ALL_CATEGORIES_JSP, Constant.CATEGORIES, categories);
        } catch (final ApplicationException e) {
            applicationLogger.error(e.getMessage(), e.getCause());
            return new ModelAndView(Constant.ALERT, Constant.MESSAGE, Constant.FAILED);
        }
    }
}
