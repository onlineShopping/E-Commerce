package com.ideas2it.onlineshopping.exception;

/**
 * Provides the detailed message and cause of the exception.
 */
public class ApplicationException extends Exception {

    private static final long serialVersionUID = 5334810351028229192L;

    /**
     * Get the detailed message and cause of the exception thrown.
     *
     * @param exceptionMessage
     *            - the detailed messgae of the exception.
     * @param cause
     *            - the throwable cause, the cause of the exception thrown.
     */
    public ApplicationException(final String exceptionMessage, final Throwable cause) {
        super(exceptionMessage, cause);
    }
}