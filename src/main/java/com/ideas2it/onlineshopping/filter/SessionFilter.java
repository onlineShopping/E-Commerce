package com.ideas2it.onlineshopping.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ideas2it.onlineshopping.common.Constant;

/**
 * <p>
 * Prevents the browser from caching.
 * </p>
 * 
 * @author Ajaykkumar R
 */
public class SessionFilter implements Filter {
    
    @Override
    public void init(final FilterConfig filterConfig) {
    }
   
    @Override
    public void destroy() {
    }
    
    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response,
                       final FilterChain chain) throws IOException, ServletException { 
        final HttpServletRequest httpRequest = (HttpServletRequest) request;
        final HttpServletResponse httpResponse = (HttpServletResponse) response;
        final HttpSession session = httpRequest.getSession(Boolean.FALSE);
        if(null != session && null == session.getAttribute(Constant.USERNAME)) {
            final RequestDispatcher req = request.getRequestDispatcher(Constant.HOME_JSP);
            req.forward(request, response);
        }
        chain.doFilter(request, response);  
    }
}  
