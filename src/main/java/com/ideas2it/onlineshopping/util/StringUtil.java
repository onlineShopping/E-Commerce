package com.ideas2it.onlineshopping.util;

/**
 * Implementation of string utils.
 */
public class StringUtil {

    /**
     * Append the various strings and return them as a single string.
     *
     * @param messages - messages, the number of strings given by the user to 
     *                   to append.
     * @return string - string, return the string which is the
     *                  addition of many strings given by the user and
     *                  used to append all the strings.
     */
    public static String concatenate(final String... messages) {
        final StringBuffer stringBuffer = new StringBuffer();
        for (int index = 0; index < messages.length; index++) {
            stringBuffer.append(messages[index]);
        }
        return stringBuffer.toString();
    }
}
