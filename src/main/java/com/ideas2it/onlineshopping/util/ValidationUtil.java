package com.ideas2it.onlineshopping.util;

import com.ideas2it.onlineshopping.common.Constant;


/**
 * Validation of user given inputs with the set patterns.
 */
public class ValidationUtil {

    /**
     * <p>
     * Validate the input given by the user and compare it with the given
     * pattern and return true, if valid else false.
     * </p>
     *
     * @param input
     *            - input, input given by the user, which will be compared
     *            with the given pattern.
     * @param pattern
     *            - pattern, pattern given by the user, which must be
     *            matched with the given input for validation.
     * @return boolean - boolean, return true if the input is valid, else
     *         return false.
     */
    public static boolean isValidInput(final String input, final String pattern) {
        return input.matches(pattern) ? Boolean.TRUE : Boolean.FALSE;
    }

    /**
     * <p>
     * Validate the phone number with the given pattern.
     * </p>
     *
     * @param phoneNumber
     *         Phone number to be validated.
     * @return boolean
     *         True if it matches the pattern,False if not matches.
     */
    public static boolean isValidPhoneNumber(final Long phoneNumber) {
        final String number = Long.toString(phoneNumber);
        return number.matches(Constant.PATTERN_NUMBER) ? Boolean.TRUE : Boolean.FALSE;
    }

    /**
     * Validate the alpha numeric inputs given by the user and compare it with the
     * given pattern and return true, if valid else false.
     *
     * @param alphaNumeric
     *            - alpha numeric input, input given by the user, which will be
     *            compared with the given pattern.
     * @return boolean - boolean, return true if the input is valid, else
     *         return false.
     */
    public static boolean isValidAlphaNumeric(final String alphaNumeric) {
        return alphaNumeric.matches(Constant.ALPHA_NUMERIC_PATTERN) ? Boolean.TRUE : Boolean.FALSE;
    }
}
