package com.ideas2it.onlineshopping.service;

import java.util.List;

import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.CartDetail;

/**
 * Implements the CartDetail Service with update, and get the cartDetails.
 */
public interface CartDetailService {

    /**
     * Implemented to update quantity of cartDetail with respect to productId
     *
     * @param cartId
     *            - Id of the cart.
     * @param productId
     *            - Id of the product.
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    void updateCartDetail(final Long cartId, final Long productId, int quantity) throws ApplicationException;

    /**
     * Implemented to retrieve cartDetail with reference of product id
     *
     * @param cartId
     *            - Id of the cart.
     * @param productId
     *            - Id of the product with which cart is been retrieved
     * @return cartDetail
     *         - Returns the cartDetail respective the the product id
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    CartDetail getCartDetail(final Long cartId, final Long productId) throws ApplicationException;

    /**
     * Implemented to delete cartdetail respect to productId and cartId.
     *
     * @param cartId
     *            - Id of the cart.
     * @param productId
     *            - Id of the product.
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    boolean deleteCartDetail(final Long cartId, final Long productId) throws ApplicationException;

    /**
     * Implemented to delete cartdetail respect to productId and cartId.
     *
     * @param CartDetail - contains details like id, product, date, customerId.
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    boolean deleteCartDetail(final CartDetail cartDetail) throws ApplicationException;

    void insertCartDetail(CartDetail cartDetail) throws ApplicationException;

    List<CartDetail> getCartDetailByCartId(final Long cartId) throws ApplicationException;
}
