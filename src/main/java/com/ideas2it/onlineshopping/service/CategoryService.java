package com.ideas2it.onlineshopping.service;

import java.util.List;

import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Category;

/**
 * Implements the Category Management with the insert, in activate, retrieve and
 * update of the category.
 *
 * @author Durgadevi Ananthakrishnan.
 */
public interface CategoryService {

    /**
     * Add the category with the category details like id, name and description
     * into the category.
     *
     * @param category
     *            - the category, the category to be inserted with the user given
     *            details.
     * @return Category
     *         - category if the category is added successfully else throws
     *         exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    Category addCategory(Category category) throws ApplicationException;

    /**
     * Get the category from the category by providing the id of the category.
     *
     * @param id
     *            - id of the category, to retrieve the category from the category.
     * @param isNeededActive
     *            - true if active category is needed else false.
     * @return Boolean.TRUE
     *         - true if the category is retrieved successfully else throws
     *         exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    Category getCategoryById(Long id, boolean isNeededActive) throws ApplicationException;

    /**
     * Validate the category details like id, name and description with their
     * corresponding valid patterns.
     *
     * @param category
     *            - category with the given details to be validated with their
     *            pattern.
     * @return boolean - holds true if the details are valid else false.
     */
    boolean isValidCategory(Category category);

    /**
     * Activate the category with the user given updated details of the category by
     * updating the is active of the category as true..
     *
     * @param id
     *            - the id, the id of the category which is to be activated.
     * @return Boolean.TRUE
     *         - true if the category is activated successfully else throws
     *         exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception.
     */
    boolean activateCategory(final Long id) throws ApplicationException;

    /**
     * Deactivate the category with the user given updated details of the
     * category by updating the is active of the category as false..
     *
     * @param id
     *            - the id, the id of the category which is to be deactivated.
     * @return Boolean.TRUE
     *         - true if the category is deactivated successfully else throws
     *         exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception.
     */
    boolean deactivateCategory(final Long id) throws ApplicationException;

    /**
     * Update the category with the user given updated details of the category.
     *
     * @param category
     *            - the category, the category to be updated with the user given
     *            updated details.
     * @param isNeededActive
     *            - true if active category is to be updated else in active category
     *            will be updated.
     * @return Boolean.TRUE
     *         - true if the category is updated successfully else throws exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception.
     */
    boolean updateCategory(Category category, boolean isNeededActive) throws ApplicationException;

    /**
     * Get all the categories with the category details from the category.
     *
     * @param isNeededAll
     *            - true if all the categories are needed else false.
     * @return - categories, all categories available in the category.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception.
     */
    List<Category> getAllCategories(boolean isNeededAll) throws ApplicationException;

    /**
     * Check whether the category detail like the id and name is exited or not.
     *
     * @param columnName
     *            - column name of the detail, to check the existence of detail.
     * @param detail
     *            - the detail of the category, to check its existence.
     * @return boolean
     *         - true if the detail is already existed in the category else false.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    boolean isCategoryDetailExists(String columnName, String detail) throws ApplicationException;

}