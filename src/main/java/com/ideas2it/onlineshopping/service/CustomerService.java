package com.ideas2it.onlineshopping.service;

import java.util.List;

import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Customer;
import com.ideas2it.onlineshopping.model.User;

/**
 * <p>
 * Customer interface which defines CRUD operations to be performed.
 * </p>
 *
 * @author Ajaykkumar R
 */
public interface CustomerService {

    /**
     * <p>
     * Create the customer and store it in the database.
     * </p>
     *
     * @param user
     *        Details of the user.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    void createCustomer(User user) throws ApplicationException;

    /**
     * <p>
     * Get the customer from the database by customer id.
     * </p>
     *
     * @param id
     *        Id of the customer.
     * @return Customer
     *        Details of the customer for the corresponding customer id.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    Customer getCustomerById(Long id) throws ApplicationException;
    
    /**
     * <p>
     * Get the customer from the database by user id.
     * </p>
     *
     * @param id
     *        Userid of the customer.
     * @return Customer
     *        Details of the customer for the corresponding user id.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    Customer getCustomerByUserId(Long id) throws ApplicationException;
    

    /**
     * <p>
     * Get the customer from the database by customer email id.
     * </p>
     *
     * @param emailId
     *        Emailid of the customer.
     * @return Customer
     *        Details of the customer for the corresponding customer email id.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    Customer getCustomerByEmailId(String emailId) throws ApplicationException;

    /**
     * <p>
     * Update the customer details in the database.
     * </p>
     * 
     * @param id
     *        Id of the customer.
     * @param name
     *        Name of the customer to be updated.
     * @param emailId
     *        Emailid of the customer to be updated.
     * @param phoneNumber
     *        Phone Number of the customer to be updated.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    void updateCustomer(Long id, String name, String emailId, Long phoneNumber) throws ApplicationException;
    
    /**
     * <p>
     * Inactivate the customer and its details.
     * </p>
     *
     * @param id
     *        Id of the customer.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    void deActivateCustomer(Long id) throws ApplicationException;

    /**
     * <p>
     * Get all the customer and it details.
     * </p>
     *
     * @return customers
     *        List of all customers and its details
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    List<Customer> getAllCustomers() throws ApplicationException;
}
