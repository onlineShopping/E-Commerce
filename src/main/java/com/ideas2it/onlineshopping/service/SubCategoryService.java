package com.ideas2it.onlineshopping.service;

import java.util.List;

import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.SubCategory;

/**
 * Implements the Sub Category Management with the insert, in activate,
 * retrieve and update of the sub category.
 *
 * @author Durgadevi Ananthakrishnan
 */
public interface SubCategoryService {

    /**
     * Added the sub category with the category details like id, name and
     * description into the sub category.
     *
     * @param subCategory
     *            - the sub category, the sub category to be inserted with the user
     *            given details.
     * @return SubCategory
     *         - sub category if the sub category is added successfully else throws
     *         exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    SubCategory addSubCategory(SubCategory subCategory) throws ApplicationException;

    /**
     * Get the sub category from the sub category by providing the id of the sub
     * category.
     *
     * @param id
     *            - id of the sub category, to retrieve the sub category from the
     *            sub category.
     * @param isNeededActive
     *            - true if active category is needed else false.
     * @return Boolean.TRUE
     *         - true if the sub category is retrieved successfully else throws
     *         exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    SubCategory getSubCategoryById(final Long id, final boolean isNeededActive) throws ApplicationException;

    /**
     * Validate the sub category details like id, name and description with their
     * corresponding valid patterns.
     *
     * @param subCategory
     *            - sub category with the given details to be validated with their
     *            pattern.
     * @return boolean - holds true if the details are valid else false.
     */
    boolean isValidSubCategory(SubCategory subCategory);

    /**
     * Activate the sub category with the user given updated details of the sub
     * category by updating the is active of the category as true..
     *
     * @param id
     *            - the id, the id of the category which is to be activated.
     * @return Boolean.TRUE
     *         - true if the sub category is activated successfully else throws
     *         exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception.
     */
    boolean activateSubCategory(final Long id) throws ApplicationException;

    /**
     * Deactivate the sub category with the user given updated details of the sub
     * category by updating the is active of the category as false..
     *
     * @param id
     *            - the id, the id of the category which is to be deactivated.
     * @return Boolean.TRUE
     *         - true if the sub category is deactivated successfully else throws
     *         exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception.
     */
    boolean deactivateSubCategory(final Long id) throws ApplicationException;

    /**
     * Update the sub category with the user given updated details of the sub
     * category.
     *
     * @param subCategory
     *            - the sub category, the sub category to be updated with the user
     *            given updated details.
     * @param isNeededActive
     *            - true if active sub category to be needed else false.
     * @return Boolean.TRUE
     *         - true if the sub category is updated successfully else throws
     *         exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    boolean updateSubCategory(final SubCategory subCategory, final boolean isNeededActive) throws ApplicationException;

    /**
     * Get all the sub categories with the sub category details from the category.
     *
     * @param isNeededAll
     *            - true if all the categories are needed else false.
     * @return - sub categories, all sub categories available in the sub category.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    List<SubCategory> getAllSubCategories(final boolean isNeededAll) throws ApplicationException;

    /**
     * Check whether the sub category detail like the id and name is exited or not.
     *
     * @param columnName
     *            - column name of the detail, to check the existence of detail.
     * @param detail
     *            - the detail of the category, to check its existence.
     * @return boolean
     *         - true if the detail is already existed in the sub category else
     *         false.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    boolean isSubCategoryDetailExists(final String columnName, final String detail) throws ApplicationException;

    /** Get the sub category from the sub category by providing the name of the sub
     * category.
     *
     * @param name
     *            - name of the sub category, to retrieve the sub category from the
     *            sub category.
     * @return Boolean.TRUE
     *         - true if the sub category is retrieved successfully else throws
     *         exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    SubCategory getSubCategoryByName(final String name) throws ApplicationException;
}
