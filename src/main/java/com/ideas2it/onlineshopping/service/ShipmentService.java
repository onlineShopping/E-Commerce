package com.ideas2it.onlineshopping.service;

import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Address;
import com.ideas2it.onlineshopping.model.Shipment;

/**
 * Implements the Shipment Service with the insert of the shipment details.
 */
public interface ShipmentService {

    /**
     * Implemented to get cart, product, customer details to store in cart
     *
     * @param shipment - amount of the order
     * @param address - contains details like id, door number, street, area, city , pincode
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    Shipment insertShipment(double price, Address address) throws ApplicationException;

    /**
     * Implements to get shipment from the shipment.
     *
     * @param shipmentId - Id of the shipment
     * @return shipment - contains detail like id, delivery address, address, price
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    Shipment getShipment(final Long shipmentId) throws ApplicationException;
}
