package com.ideas2it.onlineshopping.service;

import java.util.List;

import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Product;
import com.ideas2it.onlineshopping.model.ProductSeller;

/**
 * Implements the Product Management with the insert, in activate,
 * retrieve and update of the product.
 *
 * @author Durgadevi Ananthakrishnan
 */
public interface ProductService {

    /**
     * Add the product with the product details like id, name and description
     * into the product.
     *
     * @param product
     *            - the product, the product to be inserted with the user given
     *            details.
     * @return Product
     *         - product if the product is added successfully else throws exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    Product addProduct(Product product) throws ApplicationException;

    /**
     * Update the product with the user given updated details of the product.
     *
     * @param product
     *            - the product, the product to be updated with the user given
     *            updated details.
     * @param isNeededActive
     *            - true if active product is to be updated else in active product
     *            will be updated.
     * @return Boolean.TRUE
     *         - true if the product is updated successfully else throws exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    boolean updateProduct(Product product, boolean isNeededActive) throws ApplicationException;

    /**
     * Get the product from the product by providing the id of the product.
     *
     * @param id
     *            - id of the product, to retrieve the product from the product.
     * @param isNeededActive
     *            - true if active product is needed else false.
     * @return Boolean.TRUE
     *         - true if the product is retrieved successfully else throws
     *         exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    Product getProductById(Long id, boolean isNeededActive) throws ApplicationException;

    /**
     * Activate the product with the user given updated details of the product by
     * updating the property is active of the product as true..
     *
     * @param id
     *            - the id, the id of the product which is to be activated.
     * @return Boolean.TRUE
     *         - true if the product is activated successfully else throws
     *         exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception.
     */
    boolean activateProduct(Long id) throws ApplicationException;

    /**
     * Deactivate the product with the user given updated details of the
     * product by updating the property is active of the product as false..
     *
     * @param id
     *            - the id, the id of the product which is to be deactivated.
     * @return Boolean.TRUE
     *         - true if the product is deactivated successfully else throws
     *         exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception.
     */
    boolean deactivateProduct(Long id) throws ApplicationException;

    /**
     * Get all the products with the product details from the product.
     *
     * @param isNeededAll
     *            - true if all the categories are needed else false.
     * @return - products, all products available in the product.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    List<Product> getAllProducts(boolean isNeededAll) throws ApplicationException;

    /**
     * Validate the product details like id, name and mail id with their
     * corresponding valid patterns.
     *
     * @param product
     *            - product with the given details to be validated with their
     *            pattern.
     * @return boolean - holds true if the detail is valid else false.
     */
    boolean isValidProduct(Product product);

    /**
     * Check whether the product detail like the id and name is exited or not.
     *
     * @param columnName
     *            - column name of the detail, to check the existence of detail.
     * @param detail
     *            - the detail of the product, to check its existence.
     * @return boolean
     *         - true if the detail is already existed in the product else
     *         false.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    boolean isProductDetailExists(String columnName, String detail) throws ApplicationException;

    /**
     * Get all the productsellers with the product and seller details from the 
     * seller id.
     *
     * @param id
     *            - id of the seller to retrieve all the product sellers.
     * @return - productsellers, all products with the sellers .
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    List<ProductSeller> getProductSellersBySellerId(Long sellerId) throws ApplicationException;

    /**
    * Get all the products with the product details from the product.
    *
    * @param isNeededAll
    *            - true if all the categories are needed else false.
    * @return - products, all products available in the product.
    * @throws ApplicationException
    *             - throws the detailed message of the exception with the throwable
    *             cause of the exception
    */
   List<Product> getAllSearchProducts(String name) throws ApplicationException;
}
