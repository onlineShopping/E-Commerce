package com.ideas2it.onlineshopping.service;

import java.util.List;

import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Order;
import com.ideas2it.onlineshopping.model.OrderDetail;
import com.ideas2it.onlineshopping.model.Payment;

public interface OrderService {

    /**
     * Implemented to get orderDetail detail, orderDetail, customerId details to
     * store in order
     *
     * @param orderDetail
     *            - orderDetail contains details like id, amount, customer details
     * @param customerId
     *            - Id of the customer.
     * @oaram payment - contains details of payment like id, payment type, payment
     *        time, card number.
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    void insertOrder(final List<OrderDetail> orderDetails, final Long customerId, Payment payment)
            throws ApplicationException;

    /**
     * Implemented to get customer id as reference to get all the orderDetail detail
     * corresponding
     * that reference customer id
     *
     * @param customerId
     *            - customerId as reference to get corresponding orderDetail details
     * @return order - returns the detail of orderDetail detail like id,
     *         orderDetails and
     *         customer.
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    List<Order> getOrders(Long customerId) throws ApplicationException;
}
