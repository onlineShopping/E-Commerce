package com.ideas2it.onlineshopping.service;

import java.util.List;

import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Specification;

/**
 * Implements the Specification Management with the insert, retrieve and update
 * of the specification.
 *
 * @author Durgadevi Ananthakrishnan
 */
public interface SpecificationService {

    /**
     * Add the specification with the specification details like id, name and
     * description
     * into the specification.
     *
     * @param specification
     *            - the specification, the specification to be inserted with the
     *            user given details.
     * @return Boolean.TRUE
     *         - true if the specification is added successfully else throws
     *         exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    boolean addSpecification(final Specification specification) throws ApplicationException;

    /**
     * Get the specification from the specification by providing the id of the
     * specification.
     *
     * @param id
     *            - id of the specification, to retrieve the specification from the
     *            specification.
     * @return Boolean.TRUE
     *         - true if the specification is retrieved successfully else throws
     *         exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    Specification getSpecificationById(Long id) throws ApplicationException;

    /**
     * Validate the specification details like id, name and mail id with their
     * corresponding valid patterns.
     *
     * @param specification
     *            - specification with the given details to be validated with their
     *            pattern.
     * @return boolean - holds true if the detail is valid else false.
     */
    //List<Boolean> isValidSpecification(Specification specification);

    /**
     * Update the specification with the user given updated details of the
     * specification.
     *
     * @param specification
     *            - the specification, the specification to be updated with the user
     *            given
     *            updated details.
     * @return Boolean.TRUE
     *         - true if the specification is updated successfully else throws
     *         exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception.
     */
    boolean updateSpecification(Specification specification) throws ApplicationException;

    /**
     * Get all the specifications with the specification details from the
     * specification.
     *
     * @return - specifications, all specifications available in the specification.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception.
     */
    List<Specification> getAllSpecifications() throws ApplicationException;
}
