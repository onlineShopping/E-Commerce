package com.ideas2it.onlineshopping.service;

import java.util.List;

import com.ideas2it.onlineshopping.exception.ApplicationException;

import com.ideas2it.onlineshopping.model.Address;
import com.ideas2it.onlineshopping.model.Customer;
import com.ideas2it.onlineshopping.model.Seller;

/**
 * <p>
 * Address interface which defines CRUD operations to be performed.
 * </p>
 *
 * @author Ajaykkumar R
 */
public interface AddressService {

    /**
     * <p>
     * Store the addresses of the customers and sellers.
     * </p>
     *
     * @param address
     *        Address of the seller or customer to be stored.
     * @return boolean
     *        True if it is created successfully,False if it failed.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    boolean createAddress(Address address) throws ApplicationException;

    /**
     * <p>
     * Inactivate the addresses of the customer or seller.
     * </p>
     *
     * @param id
     *        Address id to be inactivated.
     * @return boolean
     *        True if it is inactivated successfully,False if it failed.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    boolean deActivateAddress(Long id) throws ApplicationException;

    /**
     * <p>
     * Update the address of the customer or seller.
     * </p>
     *
     * @param address
     *       Address details to be updated.
     * @return boolean
     *        True if it is updated successfully,False if it failed.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    boolean updateAddress(Address address) throws ApplicationException;

    /**
     * <p>
     * Get the address of the customer by customer id.
     * </p>
     *
     * @param id
     *        Id of the customer.
     * @return addresses
     *        List of addresses of the customer.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    List<Address> getAddressesByCustomerId(Long id) throws ApplicationException;

    /**
     * <p>
     * Get the address of the customer or seller by address id.
     * </p>
     *
     * @param id
     *        Id of the address.
     * @return address
     *        Address detail for the corresponding address id.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    Address getAddressById(Long id) throws ApplicationException;
}