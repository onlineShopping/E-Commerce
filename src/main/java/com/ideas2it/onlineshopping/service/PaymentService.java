package com.ideas2it.onlineshopping.service;

import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Payment;

/**
 * Implements the payment Service with the create and retrieve operation of the payment.
 */
public interface PaymentService {

    /**
     * Implemented to get payment, shipment details to store in payment details
     *
     * @param payment
     *            - contains details like id, payment type and shipment details
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    Payment insertPayment(Payment payment) throws ApplicationException;

    /**
     * Implemented to retrieve payment with reference of its payment id
     *
     * @param paymentId - Id of the payment.
     * @return payment - contains details of payment like id, payment type, payment time, card number.
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    Payment getPayment(Long paymentId) throws ApplicationException;
}
