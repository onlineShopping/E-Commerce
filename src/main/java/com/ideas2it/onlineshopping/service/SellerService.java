package com.ideas2it.onlineshopping.service;

import java.util.List;

import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Seller;
import com.ideas2it.onlineshopping.model.User;

/**
 * <p>
 * Seller interface which defines CRUD operations to be performed.
 * </p>
 *
 * @author Ajaykkumar R
 */
public interface SellerService {

    /**
     * <p>
     * Create the seller and store it in the database.
     * </p>
     *
     * @param user
     *            Details of the user.
     * @throws ApplicationException
     *            If anything went wrong like connection not established or
     *            problem occured while executing query.
     */
    void createSeller(User user) throws ApplicationException;

    /**
     * <p>
     * Get the seller from the database by seller id.
     * </p>
     *
     * @param id
     *            Id of the seller.
     * @return Seller
     *            Details of the seller for the corresponding seller id.
     * @throws ApplicationException
     *            If anything went wrong like connection not established or
     *            problem occured while executing query.
     */
    Seller getSellerById(Long id) throws ApplicationException;
    
    /**
     * <p>
     * Get the seller from the database by user id.
     * </p>
     *
     * @param id
     *            Userid of the seller.
     * @return Seller
     *            Details of the seller for the corresponding user id.
     * @throws ApplicationException
     *            If anything went wrong like connection not established or
     *            problem occured while executing query.
     */
    Seller getSellerByUserId(Long id) throws ApplicationException;

    /**
     * <p>
     * Get the seller from the database by email id.
     * </p>
     *
     * @param emailId
     *            Emailid of the seller.
     * @return Seller
     *            Details of the seller for the corresponding email id.
     * @throws ApplicationException
     *            If anything went wrong like connection not established or
     *            problem occured while executing query.
     */
    Seller getSellerByEmailId(String emailId) throws ApplicationException;

    /**
     * <p>
     * Update the seller details in the database.
     * </p>
     * 
     * @param seller
     *            Details of the seller.
     * @param name
     *            Name of the seller to be updated.
     * @param emailId
     *            Emailid of the seller to be updated.
     * @param phoneNumber
     *            Phone Number of the seller to be updated.
     * @throws ApplicationException
     *            If anything went wrong like connection not established or
     *            problem occured while executing query.
     */
    void updateSeller(Long id, String name, String emailId, Long phoneNumber) throws ApplicationException;

    /**
     * <p>
     * Deactivate the seller and its details.
     * </p>
     *
     * @param id
     *            Id of the seller.
     * @throws ApplicationException
     *            If anything went wrong like connection not established or
     *            problem occured while executing query.
     */
    void deActivateSeller(Long id) throws ApplicationException;

    /**
     * <p>
     * Get all the seller and it details.
     * </p>
     *
     * @return sellers
     *             List of all sellers and its details
     * @throws ApplicationException
     *             If anything went wrong like connection not established or
     *             problem occured while executing query.
     */
    List<Seller> getAllSellers() throws ApplicationException;
}
