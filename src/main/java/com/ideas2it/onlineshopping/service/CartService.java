package com.ideas2it.onlineshopping.service;

import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Cart;
import com.ideas2it.onlineshopping.model.Customer;
import com.ideas2it.onlineshopping.model.Product;

/**
 * Implements the Cart Service with the create, update,
 * delete and get of the category.
 */
public interface CartService {

    /**
     * Implemented to get cart, cartDetail, customer details to store in cart
     *
     * @param productId
     *            - Id of the product.
     * @param customerId
     *            - Id of the customer.
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    void createCart(final Long productId, final Long customerId) throws ApplicationException;

    /**
     * Implemented to get the cart and cartDetail details and update in cart
     *
     * @param productId
     *            - Id of the product.
     * @param customerId
     *            - Id of the customer.
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    void updateCart(final Cart cart, final Product product, final Customer customer) throws ApplicationException;

    /**
     * Implemented to get cart id to delete the corresponding entry from cart
     *
     * @param cartId
     *            - Id of the cart with which cart is been deleted
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    void deleteCart(Long cartId) throws ApplicationException;

    /**
     * Implemented to get customer id as reference to get all the cart corresponding
     * that reference customer id
     *
     * @param customerId
     *            - Returns the cart respective the the customer id
     * @return cart
     *         - Returns list of carts respect to particular customer
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    Cart getCartByCustomer(Long customerId) throws ApplicationException;

    /**
     * Implemented to retrieve cart with reference of its cart id
     *
     * @param cartId
     *            - Id of the cart with which cart is been retrieved
     * @return cart
     *         - Returns the cart respective the the cart id
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    Cart getCart(Long cartId) throws ApplicationException;
}
