package com.ideas2it.onlineshopping.service;

import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Invoice;
import com.ideas2it.onlineshopping.model.OrderDetail;

/**
 * Implements the invoice Service with the create and retrieve operation of the invoice.
 */
public interface InvoiceService {

    /**
     * Add the invoice with the invoice details like id, name and description
     * into the invoice.
     *
     * @param orderDetail
     *            - orderDetail contains details like id, amount, customer details
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    void addInvoice(OrderDetail orderDetail) throws ApplicationException;

    /**
     * Get the invoice from the invoice by providing the id of the invoice.
     *
     * @param id
     *            - id of the invoice, to retrieve the invoice from the invoice.
     * @return Boolean.TRUE
     *         - true if the invoice is retrieved successfully else throws
     *         exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    Invoice getInvoiceById(Long id) throws ApplicationException;
}
