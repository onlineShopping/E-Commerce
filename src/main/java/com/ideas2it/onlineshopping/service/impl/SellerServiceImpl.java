package com.ideas2it.onlineshopping.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.dao.SellerDao;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Seller;
import com.ideas2it.onlineshopping.model.User;
import com.ideas2it.onlineshopping.service.SellerService;

/**
 * <p>
 * Performs seller CRUD operations.
 * </p>
 *
 * @author Ajaykkumar R
 */
@Service
public class SellerServiceImpl implements SellerService {
    
    @Autowired
    private SellerDao sellerDao;

    /**
     * @see com.ideas2it.onlineshopping.service.SellerService
     * @method createSeller
     */
    @Override
    public void createSeller(final User user) throws ApplicationException {
        final Seller seller = new Seller();
        seller.setUser(user);
        user.setSeller(seller);
        sellerDao.insertSeller(seller);
    }

    /**
     * @see com.ideas2it.onlineshopping.service.SellerService
     * @method getSellerById
     */
    @Override
    public Seller getSellerById(final Long id) throws ApplicationException {
        return sellerDao.getSeller(Constant.ID, id);
    }
    
    /**
     * @see com.ideas2it.onlineshopping.service.SellerService
     * @method getSellerById
     */
    @Override
    public Seller getSellerByUserId(final Long id) throws ApplicationException {
        return sellerDao.getSeller(Constant.USER_ID, id);
    }

    /**
     * @see com.ideas2it.onlineshopping.service.SellerService
     * @method getSellerByEmailId
     */
    @Override
    public Seller getSellerByEmailId(final String emailId) throws ApplicationException {
        return sellerDao.getSellerByEmailId(emailId);
    }

    /**
     * @see com.ideas2it.onlineshopping.service.SellerService
     * @method updateSeller
     */
    @Override
    public void updateSeller(final Long id, final String name, final String emailId, final Long phoneNumber) throws ApplicationException {
        final Seller seller = getSellerById(id);
        final User user = seller.getUser();
        user.setName(name);
        user.setEmailId(emailId);
        user.setPhoneNumber(phoneNumber);
        seller.setUser(user);
        sellerDao.updateSeller(seller);
    }

    /**
     * @see com.ideas2it.onlineshopping.service.SellerService
     * @method deActivateSeller
     */
    @Override
    public void deActivateSeller(final Long id) throws ApplicationException {
        final Seller seller = getSellerById(id);
        seller.setActive(Boolean.FALSE);
        sellerDao.updateSeller(seller);
    }

    /**
     * @see com.ideas2it.onlineshopping.service.SellerService
     * @method getAllSeller
     */
    @Override
    public List<Seller> getAllSellers() throws ApplicationException {
        return sellerDao.retrieveAllSellers();
    }
}
