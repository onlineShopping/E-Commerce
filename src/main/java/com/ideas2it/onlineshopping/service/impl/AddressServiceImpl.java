package com.ideas2it.onlineshopping.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.dao.AddressDao;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Address;
import com.ideas2it.onlineshopping.model.Customer;
import com.ideas2it.onlineshopping.model.Seller;
import com.ideas2it.onlineshopping.service.AddressService;

/**
 * <p>
 * Performs address CRUD for seller and customer.
 * </p>
 *
 * @author Ajaykkumar R
 */
@Service(Constant.ADDRESS_SERVICE)
public class AddressServiceImpl implements AddressService {

    @Autowired
    private AddressDao addressDao;

    /**
     * @see     com.ideas2it.onlineshopping.service.AddressService
     * @method  createAddress
     */
    public boolean createAddress(Address address) throws ApplicationException {
        return addressDao.insertAddress(address);
    }

    /**
     * @see     com.ideas2it.onlineshopping.service.AddressService
     * @method  inActivateAddress
     */
    @Override
    public boolean deActivateAddress(Long id) throws ApplicationException {
        Address address = getAddressById(id);
        address.setActive(Boolean.FALSE);
        return addressDao.updateAddress(address);
    }

    /**
     * @see     com.ideas2it.onlineshopping.service.AddressService
     * @method  updateAddress
     */
    public boolean updateAddress(Address address) throws ApplicationException {
        Address addressDetail = getAddressById(address.getId());
    	address.setSeller(addressDetail.getSeller());
        address.setCustomer(addressDetail.getCustomer());
        return addressDao.updateAddress(address);
    }

    /**
     * @see     com.ideas2it.onlineshopping.service.AddressService
     * @method  getAddressesByCustomerId
     */
    public List<Address> getAddressesByCustomerId(Long id) throws ApplicationException {
        return addressDao.getAddressesByCustomerId(id);
    }

    /**
     * @see     com.ideas2it.onlineshopping.service.AddressService
     * @method  getAddressById
     */
    public Address getAddressById(Long id) throws ApplicationException {
        return addressDao.getAddressById(id);
    }
}