package com.ideas2it.onlineshopping.service.impl;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.dao.OrderDetailDao;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Cart;
import com.ideas2it.onlineshopping.model.CartDetail;
import com.ideas2it.onlineshopping.model.OrderDetail;
import com.ideas2it.onlineshopping.model.Payment;
import com.ideas2it.onlineshopping.model.Product;
import com.ideas2it.onlineshopping.model.Shipment;
import com.ideas2it.onlineshopping.service.CartService;
import com.ideas2it.onlineshopping.service.CustomerService;
import com.ideas2it.onlineshopping.service.InvoiceService;
import com.ideas2it.onlineshopping.service.OrderDetailService;
import com.ideas2it.onlineshopping.service.OrderService;
import com.ideas2it.onlineshopping.service.PaymentService;
import com.ideas2it.onlineshopping.service.ShipmentService;

/**
 * Implements the OrderDetail service with the insert, delete,
 * retrieve and update of the category.
 */
@Service(Constant.ORDER_SERVICE)
public class OrderDetailServiceImpl implements OrderDetailService {

    @Autowired
    private OrderDetailDao orderDetailDao;
    @Autowired
    private OrderService orderService;
    @Autowired
    private CartService cartService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private ShipmentService shipmentService;
    @Autowired
    private PaymentService paymentService;
    @Autowired
    private InvoiceService invoiceService;

    /**
     * @see - com.ideas2it.onlineshopping.service.OrderDetailService
     *      #createOrderDetail(Long cartId, Long shipmentId, Long customerId)
     */
    @Override
    public void createOrderDetail(final Long cartId, final Long shipmentId, final Long customerId, final Long paymentId)
            throws ApplicationException {
        final OrderDetail orderDetail = new OrderDetail();
        final Cart cart = cartService.getCart(cartId);
        final Shipment shipment = shipmentService.getShipment(shipmentId);
        //final Customer customer = customerService.getCustomerById(customerId);
        final Payment payment = paymentService.getPayment(paymentId);
        final Calendar orderDetailTime = cart.getModifiedTime();
        final List<CartDetail> cartDetails = cart.getCartDetails();
        for (final CartDetail cartDetail : cartDetails) {
            //            final Long id = cart.getId();
            final Product product = cartDetail.getProduct();
            final int quantity = cartDetail.getQuantity();
            final double amount = product.getPrice() * quantity;
            orderDetail.setCartId(cartId);
            orderDetail.setProduct(product);
            orderDetail.setQuantity(quantity);
            orderDetail.setAmount(amount);
            orderDetail.setShipment(shipment);
            shipment.setOrderDetail(orderDetail);
            //invoiceService.addInvoice(orderDetail);
            final OrderDetail orderde = orderDetailDao.insertOrderDetail(orderDetail);
        }
        final List<OrderDetail> orderDetails = orderDetailDao.getOrderDetail(cart.getId());
        orderService.insertOrder(orderDetails, customerId, payment);
    }
}
