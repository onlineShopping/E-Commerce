package com.ideas2it.onlineshopping.service.impl;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.dao.ShipmentDao;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Address;
import com.ideas2it.onlineshopping.model.Shipment;
import com.ideas2it.onlineshopping.service.ShipmentService;

/**
 * Implements the Shipment Service with the insert of the shipment details.
 */
@Service(Constant.SHIPMENT_SERVICE)
public class ShipmentServiceImpl implements ShipmentService{

    @Autowired
    private ShipmentDao shipmentDao;

    /**
     * @see - com.ideas2it.onlineshopping.service.shipmentService
     *        #insertShipment(Shipment shipment, Address address)
     */
    @Override
    public Shipment insertShipment(final double price, final Address address) throws ApplicationException {
        final Shipment shipment = new Shipment();
        final Calendar deliveryDate = Calendar.getInstance();
        deliveryDate.add(Calendar.DATE, 7);
        shipment.setDeliveryDate(deliveryDate);
        shipment.setAddress(address);
        shipment.setPrice(price);
        return shipmentDao.insertShipment(shipment);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.shipmentService
     *        #getShipment(final Long shipmentId)
     */
    @Override
    public Shipment getShipment(final Long shipmentId) throws ApplicationException {
        return shipmentDao.getShipment(shipmentId);
    }
}
