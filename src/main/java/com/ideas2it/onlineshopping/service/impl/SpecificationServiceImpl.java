package com.ideas2it.onlineshopping.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.ideas2it.onlineshopping.dao.SpecificationDao;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Specification;
import com.ideas2it.onlineshopping.service.SpecificationService;
import com.ideas2it.onlineshopping.common.Constant;

/**
 * Implements the Specification Management with the insert, retrieve and update
 * of the product.
 *
 * @author Durgadevi Ananthakrishnan
 */
@Service(Constant.SPECIFICATION_SERVICE)
public class SpecificationServiceImpl implements SpecificationService {

    @Autowired
    private SpecificationDao specificationDao;

    /**
     * @see - com.ideas2it.onlineshopping.service.SpecificationService
     *      #addSpecification(Specification)
     */
    public boolean addSpecification(final Specification specification) throws ApplicationException {
        return specificationDao.insertSpecification(specification);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.SpecificationService
     *      #getSpecificationById(String)
     */
    public Specification getSpecificationById(final Long id) throws ApplicationException {
        return specificationDao.retrieveSpecificationById(id);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.SpecificationService
     *      #updateSpecification(Specification)
     */
    public boolean updateSpecification(final Specification specification) throws ApplicationException {
        final Specification specificationDetail = getSpecificationById(specification.getId());
        specification.setProduct(specificationDetail.getProduct());
        return specificationDao.updateSpecification(specificationDetail);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.SpecificationService
     *      #getSpecifications()
     */
    public List<Specification> getAllSpecifications() throws ApplicationException {
        return specificationDao.retrieveAllSpecifications();
    }
}
