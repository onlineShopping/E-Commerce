package com.ideas2it.onlineshopping.service.impl;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.dao.PaymentDao;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Payment;
import com.ideas2it.onlineshopping.service.PaymentService;

/**
 * Implements the payment Service with the create and retrieve operation of the payment.
 */
@Service(Constant.PAYMENT_SERVICE)
public class PaymentServiceImpl implements PaymentService{

    @Autowired
    private PaymentDao paymentDao;

    /**
     * @see - com.ideas2it.onlineshopping.service.paymentService
     *        #insertPayment(Payment payment)
     */
    @Override
    public Payment insertPayment(final Payment payment) throws ApplicationException {
        final Calendar paymentTime = Calendar.getInstance();
        payment.setPaymentTime(paymentTime);
        payment.setPaymentType("CARD");
        return paymentDao.insertPayment(payment);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.paymentService
     *        #getPayment(final Long paymentId)
     */
    @Override
    public Payment getPayment(final Long paymentId) throws ApplicationException {
        return paymentDao.getPayment(paymentId);
    }
}
