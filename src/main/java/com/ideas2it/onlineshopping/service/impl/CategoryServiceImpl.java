package com.ideas2it.onlineshopping.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.dao.CategoryDao;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Category;
import com.ideas2it.onlineshopping.service.CategoryService;
import com.ideas2it.onlineshopping.util.ValidationUtil;

/**
 * Implements the Category Management with the insert, in activate,
 * retrieve and update of the category.
 *
 * @author Durgadevi Ananthakrishnan
 */
@Service(Constant.CATEGORY_SERVICE)
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryDao categoryDao;

    /**
     * @see - com.ideas2it.onlineshopping.service.CategoryService
     *      #addCategory(Category)
     */
    public Category addCategory(final Category category) throws ApplicationException {
        return categoryDao.insertCategory(category);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.CategoryService
     *      #getCategoryById(Long, boolean)
     */
    public Category getCategoryById(final Long id, final boolean isNeededActive) throws ApplicationException {
        return categoryDao.retrieveCategoryById(id, isNeededActive);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.CategoryService
     *      #ActivateCategory(Long)
     */
    public boolean activateCategory(final Long id) throws ApplicationException {
        final Category category = getCategoryById(id, Boolean.FALSE);
        category.setIsActive(Boolean.TRUE);
        return categoryDao.updateCategory(category);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.CategoryService
     *      #DeactivateCategory(Long)
     */
    public boolean deactivateCategory(final Long id) throws ApplicationException {
        final Category category = getCategoryById(id, Boolean.TRUE);
        category.setIsActive(Boolean.FALSE);
        return categoryDao.updateCategory(category);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.CategoryService
     *      #updateCategory(Category, boolean)
     */
    public boolean updateCategory(final Category category, final boolean isNeededActive) throws ApplicationException {
        final Category categoryDetail = getCategoryById(category.getId(), isNeededActive);
        category.setSubCategories(categoryDetail.getSubCategories());
        return categoryDao.updateCategory(category);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.CategoryService
     *      #getAllCategories(boolean)
     */
    public List<Category> getAllCategories(final boolean isNeededAll) throws ApplicationException {
        return categoryDao.retrieveAllCategories(isNeededAll);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.CategoryService
     *      #isDetailExists(String)
     */
    public boolean isCategoryDetailExists(final String columnName, final String detail) throws ApplicationException {
        return categoryDao.isCategoryDetailExists(columnName, detail);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.CategoryService
     *      #isValidCategory(Category)
     */
    public boolean isValidCategory(final Category category) {
        boolean isValid = Boolean.TRUE;
        final List<Boolean> results = new ArrayList<Boolean>();
        results.add(ValidationUtil.isValidAlphaNumeric(category.getName()));
        results.add(ValidationUtil.isValidAlphaNumeric(category.getDescription()));
        for (final Boolean result : results) {
            if (!result) {
                isValid = Boolean.FALSE;
                break;
            }
        }
        return isValid;
    }
}
