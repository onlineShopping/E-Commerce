package com.ideas2it.onlineshopping.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.dao.UserDao;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.User;
import com.ideas2it.onlineshopping.service.UserService;
import com.ideas2it.onlineshopping.util.ValidationUtil;

/**
 * <p>
 * Performs login authentication and update the user credential.
 * </p>
 *
 * @author Ajaykkumar R
 */
@Service(Constant.USER_SERVICE)
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    /**
     * @see com.ideas2it.onlineshopping.service.UserService
     * @method authenticateUser
     */
    @Override
    public User authenticateUser(final String userName, final String password) throws ApplicationException {
        return userDao.authenticateUser(userName, password);
    }

    /**
     * @see com.ideas2it.onlineshopping.service.UserService
     * @method addUser
     */
    @Override
    public void addUser(final User user) throws ApplicationException {
        userDao.createUser(user);
    }

    /**
     * @see com.ideas2it.onlineshopping.service.UserService
     * @method isValidInput
     */
    @Override
    public boolean isValidInput(final User user) {
        boolean result = Boolean.TRUE;
        if (!ValidationUtil.isValidAlphaNumeric(user.getName())) {
            result = Boolean.FALSE;
        }
        if (!ValidationUtil.isValidAlphaNumeric(user.getUserName())) {
            result = Boolean.FALSE;
        }
        if (!ValidationUtil.isValidPhoneNumber(user.getPhoneNumber())) {
            result = Boolean.FALSE;
        }
        return result;
    }

    /**
     * @see com.ideas2it.onlineshopping.service.UserService
     * @method isDetailExists
     */
    @Override
    public boolean isDetailExists(final User user) throws ApplicationException {
        boolean result = Boolean.TRUE;
        if (null != userDao.getUserDetail(Constant.USERNAME, user.getUserName())) {
            result = Boolean.FALSE;
        }
        if (null != userDao.getUserDetail(Constant.EMAILID, user.getEmailId())) {
            result = Boolean.FALSE;
        }
        if (null != userDao.getUser(Constant.PHONE_NUMBER, user.getPhoneNumber())) {
            result = Boolean.FALSE;
        }
        return result;
    }

    /**
     * @see com.ideas2it.onlineshopping.service.UserService
     * @method getUserDetailByEmail
     */
    @Override
    public User getUserDetailByEmail(final String emailId) throws ApplicationException {
        return userDao.getUserDetail(Constant.EMAIL_ID, emailId);
    }
    
    /**
     * @see com.ideas2it.onlineshopping.service.UserService
     * @method getUserById
     */
    @Override
    public User getUserById(final Long id) throws ApplicationException {
        return userDao.getUser(Constant.ID, id);
    }
    
    /**
     * @see com.ideas2it.onlineshopping.service.UserService
     * @method deActivateUser
     */
    @Override
    public void deActivateUser(final Long id) throws ApplicationException {
        final User user = getUserById(id);
        user.setIsActive(Boolean.FALSE);
        userDao.updateUser(user);
    }
    
    /**
     * @see com.ideas2it.onlineshopping.service.UserService
     * @method updatePassword
     */
    @Override
    public User updatePassword(final User user) throws ApplicationException {
        return userDao.updateUser(user);
    }
    
    /**
     * @see com.ideas2it.onlineshopping.service.UserService
     * @method getAllUsers
     */
    @Override
    public List<User> getAllUsers() throws ApplicationException {
        return userDao.retrieveAllUsers();
    }
}
