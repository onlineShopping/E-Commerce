package com.ideas2it.onlineshopping.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.dao.CartDetailDao;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.CartDetail;
import com.ideas2it.onlineshopping.service.CartDetailService;

/**
 * Implements the CartDetail Service with update, and get the cartDetails.
 */
@Service(Constant.CART_DETAIL_SERVICE)
public class CartDetailServiceImpl implements CartDetailService {

    @Autowired
    private CartDetailDao cartDetailDao;

    /**
     * @see - com.ideas2it.onlineshopping.service.CartDetailService
     *      #updateCartDetail(Long cartId, Long productId, int quantity)
     */
    @Override
    public void updateCartDetail(final Long cartId, final Long productId, final int quantity)
            throws ApplicationException {
        final CartDetail cartDetail = getCartDetail(cartId, productId);
        cartDetail.setQuantity(quantity);
        cartDetailDao.updateCartDetail(cartDetail);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.CartDetailService
     *      #deleteCartDetail(Long cartId, Long productId, boolean IsActive)
     */
    @Override
    public boolean deleteCartDetail(final Long cartId, final Long productId) throws ApplicationException {
        final CartDetail cartDetail = getCartDetail(cartId, productId);
        return cartDetailDao.deleteCartDetail(cartDetail);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.CartDetailService
     *      #deleteCartDetail(CartDetail cartDetail)
     */
    @Override
    public boolean deleteCartDetail(final CartDetail cartDetail) throws ApplicationException {
        return cartDetailDao.deleteCartDetail(cartDetail);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.CartDetailService
     *      #CartDetail getCartDetail(final Long cartId, final Long productId)
     */
    @Override
    public CartDetail getCartDetail(final Long cartId, final Long productId) throws ApplicationException {
        return cartDetailDao.getCartDetail(cartId, productId);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.CartDetailService
     *      #CartDetail getCartDetail(final Long cartId, final Long productId)
     */
    @Override
    public List<CartDetail> getCartDetailByCartId(final Long cartId) throws ApplicationException {
        return cartDetailDao.getCartDetailByCartId(cartId);
    }

    @Override
    public void insertCartDetail(final CartDetail cartDetail) throws ApplicationException {
        cartDetailDao.insertCartDetail(cartDetail);

    }
}
