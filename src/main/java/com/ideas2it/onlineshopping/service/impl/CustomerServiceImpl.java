package com.ideas2it.onlineshopping.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.dao.CustomerDao;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Customer;
import com.ideas2it.onlineshopping.model.User;
import com.ideas2it.onlineshopping.service.CustomerService;

/**
 * <p>
 * Performs customer CRUD operations.
 * </p>
 *
 * @author Ajaykkumar R
 */
@Service(Constant.CUSTOMER_SERVICE)
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;

    /**
     * @see com.ideas2it.onlineshopping.service.CustomerService
     * @method createCustomer
     */
    @Override
    public void createCustomer(final User user) throws ApplicationException {
        final Customer customer = new Customer();
        customer.setUser(user);
        user.setCustomer(customer);
        customerDao.insertCustomer(customer);
    }

    /**
     * @see com.ideas2it.onlineshopping.service.CustomerService
     * @method getCustomerById
     */
    @Override
    public Customer getCustomerById(final Long id) throws ApplicationException {
        return customerDao.getCustomer(Constant.ID, id);
    }
    
    /**
     * @see com.ideas2it.onlineshopping.service.CustomerService
     * @method getCustomerByUserId
     */
    @Override
    public Customer getCustomerByUserId(final Long id) throws ApplicationException {
        return customerDao.getCustomer("user.id", id);
    }
    
    /**
     * @see com.ideas2it.onlineshopping.service.CustomerService
     * @method getCustomerByEmailId
     */
    @Override
    public Customer getCustomerByEmailId(final String emailId) throws ApplicationException {
        return customerDao.getCustomerByEmailId(emailId);
    }

    /**
     * @see com.ideas2it.onlineshopping.service.CustomerService
     * @method updateCustomer
     */
    @Override
    public void updateCustomer(final Long id, final String name, final String emailId, final Long phoneNumber)
            throws ApplicationException {
        final Customer customer = getCustomerById(id);
        final User user = customer.getUser();
        user.setName(name);
        user.setEmailId(emailId);
        user.setPhoneNumber(phoneNumber);
        customer.setUser(user);
        customerDao.updateCustomer(customer);
    }

    /**
     * @see com.ideas2it.onlineshopping.service.CustomerService
     * @method deActivateCustomer
     */
    @Override
    public void deActivateCustomer(final Long id) throws ApplicationException {
        final Customer customer = getCustomerById(id);
        customer.setIsActive(Boolean.FALSE);
        customerDao.updateCustomer(customer);
    }

    /**
     * @see com.ideas2it.onlineshopping.service.CustomerService
     * @method getAllCustomers
     */
    @Override
    public List<Customer> getAllCustomers() throws ApplicationException {
        return customerDao.retrieveAllCustomers();
    }
}
