package com.ideas2it.onlineshopping.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.dao.CartDao;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Cart;
import com.ideas2it.onlineshopping.model.CartDetail;
import com.ideas2it.onlineshopping.model.Customer;
import com.ideas2it.onlineshopping.model.Product;
import com.ideas2it.onlineshopping.service.CartDetailService;
import com.ideas2it.onlineshopping.service.CartService;
import com.ideas2it.onlineshopping.service.CustomerService;
import com.ideas2it.onlineshopping.service.ProductService;

/**
 * Implements the Cart Service with create, update,
 * delete and get cart details.
 */
@Service(Constant.CART_SERVICE)
public class CartServiceImpl implements CartService {

    @Autowired
    private CartDao cartDao;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private ProductService productService;

    @Autowired
    private CartDetailService cartDetailService;

    /**
     * @see - com.ideas2it.onlineshopping.service.CartService
     *      #createCart(final Long productId, final Long customerId)
     */
    @Override
    public void createCart(final Long productId, final Long customerId) throws ApplicationException {
        final Cart cart = new Cart();
        final CartDetail cartDetail = new CartDetail();
        final Customer customer = customerService.getCustomerById(customerId);
        System.out.println(customer);
        final Product product = productService.getProductById(productId, Boolean.FALSE);
        cartDetail.setProduct(product);
        cartDetail.setCart(cart);
        final List<CartDetail> cartDetails = new ArrayList<>();
        cartDetails.add(cartDetail);
        cart.setCartDetails(cartDetails);
        cart.setCustomer(customer);
        cartDao.insertCart(cart);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.CartService
     *      #updateCart(Long productId, Long customerId)
     */
    @Override
    public void updateCart(final Cart cart, final Product product, final Customer customer) throws ApplicationException {
        final CartDetail cartDetail = new CartDetail();
        final List<CartDetail> cartDetails = cart.getCartDetails();
        cartDetail.setProduct(product);
        System.out.println("dsfffffffffffffffffffffffffffffffff");
        cartDetail.setCart(cart);

        cartDetailService.insertCartDetail(cartDetail);
        /*cartDetails.add(cartDetail);
        System.out.println("dsfffffffffffffffffffffffffffffffff");
        cart.setCartDetails(cartDetails);
        System.out.println("dsfffffffffffffffffffffffffffffffff");
        System.out.println("dsfffffffffffffffffffffffffffffffff");
        cartDao.updateCart(cart);*/
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.CartService
     *      #deleteCart(Long cartId)
     */
    @Override
    public void deleteCart(final Long cartId) throws ApplicationException {
        cartDao.deleteCart(cartId);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.CartService
     *      #getCartByCustomer(final Long customerId)
     */
    @Override
    public Cart getCartByCustomer(final Long customerId) throws ApplicationException {
        return cartDao.getCartByCustomer(customerId);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.CartService
     *      #Cart getCart(Long cartId)
     */
    @Override
    public Cart getCart(final Long cartId) throws ApplicationException {
        return cartDao.getCart(cartId);
    }
}
