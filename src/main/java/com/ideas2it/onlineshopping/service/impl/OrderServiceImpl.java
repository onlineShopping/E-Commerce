package com.ideas2it.onlineshopping.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.dao.OrderDao;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Customer;
import com.ideas2it.onlineshopping.model.Order;
import com.ideas2it.onlineshopping.model.OrderDetail;
import com.ideas2it.onlineshopping.model.Payment;
import com.ideas2it.onlineshopping.service.CustomerService;
import com.ideas2it.onlineshopping.service.OrderService;

@Service(Constant.ORDER_DETAIL_SERVICE)
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDao orderDao;
    @Autowired
    private CustomerService customerService;

    /**
     * @see - com.ideas2it.onlineshopping.service.OrderService
     *      #insertOrder(final OrderDetail orderDetail, final Long customerId, Payment payment)
     */
    @Override
    public void insertOrder(final List<OrderDetail> orderDetails, final Long customerId, final Payment payment)
            throws ApplicationException {
        //        final Order order = orderDao.getOrder(customerId);
        final Order order = new Order();
        final Customer customer = customerService.getCustomerById(customerId);
        order.setPayment(payment);
        order.setOrderDetails(orderDetails);
        order.setCustomer(customer);
        for (final OrderDetail orderDetail : orderDetails) {
            orderDetail.setOrder(order);
        }
        payment.setOrder(order);
        orderDao.insertOrder(order);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.OrderService
     *      #getOrders(Long customerId)
     */
    @Override
    public List<Order> getOrders(final Long customerId) throws ApplicationException {
        return orderDao.getOrders(customerId);
    }
}
