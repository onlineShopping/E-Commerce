package com.ideas2it.onlineshopping.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.dao.InvoiceDao;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Invoice;
import com.ideas2it.onlineshopping.model.OrderDetail;
import com.ideas2it.onlineshopping.service.InvoiceService;

/**
 * Implements the invoice Service with the create and retrieve operation of the invoice.
 */
@Service(Constant.INVOICE_SERVICE)
public class InvoiceServiceImpl implements InvoiceService {

    @Autowired
    private InvoiceDao invoiceDao;

    /**
     * @see - com.ideas2it.onlineshopping.service.InvoiceService
     *      #addInvoice(Invoice)
     */
    @Override
    public void addInvoice(final OrderDetail orderDetail) throws ApplicationException {
        final Invoice invoice = new Invoice();
        invoice.setInvoiceDate(orderDetail.getOrderTime());
        invoice.setOrderDate(orderDetail.getOrderTime());
        invoice.setOrderDetail(orderDetail);
        invoiceDao.insertInvoice(invoice);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.InvoiceService
     *      #getInvoiceById(String)
     */
    @Override
    public Invoice getInvoiceById(final Long id) throws ApplicationException {
        return invoiceDao.retrieveInvoiceById(id);
    }

}
