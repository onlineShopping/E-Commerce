package com.ideas2it.onlineshopping.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.dao.SubCategoryDao;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.SubCategory;
import com.ideas2it.onlineshopping.service.SubCategoryService;
import com.ideas2it.onlineshopping.util.ValidationUtil;

/**
 * Implements the Sub Category Management with the insert, in activate,
 * retrieve and update of the sub category.
 *
 * @author Durgadevi Ananthakrishnan
 */
@Service(Constant.SUBCATEGORY_SERVICE)
public class SubCategoryServiceImpl implements SubCategoryService {

    @Autowired
    private SubCategoryDao subCategoryDao;

    /**
     * @see - com.ideas2it.onlineshopping.service.SubCategoryService
     *      #addSubCategory(SubCategory)
     */
    @Override
    public SubCategory addSubCategory(final SubCategory subCategory) throws ApplicationException {
        return subCategoryDao.insertSubCategory(subCategory);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.SubCategoryService
     *      #getSubCategoryById(Long, boolean)
     */
    @Override
    public SubCategory getSubCategoryById(final Long id, final boolean isNeededActive) throws ApplicationException {
        return subCategoryDao.retrieveSubCategoryById(id, isNeededActive);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.SubCategoryService
     *      #ActivateSubCategory(Long)
     */
    @Override
    public boolean activateSubCategory(final Long id) throws ApplicationException {
        final SubCategory subCategory = getSubCategoryById(id, Boolean.FALSE);
        subCategory.setIsActive(Boolean.TRUE);
        return subCategoryDao.updateSubCategory(subCategory);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.SubCategoryService
     *      #DeactivateSubCategory(Long)
     */
    @Override
    public boolean deactivateSubCategory(final Long id) throws ApplicationException {
        final SubCategory subCategory = getSubCategoryById(id, Boolean.TRUE);
        subCategory.setIsActive(Boolean.FALSE);
        return subCategoryDao.updateSubCategory(subCategory);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.SubCategoryService
     *      #updateSubCategory(SubCategory, boolean)
     */
    @Override
    public boolean updateSubCategory(final SubCategory subCategory, final boolean isNeededActive)
            throws ApplicationException {
        final SubCategory subCategoryDetail = getSubCategoryById(subCategory.getId(), isNeededActive);
        subCategory.setCategory(subCategoryDetail.getCategory());
        subCategory.setProducts(subCategoryDetail.getProducts());
        return subCategoryDao.updateSubCategory(subCategory);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.SubCategoryService
     *      #getAllSubCategories(boolean)
     */
    @Override
    public List<SubCategory> getAllSubCategories(final boolean isNeededAll) throws ApplicationException {
        return subCategoryDao.retrieveAllSubCategories(isNeededAll);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.SubCategoryService
     *      #isSubCategoryDetailExists(String)
     */
    @Override
    public boolean isSubCategoryDetailExists(final String columnName, final String detail) throws ApplicationException {
        return subCategoryDao.isSubCategoryDetailExists(columnName, detail);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.SubCategoryService
     *      #isValidSubCategory(SubCategory)
     */
    @Override
    public boolean isValidSubCategory(final SubCategory subCategory) {
        boolean isValid = Boolean.TRUE;
        final List<Boolean> results = new ArrayList<>();
        results.add(ValidationUtil.isValidAlphaNumeric(subCategory.getName()));
        results.add(ValidationUtil.isValidAlphaNumeric(subCategory.getDescription()));
        for (final Boolean result : results) {
            if (!result) {
                isValid = Boolean.FALSE;
                break;
            }
        }
        return isValid;
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.SubCategoryService
     *      #getSubCategoryByName(String)
     */
    @Override
    public SubCategory getSubCategoryByName(final String name) throws ApplicationException {
        return subCategoryDao.retrieveSubCategoryByName(name);
    }
}
