package com.ideas2it.onlineshopping.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.dao.ProductDao;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Product;
import com.ideas2it.onlineshopping.model.ProductSeller;
import com.ideas2it.onlineshopping.model.Specification;
import com.ideas2it.onlineshopping.service.ProductService;
import com.ideas2it.onlineshopping.util.ValidationUtil;

/**
 * Implements the Product Management with the insert, in activate,
 * retrieve and update of the product.
 *
 * @author Durgadevi Ananthakrishnan
 */
@Service(Constant.PRODUCT_SERVICE)
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductDao productDao;

    /**
     * @see - com.ideas2it.onlineshopping.service.ProductService
     *      #addProduct(Product)
     */
    @Override
    public Product addProduct(final Product product) throws ApplicationException {
        Specification specification = product.getSpecification();
        specification.setProduct(product);
        return productDao.insertProduct(product);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.ProductService
     *      #getProductById(String, boolean)
     */
    @Override
    public Product getProductById(final Long id, final boolean isNeededActive) throws ApplicationException {
        return productDao.retrieveProductById(id, isNeededActive);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.ProductService
     *      #getProductBySellerssId(String, boolean)
     */
    @Override
    public List<ProductSeller> getProductSellersBySellerId(final Long sellerId) throws ApplicationException {
        return productDao.retrieveProductSellersBySellerId(sellerId);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.ProductService
     *      #activateProduct(Long)
     */
    @Override
    public boolean activateProduct(final Long id) throws ApplicationException {
        final Product product = getProductById(id, Boolean.FALSE);
        product.setIsActive(Boolean.TRUE);
        return productDao.updateProduct(product);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.ProductService
     *      #deactivateProduct(Long)
     */
    @Override
    public boolean deactivateProduct(final Long id) throws ApplicationException {
        final Product product = getProductById(id, Boolean.TRUE);
        product.setIsActive(Boolean.FALSE);
        return productDao.updateProduct(product);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.ProductService
     *      #updateProduct(Product, boolean)
     */
    @Override
    public boolean updateProduct(final Product product, final boolean isNeededActive) throws ApplicationException {
        final Product productDetail = getProductById(product.getId(), isNeededActive);
        product.setSubCategory(productDetail.getSubCategory());
        product.setCategory(productDetail.getCategory());
        product.setSpecification(productDetail.getSpecification());
        return productDao.updateProduct(product);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.ProductService
     *      #getAllProducts(boolean)
     */
    @Override
    public List<Product> getAllProducts(final boolean isNeededAll) throws ApplicationException {
        return productDao.retrieveAllProducts(isNeededAll);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.ProductService
     *      #isDetailExists(String)
     */
    @Override
    public boolean isProductDetailExists(final String columnName, final String detail) throws ApplicationException {
        return productDao.isProductDetailExists(columnName, detail);
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.ProductService
     *      #isValidProduct(Product)
     */
    @Override
    public boolean isValidProduct(final Product product) {
        boolean isValid = Boolean.TRUE;
        final List<Boolean> results = new ArrayList<>();
        results.add(ValidationUtil.isValidAlphaNumeric(product.getName()));
        results.add(ValidationUtil.isValidAlphaNumeric(product.getDescription()));
        for (final Boolean result : results) {
            if (!result) {
                isValid = Boolean.FALSE;
                break;
            }
        }
        return isValid;
    }

   /**
    * @see - com.ideas2it.onlineshopping.service.ProductService
    *      #getAllProducts(boolean)
    */
   @Override
   public List<Product> getAllSearchProducts(final String name) throws ApplicationException {
       return productDao.retrieveAllSearchProducts(name);
   }

}
