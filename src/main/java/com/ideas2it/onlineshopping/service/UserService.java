package com.ideas2it.onlineshopping.service;

import java.util.List;

import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.User;

/**
 * <p>
 * User interface which defines login authentication and updation of user credential
 * operations to be performed.
 * </p>
 *
 * @author Ajaykkumar R
 */
public interface UserService {

    /**
     * <p>
     * Check the user credentials with database and forwards to the
     * corresponding pages.
     * </p>
     *
     * @param userName
     *        Username given by the user.
     * @param password
     *        Password given by the user.
     * @return user
     *        User details for the corressponding username and password.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    User authenticateUser(String userName, String password) throws ApplicationException;

    /**
     * <p>
     * Add the user credentials in the database.
     * </p>
     *
     * @param user
     *        Details of the user.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    void addUser(User user) throws ApplicationException;
    
    /**
     * <p>
     * Get the input from the user and validate it.
     * </p>
     *  
     * @param user
     *        Details of the user.
     * @return boolean
     *        True if it matches the pattern,False if not.
     */
    boolean isValidInput(final User user);
    
    /**
     * <p>
     * Check whether the details already existed or not.
     * </p>
     * 
     * @param user
     *        Details of the user.
     * @return boolean
     *        True if it is unique, False if it is already present. 
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    boolean isDetailExists(final User user) throws ApplicationException;
    
    /**
     * <p>
     * Get the user details by emailid from the database.
     * </p>
     *
     * @param emailId
     *        Email id of the user.
     * @return user
     *        Details of the user.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    User getUserDetailByEmail(String emailId) throws ApplicationException;
    
    /**
     * <p>
     * Get user details by user id.
     * </p>
     * 
     * @param id
     *        Id of the user.
     * @return user
     *        Details of the user.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    User getUserById(final Long id) throws ApplicationException;
    
    /**
     * <p>
     * Deactivate the user by user id.
     * </p>
     * 
     * @param id
     *        Id of the user.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    void deActivateUser(final Long id) throws ApplicationException;
    
    /**
     * <p>
     * Update the password of the user in the database.
     * </p>
     *
     * @param user
     *        Details of the user.
     * @return user
     *        Details of the updated user.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    User updatePassword(User user) throws ApplicationException;
    
    /**
     * <p>
     * Get all user details from database.
     * </p>
     * 
     * @return users
     *         Details of all the users.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    List<User> getAllUsers() throws ApplicationException;
}
