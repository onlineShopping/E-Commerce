package com.ideas2it.onlineshopping.service;

import com.ideas2it.onlineshopping.exception.ApplicationException;

/**
 * Implements the OrderDetail service with the create and retrieve of the orderDetail.
 */
public interface OrderDetailService {

    /**
     * Implemented to add the cart with the orderDetail details like orderDetail, cart and
     * customer into the orderDetail.
     *
     * @param cartId
     *            - Id of the cart.
     * @param shipmentId
     *            - Id of the shipment.
     * @param customerId
     *            - Id of the customer.
     * @param paymentId
     *            - Id of the payment
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    void createOrderDetail(Long cartId, final Long shipmentId, final Long customerId,
            final Long paymentId) throws ApplicationException;
}
