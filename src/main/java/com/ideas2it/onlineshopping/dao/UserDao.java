package com.ideas2it.onlineshopping.dao;

import java.util.List;

import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.User;

/**
 * <p>
 * Performs login authentication and updation of user credential in the database.
 * </p>
 *
 * @author Ajaykkumar R
 */
public interface UserDao {

    /**
     * <p>
     * Create the user credentials in the database.
     * </p>
     *
     * @param user
     *        Details of the user.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    void createUser(User user) throws ApplicationException;

    /**
     * <p>
     * Authenticate the user credentials and forward to corresponding
     * page.
     * </p>
     *
     * @param userName
     *        The username and password of the user.
     * @param password
     *        The login details of the user.
     * @return user
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    User authenticateUser(String userName, String password) throws ApplicationException;

    /**
     * <p>
     * Get the user credentials from the database.
     * </p>
     *
     * @param key
     *        Username or emailid.
     * @param value
     *        Username or emailid of the user.
     * @return User
     *        Details of the user.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    User getUserDetail(final String key, final String value) throws ApplicationException;
    
    /**
     * <p>
     * Get the user credentials from the database.
     * </p>
     *
     * @param key
     *        Id or Phone number.
     * @param value
     *        Id or Phone number of the user.
     * @return user
     *        Details of the user.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    User getUser(final String key, final Long value) throws ApplicationException;
    
    /**
     * <p>
     * Update the user details in the database.
     * </p>
     *
     * @param user
     *        Details of the user.
     * @return user
     *        Details of the updated user.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    User updateUser(User user) throws ApplicationException;
    
    /**
     * <p>
     * Retrieve details of all users from the database. 
     * </p>
     * 
     * @return users
     *        Details of all users.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    List<User> retrieveAllUsers() throws ApplicationException;

}
