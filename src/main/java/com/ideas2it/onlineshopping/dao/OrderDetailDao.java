package com.ideas2it.onlineshopping.dao;

import java.util.List;

import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.OrderDetail;

public interface OrderDetailDao {

    /**
     * Implemented to insert orderDetail details into database
     *
     * @param orderDetail
     *            - OrderDetail contains details like id, amount, customer details
     * @return orderDetail
     *         - Returns details of orderDetail like id, product, quantity,
     *         amount, customer, shipment, orderDetail time.
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    OrderDetail insertOrderDetail(OrderDetail orderDetail) throws ApplicationException;

    /**
     * Implemented to get orderDetail details from the database
     *
     * @param orderDetailId
     *              - Id of orderDetail
     * @return orderDetails
     *             - returns list of orderDetails contains details like id, product,
     *             quantity, amount, customer, shipment, orderDetail time.
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    List<OrderDetail> getOrderDetail(final Long orderDetailId) throws ApplicationException;
}
