package com.ideas2it.onlineshopping.dao;

import java.util.List;

import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Specification;

/**
 * Implements the Specification Management with the insert, retrieve and update
 * of the specification.
 *
 * @author Durgadevi Ananthakrishnan
 */
public interface SpecificationDao {

    /**
     * Retrieve all the specifications with the specification details from the
     * specification.
     *
     * @return - specifications, all specifications available in the specification.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    List<Specification> retrieveAllSpecifications() throws ApplicationException;

    /**
     * Update the specification with the user given updated details of the
     * specification.
     *
     * @param specification
     *            - the specification, the specification to be updated with the user
     *            given updated details.
     * @return Boolean.TRUE
     *         - true if the specification is updated successfully else throws
     *         exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    boolean updateSpecification(Specification specification) throws ApplicationException;

    /**
     * Retrieve the specification from the specification by providing the id of the
     * specification.
     *
     * @param id
     *            - id of the specification, to retrieve the specification from the
     *            specification.
     * @return Boolean.TRUE
     *         - true if the specification is retrieved successfully else throws
     *         exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    Specification retrieveSpecificationById(Long id) throws ApplicationException;

    /**
     * Insert the specification with the specification details like id, name and
     * description
     * into the specification.
     *
     * @param specification
     *            - the specification, the specification to be inserted with the
     *            user given details.
     * @return Boolean.TRUE
     *         - true if the specification is inserted successfully else throws
     *         exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    boolean insertSpecification(Specification specification) throws ApplicationException;
}
