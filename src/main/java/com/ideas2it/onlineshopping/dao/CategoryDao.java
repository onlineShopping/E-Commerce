package com.ideas2it.onlineshopping.dao;

import java.util.List;

import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Category;

/**
 * Implements the Category Management with the insert, in activate,
 * retrieve and update of the category.
 *
 * @author Durgadevi Ananthakrishnan
 */
public interface CategoryDao {

    /**
     * Retrieve all the categories with the category details from the category.
     *
     * @param isNeededAll
     *            - true if all the categories are needed else false.
     * @return - categories, all categories available in the category.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    List<Category> retrieveAllCategories(boolean isNeededAll) throws ApplicationException;

    /**
     * Check whether the category detail like the id and name is exited or not.
     *
     * @param columnName
     *            - column name of the detail, to check the existence of detail.
     * @param detail
     *            - the detail of the category, to check its existence.
     * @return boolean
     *         - true if the detail is already existed in the category else false.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    boolean isCategoryDetailExists(String columnName, String detail) throws ApplicationException;

    /**
     * Update the category with the user given updated details of the category.
     *
     * @param category
     *            - the category, the category to be updated with the user given
     *            updated details.
     * @return Boolean.TRUE
     *         - true if the category is updated successfully else throws exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    boolean updateCategory(Category category) throws ApplicationException;

    /**
     * Retrieve the category from the category by providing the id of the category.
     *
     * @param id
     *            - id of the category, to retrieve the category from the category.
     * @param isNeededActive
     *            - true if active category is needed else false.
     * @return Boolean.TRUE
     *         - true if the category is retrieved successfully else throws
     *         exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    Category retrieveCategoryById(Long id, boolean isNeededActive) throws ApplicationException;

    /**
     * Insert the category with the category details like id, name and description
     * into the category.
     *
     * @param category
     *            - the category, the category to be inserted with the user given
     *            details.
     * @return Category
     *         - category if the category is inserted successfully else throws
     *         exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    Category insertCategory(Category category) throws ApplicationException;
}
