package com.ideas2it.onlineshopping.dao;

import java.util.List;

import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Seller;

/**
 * Seller dao interface which defines CRUD operations to be performed in database.
 *
 * @author Ajaykkumar R
 */
public interface SellerDao {

    /**
     * <p>
     * Inserts the seller details into the database.
     * </p>
     *
     * @param seller
     *        Seller details to be inserted into the database.
     * @return seller
     *        Details of the seller.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    Seller insertSeller(Seller seller) throws ApplicationException;

    /**
     * <p>
     * Updates the details of the seller.
     * </p>
     *
     * @param seller
     *        Details of the seller to be updated.
     * @return seller
     *        Details of the seller.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    Seller updateSeller(Seller seller) throws ApplicationException;

    /**
     * <p>
     * Get the seller detail from the database.
     * </p>
     *
     * @param emailId
     *        Emailid of the seller.
     * @return seller
     *        Details of the seller for the corressponding emailid.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    Seller getSellerByEmailId(String emailId) throws ApplicationException;

    /**
     * <p>
     * Get the seller details by id from the database.
     * </p>
     *
     * @param key
     *        Userid or id.
     * @param value
     *        Userid or id of the seller.
     * @return seller
     *        Details of the seller for the corresponding seller id.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    Seller getSeller(String key, Long value) throws ApplicationException;

    /**
     * <p>
     * Get all the seller details from the database.
     * </p>
     *
     * @return sellers
     *        List of all seller and their details.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    List<Seller> retrieveAllSellers() throws ApplicationException;
}
