package com.ideas2it.onlineshopping.dao;

import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Payment;

/**
 * Implements the payment Service with the insert and retrieve of the payment.
 */
public interface PaymentDao {

    /**
     * Implemented to insert payment details into database
     *
     * @param payment
     *            - contains details like id, payment type and shipment details
     * @return payment - returns details of payment like id, payment type, payment
     *         time, card number.
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    Payment insertPayment(Payment payment) throws ApplicationException;

    /**
     * Implemented to get payment details from the database
     *
     * @param paymentId
     *            - Id of the payment
     * @return payment - returns details of payment like id, payment type, payment
     *         time, card number.
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    Payment getPayment(Long paymentId) throws ApplicationException;

}
