package com.ideas2it.onlineshopping.dao;

import java.util.List;

import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Address;

/**
 * Address dao interface which defines CRUD operations to be performed in database.
 *
 * @author Ajaykkumar R
 */
public interface AddressDao {

    /**
     * <p>
     * Inserts the address details into the database.
     * </p>
     *
     * @param address
     *        Address details that is to be inserted in the database.
     * @returns boolean
     *        True if it is inserted successfully,False if it failed.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    boolean insertAddress(Address address) throws ApplicationException;

    /**
     * <p>
     * Update the address details into the database.
     * </p>
     *
     * @param address
     *        Address details to be updated in the database.
     * @returns boolean
     *        True if it is updated successfully,False if it failed.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    boolean updateAddress(Address address) throws ApplicationException;

    /**
     * <p>
     * Get the addresses from the table in the database by customer id.
     * </p>
     *
     * @param id
     *        Id of the customer.
     * @return addresses
     *        List of addresses for corresponding customer id.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    List<Address> getAddressesByCustomerId(Long id) throws ApplicationException;

    /**
     * <p>
     * Get the address from the table in the database.
     * </p>
     *
     * @param id
     *        Id of the address.
     * @return address
     *        Address details for the corresponding address id.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    Address getAddressById(Long id) throws ApplicationException;
}