package com.ideas2it.onlineshopping.dao;

import java.util.List;

import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.CartDetail;

/**
 * Implements the CartDetail Dao with the update and get the cartDetails.
 */
public interface CartDetailDao {

    /**
     * Update an cart from the carts
     *
     * @param cartDetail
     *            - CartDetail contains details like id, product, quantity and cart
     * @return cartDetail
     *         - CartDetail return details like id, product, cart and quantity.
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    CartDetail updateCartDetail(final CartDetail cartDetail) throws ApplicationException;

    /**
     * Gets the cartDetails information from the database.
     *
     * @param cartId
     *            - Id of the cart
     * @param productId
     *            - Id of the product is used to retrieve cartDetail
     *            respect to the product id
     * @return cartDetail -
     *         details of cartDetail retrieved respect to product id
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    CartDetail getCartDetail(final Long cartId, final Long productId) throws ApplicationException;

    List<CartDetail> getCartDetailByCartId(final Long cartId) throws ApplicationException;

    void insertCartDetail(CartDetail cartDetail) throws ApplicationException;

    /**
     * Delete an cart from the carts
     *
     * @param cartDetail
     *            - CartDetail contains details like id, product, quantity and cart
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    boolean deleteCartDetail(CartDetail cartDetail) throws ApplicationException;
}
