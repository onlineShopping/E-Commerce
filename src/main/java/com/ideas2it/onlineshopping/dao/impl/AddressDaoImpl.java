package com.ideas2it.onlineshopping.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.dao.AddressDao;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Address;
import com.ideas2it.onlineshopping.util.StringUtil;

/**
 * <p>
 * Performs address CRUD for seller and customer in the database.
 * </p>
 * 
 * @author Ajaykkumar R
 */
@Repository(Constant.ADDRESS_DAO)
public class AddressDaoImpl implements AddressDao {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * @see com.ideas2it.onlineshopping.dao.AddressDao
     * @method insertAddress
     */
    public boolean insertAddress(Address address) throws ApplicationException {
        boolean isInserted = Boolean.TRUE;
        Transaction transaction = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.save(address);
            transaction.commit();
        } catch (HibernateException e) {
            if(null != transaction) {
                transaction.rollback();
            }
            isInserted = Boolean.FALSE;
            throw new ApplicationException(
                    StringUtil.concatenate(Constant.EX_INSERT, Constant.ADDRESS, Long.toString(address.getId())), e);
        } finally {
            session.close();
        }
        return isInserted;
    }

    /**
     * @see com.ideas2it.onlineshopping.dao.AddressDao
     * @method updateAddress
     */
    public boolean updateAddress(Address address) throws ApplicationException {
        boolean isUpdated = Boolean.TRUE;
        Transaction transaction = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.update(address);
            transaction.commit();
        } catch (HibernateException e) {
            if(null != transaction) {
                transaction.rollback();
            }
            isUpdated = Boolean.FALSE;
            throw new ApplicationException(
                    StringUtil.concatenate(Constant.EX_UPDATE, Constant.ADDRESS, Long.toString(address.getId())), e);
        } finally {
            session.close();
        }
        return isUpdated;
    }

    /**
     * @see com.ideas2it.onlineshopping.dao.AddressDao
     * @method getAddressByCustomerId
     */
    public List<Address> getAddressesByCustomerId(Long id) throws ApplicationException {
        List<Address> addresses = new ArrayList<>();
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Criteria criteria = session.createCriteria(Address.class).add(Restrictions.eq(Constant.ID, id));
            addresses = criteria.list();
        } catch (HibernateException e) {
            throw new ApplicationException(StringUtil.concatenate(Constant.EX_SEARCH, Constant.ADDRESS,
                    Constant.CUSTOMER, Long.toString(id)), e);
        } finally {
            session.close();
        }
        return addresses;
    }

    /**
     * @see com.ideas2it.onlineshopping.dao.AddressDao
     * @method getAddressById
     */
    public Address getAddressById(Long id) throws ApplicationException {
        Address address;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Criteria criteria = session.createCriteria(Address.class).add(Restrictions.eq(Constant.ID, id));
            address = (Address) criteria.uniqueResult();
        } catch (HibernateException e) {
            throw new ApplicationException(
                    StringUtil.concatenate(Constant.EX_SEARCH, Constant.ADDRESS, Long.toString(id)), e);
        } finally {
            session.close();
        }
        return address;
    }
}
