package com.ideas2it.onlineshopping.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.dao.ShipmentDao;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Shipment;
import com.ideas2it.onlineshopping.util.StringUtil;

/**
 * Implements the Shipment Dao with the insert of the shipment.
 */
@Repository(Constant.SHIPMENT_DAO)
public class ShipmentDaoImpl implements ShipmentDao {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * @see - com.ideas2it.onlineshopping.service.shipmentDao
     *        #insertShipment(Shipment shipment)
     */
    @Override
    public Shipment insertShipment(final Shipment shipment) throws ApplicationException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.save(shipment);
            transaction.commit();
        } catch (final HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            }
            throw new ApplicationException(StringUtil.concatenate(Constant.EXCEPTION_WHILE, Constant.INSERTING,
                    Constant.SHIPMENT_ID, String.valueOf(shipment.getId())), exception);
        } finally {
            session.close();
        }
        return shipment;
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.shipmentDao
     *        #getShipment(int shipmentId)
     */
    @Override
    public Shipment getShipment(final Long shipmentId) throws ApplicationException {
        Session session = null;
        Shipment shipment = null;
        try {
            session = sessionFactory.openSession();
            final Criteria criteria = session.createCriteria(Shipment.class);
            criteria.add(Restrictions.eq("id", shipmentId));
            shipment = (Shipment) criteria.uniqueResult();
        } catch (final HibernateException exception) {
            throw new ApplicationException(StringUtil.concatenate(Constant.EXCEPTION_WHILE, Constant.INSERTING,
                    Constant.SHIPMENT_ID, String.valueOf(shipment.getId())), exception);
        } finally {
            session.close();
        }
        return shipment;
    }
}
