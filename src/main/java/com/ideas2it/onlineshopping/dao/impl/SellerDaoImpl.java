package com.ideas2it.onlineshopping.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.dao.SellerDao;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Seller;
import com.ideas2it.onlineshopping.util.StringUtil;

/**
 * <p>
 * Performs seller CRUD operation in the database.
 * </p>
 * 
 * @author Ajaykkumar R
 */
@Repository
public class SellerDaoImpl implements SellerDao {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * @see com.ideas2it.onlineshopping.dao.SellerDao
     * @method insertSeller
     */
    @Override
    public Seller insertSeller(final Seller seller) throws ApplicationException {
        Transaction transaction = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.save(seller);
            transaction.commit();
        } catch (final HibernateException e) {
            if (null != transaction) {
                transaction.rollback();
            }
            throw new ApplicationException(
                    StringUtil.concatenate(Constant.EX_INSERT, Constant.SELLER, Long.toString(seller.getId())), e);
        } finally {
            session.close();
        }
        return seller;
    }

    /**
     * @see com.ideas2it.onlineshopping.dao.SellerDao
     * @method getSeller
     */
    @Override
    public Seller getSellerByEmailId(final String emailId) throws ApplicationException {
        Seller seller = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            final Criteria criteria =
                    session.createCriteria(Seller.class).add(Restrictions.eq(Constant.EMAIL_ID, emailId));
            seller = (Seller) criteria.uniqueResult();
        } catch (final HibernateException e) {
            throw new ApplicationException(StringUtil.concatenate(Constant.EX_SEARCH, Constant.SELLER, emailId), e);
        } finally {
            session.close();
        }
        return seller;
    }

    /**
     * @see com.ideas2it.onlineshopping.dao.SellerDao
     * @method getSellerByPhone
     */
    @Override
    public Seller getSeller(final String key, final Long value) throws ApplicationException {
        Seller seller = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            final Criteria criteria = session.createCriteria(Seller.class)
                    .add(Restrictions.eq(Constant.ISACTIVE, Boolean.TRUE)).add(Restrictions.eq(key, value));
            seller = (Seller) criteria.uniqueResult();
        } catch (final HibernateException e) {
            throw new ApplicationException(StringUtil.concatenate(Constant.EX_SEARCH, Constant.SELLER, Long.toString(value)), e);
        } finally {
            session.close();
        }
        return seller;
    }

    /**
     * @see com.ideas2it.onlineshopping.dao.SellerDao
     * @method updateSeller
     */
    @Override
    public Seller updateSeller(final Seller seller) throws ApplicationException {
        Transaction transaction = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.update(seller);
            transaction.commit();
        } catch (final HibernateException e) {
            if (null != transaction) {
                transaction.rollback();
            }
            throw new ApplicationException(
                    StringUtil.concatenate(Constant.EX_UPDATE, Constant.SELLER, Long.toString(seller.getId())), e);
        } finally {
            session.close();
        }
        return seller;
    }

    /**
     * @see com.ideas2it.onlineshopping.dao.SellerDao
     * @method retrieveSeller
     */
    @Override
    public List<Seller> retrieveAllSellers() throws ApplicationException {
        List<Seller> sellers = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            final Criteria criteria = session.createCriteria(Seller.class);
            sellers = criteria.list();
        } catch (final HibernateException e) {
            throw new ApplicationException(StringUtil.concatenate(Constant.EX_RETREIVE, Constant.SELLER), e);
        } finally {
            session.close();
        }
        return sellers;
    }
}
