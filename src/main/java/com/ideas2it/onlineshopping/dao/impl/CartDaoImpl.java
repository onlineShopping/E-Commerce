package com.ideas2it.onlineshopping.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.dao.CartDao;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Cart;
import com.ideas2it.onlineshopping.util.StringUtil;

/**
 * Implements the Cart Service with the insert, delete, retrieve and update of
 * the category.
 */
@Repository(Constant.CART_DAO)
public class CartDaoImpl implements CartDao {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * @see - com.ideas2it.onlineshopping.dao.CartDao
     *      #insertCart(Cart cart)
     */
    @Override
    public Cart insertCart(final Cart cart) throws ApplicationException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.save(cart);
            transaction.commit();
        } catch (final HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            }
            throw new ApplicationException(StringUtil.concatenate(Constant.EXCEPTION_WHILE, Constant.INSERTING,
                    Constant.CART_ID, String.valueOf(cart.getId())), exception);
        } finally {
            session.close();
        }
        return cart;
    }

    /**
     * @see - com.ideas2it.onlineshopping.dao.CartDao
     *      #getCart(Long customerId)
     */
    @Override
    public Cart getCartByCustomer(final Long customerId) throws ApplicationException {
        Session session = null;
        Cart cart = null;
        try {
            session = sessionFactory.openSession();
            final Criteria criteria = session.createCriteria(Cart.class);
            criteria.add(Restrictions.eq("customer.id", customerId));
            cart = (Cart) criteria.uniqueResult();
        } catch (final HibernateException exception) {
            throw new ApplicationException(
                    StringUtil.concatenate(Constant.EXCEPTION_WHILE, Constant.GETTING, String.valueOf(customerId)),
                    exception);
        } finally {
            session.close();
        }
        return cart;
    }

    /**
     * @see - com.ideas2it.onlineshopping.dao.CartDao
     *      #getCart(Long cartId)
     */
    @Override
    public Cart getCart(final Long cartId) throws ApplicationException {
        Session session = null;
        Cart cart = null;
        try {
            session = sessionFactory.openSession();
            final Criteria criteria = session.createCriteria(Cart.class);
            criteria.add(Restrictions.eq("id", cartId));
            cart = (Cart) criteria.uniqueResult();
        } catch (final HibernateException exception) {
            throw new ApplicationException(StringUtil.concatenate(Constant.EXCEPTION_WHILE, Constant.GETTING,
                    Constant.CART_ID, String.valueOf(cartId)), exception);
        } finally {
            session.close();
        }
        return cart;
    }

    /**
     * @see - com.ideas2it.onlineshopping.dao.CartDao
     *      #deleteCart(Long cartId)
     */
    @Override
    public void deleteCart(final Long cartId) throws ApplicationException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            final Criteria criteria = session.createCriteria(Cart.class);
            criteria.add(Restrictions.eq("id", cartId));
            final Cart cart = (Cart) criteria.uniqueResult();
            session.delete(cart);
            transaction.commit();
        } catch (final HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            }
            throw new ApplicationException(StringUtil.concatenate(Constant.EXCEPTION_WHILE, Constant.DELETING,
                    Constant.CART_ID, String.valueOf(cartId)), exception);
        } finally {
            session.close();
        }
    }

    /**
     * @see - com.ideas2it.onlineshopping.dao.CartDao
     *      #updateCart(Cart cart)
     */
    @Override
    public Cart updateCart(final Cart cart) throws ApplicationException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.update(cart);
            transaction.commit();
        } catch (final HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            }
            throw new ApplicationException(
                    StringUtil.concatenate(Constant.EXCEPTION_WHILE, Constant.UPDATING, String.valueOf(cart.getId())),
                    exception);
        } finally {
            session.close();
        }
        return cart;
    }
}
