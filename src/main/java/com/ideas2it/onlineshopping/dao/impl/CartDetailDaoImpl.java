package com.ideas2it.onlineshopping.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.dao.CartDetailDao;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.CartDetail;
import com.ideas2it.onlineshopping.util.StringUtil;

/**
 * Implements the CartDetail Dao with the update and get the cartDetail.
 */
@Repository(Constant.CART_DETAIL_DAO)
public class CartDetailDaoImpl implements CartDetailDao {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * @see - com.ideas2it.onlineshopping.dao.CartDetailDao
     *      #updateCartDetail(final CartDetail cartDetail)
     */
    @Override
    public CartDetail updateCartDetail(final CartDetail cartDetail) throws ApplicationException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.update(cartDetail);
            transaction.commit();
        } catch (final HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            }
            throw new ApplicationException(StringUtil.concatenate(Constant.EXCEPTION_WHILE, Constant.UPDATING,
                    String.valueOf(cartDetail.getId())), exception);
        } finally {
            session.close();
        }
        return cartDetail;
    }

    /**
     * @see - com.ideas2it.onlineshopping.dao.CartDetailDao
     *      #getCartDetail(final Long cartId, final Long productId)
     */
    @Override
    public CartDetail getCartDetail(final Long cartId, final Long productId) throws ApplicationException {
        Session session = null;
        CartDetail cartDetail = null;
        try {
            session = sessionFactory.openSession();
            final Criteria criteria = session.createCriteria(CartDetail.class);
            criteria.add(Restrictions.eq("cart.id", cartId));
            criteria.add(Restrictions.eq("product.id", productId));
            cartDetail = (CartDetail) criteria.uniqueResult();
        } catch (final HibernateException exception) {
            throw new ApplicationException(StringUtil.concatenate(Constant.EXCEPTION_WHILE, Constant.GETTING,
                    Constant.PRODUCT_ID, String.valueOf(productId)), exception);
        } finally {
            session.close();
        }
        return cartDetail;
    }

    /**
     * @see - com.ideas2it.onlineshopping.dao.CartDetailDao
     *      #getCartDetail(final Long cartId, final Long productId)
     */
    @Override
    public List<CartDetail> getCartDetailByCartId(final Long cartId) throws ApplicationException {
        Session session = null;
        List<CartDetail> cartDetails = new ArrayList<>();
        try {
            session = sessionFactory.openSession();
            final Criteria criteria = session.createCriteria(CartDetail.class);
            criteria.add(Restrictions.eq("cart.id", cartId));
            criteria.add(Restrictions.eq("isActive", Boolean.TRUE));
            cartDetails = criteria.list();
        } catch (final HibernateException exception) {
            throw new ApplicationException(StringUtil.concatenate(Constant.EXCEPTION_WHILE, Constant.GETTING),
                    exception);
        } finally {
            session.close();
        }
        return cartDetails;
    }

    @Override
    public void insertCartDetail(final CartDetail cartDetail) throws ApplicationException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.save(cartDetail);
            transaction.commit();
        } catch (final HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            }
            throw new ApplicationException(StringUtil.concatenate(Constant.EXCEPTION_WHILE, Constant.INSERTING,
                    String.valueOf(cartDetail.getId())), exception);
        } finally {
            session.close();
        }

    }

    /**
     * @see - com.ideas2it.onlineshopping.dao.CartDetailDao
     *      #deleteCartDetail(CartDetail cartDetail)
     */
    @Override
    public boolean deleteCartDetail(final CartDetail cartDetail) throws ApplicationException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            final Criteria criteria = session.createCriteria(CartDetail.class);
            session.delete(cartDetail);
            transaction.commit();
        } catch (final HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            }
            throw new ApplicationException(StringUtil.concatenate(Constant.EXCEPTION_WHILE, Constant.DELETING,
                    Constant.CART_DETAIL_ID, String.valueOf(cartDetail.getId())), exception);
        } finally {
            session.close();
        }
        return Boolean.TRUE;
    }

}
