package com.ideas2it.onlineshopping.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.dao.OrderDetailDao;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.OrderDetail;
import com.ideas2it.onlineshopping.util.StringUtil;

@Repository(Constant.ORDER_DAO)
public class OrderDetailDaoImpl implements OrderDetailDao {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * @see - com.ideas2it.onlineshopping.service.OrderDetailDaoImpl
     *      #insertOrderDetail(OrderDetail orderDetail)
     */
    @Override
    public OrderDetail insertOrderDetail(final OrderDetail orderDetail) throws ApplicationException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.save(orderDetail);
            transaction.commit();
        } catch (final HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            }
            throw new ApplicationException(StringUtil.concatenate(Constant.EXCEPTION_WHILE, Constant.INSERTING,
                    Constant.ORDER_ID, String.valueOf(orderDetail.getId())), exception);
        } finally {
            session.close();
        }
        return orderDetail;
    }

    /**
     * @see - com.ideas2it.onlineshopping.dao.CartDetailDao
     *      #getOrderDetail(final Long orderDetailId)
     */
    @Override
    public List<OrderDetail> getOrderDetail(final Long orderDetailId) throws ApplicationException {
        Session session = null;
        final List<OrderDetail> orderDetails;
        try {
            session = sessionFactory.openSession();
            final Criteria criteria = session.createCriteria(OrderDetail.class);
            criteria.add(Restrictions.eq("cartId", orderDetailId));
            orderDetails = criteria.list();
        } catch (final HibernateException exception) {
            throw new ApplicationException(StringUtil.concatenate(Constant.EXCEPTION_WHILE, Constant.GETTING,
                    Constant.ORDER_DETAIL_ID, String.valueOf(orderDetailId)), exception);
        } finally {
            session.close();
        }
        return orderDetails;
    }
}
