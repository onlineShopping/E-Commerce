package com.ideas2it.onlineshopping.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.dao.PaymentDao;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Payment;
import com.ideas2it.onlineshopping.util.StringUtil;

/**
 * Implements the payment Service with the insert and retrieve of the payment.
 */
@Repository(Constant.PAYMENT_DAO)
public class PaymentDaoImpl implements PaymentDao{

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * @see - com.ideas2it.onlineshopping.dao.paymentDao
     *        #insertPayment(Payment payment)
     */
    @Override
    public Payment insertPayment(final Payment payment) throws ApplicationException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.save(payment);
            transaction.commit();
        } catch (final HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            }
            throw new ApplicationException(StringUtil.concatenate(Constant.EXCEPTION_WHILE, Constant.INSERTING,
                    Constant.PAYMENT_ID, String.valueOf(payment.getId())), exception);
        } finally {
            session.close();
        }
        return payment;
    }

    /**
     * @see - com.ideas2it.onlineshopping.dao.PaymentDao
     *      #getPayment(Long paymentId)
     */
    @Override
    public Payment getPayment(final Long paymentId) throws ApplicationException {
        Session session = null;
        Payment payment = null;
        try {
            session = sessionFactory.openSession();
            final Criteria criteria = session.createCriteria(Payment.class);
            criteria.add(Restrictions.eq("id", paymentId));
            payment = (Payment) criteria.uniqueResult();
        } catch (final HibernateException exception) {
            throw new ApplicationException(StringUtil.concatenate(Constant.EXCEPTION_WHILE, Constant.GETTING,
                    Constant.PAYMENT_ID, String.valueOf(paymentId)), exception);
        } finally {
            session.close();
        }
        return payment;
    }
}