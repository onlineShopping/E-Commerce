package com.ideas2it.onlineshopping.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.dao.InvoiceDao;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Invoice;
import com.ideas2it.onlineshopping.util.StringUtil;

/**
 * Implements the invoice Service with the insert and retrieve of the invoice.
 */
@Repository(Constant.INVOICE_DAO)
public class InvoiceDaoImpl implements InvoiceDao {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * @see - com.ideas2it.onlineshopping.dao.InvoiceDao
     *      - insertInvoice(Invoice invoice)
     */
    @Override
    public Invoice insertInvoice(final Invoice invoice) throws ApplicationException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.save(invoice);
            transaction.commit();
        } catch (final HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            }
            throw new ApplicationException(StringUtil.concatenate(Constant.EXCEPTION_WHILE, Constant.INSERTING,
                    Constant.INVOICE_ID, String.valueOf(invoice.getId())), exception);
        } finally {
            session.close();
        }
        return invoice;
    }

    /**
     * @see - com.ideas2it.onlineshopping.dao.InvoiceDao
     *      - retrieveInvoiceById(Long id)
     */
    @Override
    public Invoice retrieveInvoiceById(final Long id) throws ApplicationException {
        Session session = null;
        Invoice invoice = null;
        try {
            session = sessionFactory.openSession();
            final Criteria criteria = session.createCriteria(Invoice.class);
            criteria.add(Restrictions.eq("id", id));
            invoice = (Invoice) criteria.uniqueResult();
        } catch (final HibernateException exception) {
            throw new ApplicationException(StringUtil.concatenate(Constant.EXCEPTION_WHILE, Constant.GETTING,
                    Constant.INVOICE_ID, String.valueOf(id)), exception);
        } finally {
            session.close();
        }
        return invoice;
    }
}

