package com.ideas2it.onlineshopping.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.dao.SpecificationDao;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Specification;
import com.ideas2it.onlineshopping.util.StringUtil;

/**
 * Implements the Specification Management with the insert, retrieve and update
 * of the specification.
 *
 * @author Durgadevi Ananthakrishnan
 */
@Repository(Constant.SPECIFICATION_DAO)
public class SpecificationDaoImpl implements SpecificationDao {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * @see - com.ideas2it.onlineshopping.dao.SpecificationDao
     * @method - retrieveAllSpecifications();
     */
    @Override
    public List<Specification> retrieveAllSpecifications() throws ApplicationException {
        Session session = null;
        List<Specification> specifications = new ArrayList<>();
        try {
            session = sessionFactory.openSession();
            final Criteria criteria = session.createCriteria(Specification.class);
            specifications = criteria.list();
        } catch (final HibernateException e) {
            throw new ApplicationException(StringUtil.concatenate(Constant.RETRIEVE_EX, Constant.SPECIFICATION), e);
        } finally {
            session.close();
        }
        return specifications;
    }

    /**
     * @see - com.ideas2it.onlineshopping.dao.SpecificationDao
     *      - updateSpecification(Specification)
     */
    @Override
    public boolean updateSpecification(final Specification specification) throws ApplicationException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            final Criteria criteria = session.createCriteria(Specification.class);
            session.update(specification);
            transaction.commit();
        } catch (final HibernateException e) {
            transaction.rollback();
            throw new ApplicationException(StringUtil.concatenate(Constant.UPDATE_EX, Constant.SPECIFICATION,
                    String.valueOf(specification.getId())), e);
        } finally {
            session.close();
        }
        return Boolean.TRUE;
    }

    /**
     * @see - com.ideas2it.onlineshopping.dao.SpecificationDao
     *      - retrieveSpecification(Long)
     */
    @Override
    public Specification retrieveSpecificationById(final Long id) throws ApplicationException {
        Session session = null;
        Specification specification = null;
        try {
            session = sessionFactory.openSession();
            final Criteria criteria = session.createCriteria(Specification.class);
            criteria.add(Restrictions.eq(Constant.ID, id));
            specification = (Specification) criteria.uniqueResult();

        } catch (final HibernateException e) {
            throw new ApplicationException(StringUtil.concatenate(Constant.RETRIEVE_EX, Constant.SPECIFICATION,
                    String.valueOf(specification.getId())), e);
        } finally {
            session.close();
        }
        return specification;
    }

    /**
     * @see - com.ideas2it.onlineshopping.dao.SpecificationDao
     *      - insertSpecification(Specification)
     */
    @Override
    public boolean insertSpecification(final Specification specification) throws ApplicationException {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            final Transaction transaction = session.beginTransaction();
            session.save(specification);
            transaction.commit();
        } catch (final HibernateException e) {
            throw new ApplicationException(StringUtil.concatenate(Constant.INSERT_EX, Constant.SPECIFICATION,
                    String.valueOf(specification.getId())), e);
        } finally {
            session.close();
        }
        return Boolean.TRUE;
    }
}
