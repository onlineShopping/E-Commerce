package com.ideas2it.onlineshopping.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.dao.SubCategoryDao;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.SubCategory;
import com.ideas2it.onlineshopping.util.StringUtil;

/**
 * Implements the Sub Category Management with the insert, in activate,
 * retrieve and update of the sub category.
 *
 * @author Durgadevi Ananthakrishnan
 */
@Repository(Constant.SUBCATEGORY_DAO)
public class SubCategoryDaoImpl implements SubCategoryDao {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * @see - com.ideas2it.onlineshopping.dao.SubCategoryDao
     *      - retrieveAllSubCategories(boolean);
     */
    @Override
    public List<SubCategory> retrieveAllSubCategories(final boolean isNeededAll) throws ApplicationException {
        Session session = null;
        List<SubCategory> subCategories = new ArrayList<>();
        try {
            session = sessionFactory.openSession();
            final Criteria criteria = session.createCriteria(SubCategory.class);
            if (!isNeededAll) {
                criteria.add(Restrictions.eq(Constant.IS_ACTIVE, Boolean.TRUE));
            }
            subCategories = criteria.list();
        } catch (final HibernateException e) {
            throw new ApplicationException(StringUtil.concatenate(Constant.RETRIEVE_EX, Constant.SUB_CATEGORY), e);
        } finally {
            session.close();
        }
        return subCategories;
    }

    /**
     * @see - com.ideas2it.onlineshopping.dao.SubCategoryDao
     *      - isSubCategoryDetailExists(String, String);
     */
    @Override
    public boolean isSubCategoryDetailExists(final String columnName, final String detail) throws ApplicationException {
        Session session = null;
        SubCategory subCategory = null;
        boolean isDetailExisted = Boolean.FALSE;
        try {
            session = sessionFactory.getCurrentSession();
            final Criteria criteria = session.createCriteria(SubCategory.class);
            criteria.add(Restrictions.eq(Constant.IS_ACTIVE, Boolean.TRUE));
            criteria.add(Restrictions.eq(columnName, detail));
            subCategory = (SubCategory) criteria.uniqueResult();
            if (null != subCategory) {
                isDetailExisted = Boolean.TRUE;
            } else {
                isDetailExisted = Boolean.FALSE;
            }
        } catch (final HibernateException e) {
            throw new ApplicationException(StringUtil.concatenate(Constant.RETRIEVE_EX, Constant.SUB_CATEGORY, detail),
                    e);
        } finally {
            session.close();
        }
        return isDetailExisted;
    }

    /**
     * @see - com.ideas2it.onlineshopping.dao.SubCategoryDao
     *      - updateSubCategory(SubCategory)
     */
    @Override
    public boolean updateSubCategory(final SubCategory subCategory) throws ApplicationException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            final Criteria criteria = session.createCriteria(SubCategory.class);
            criteria.add(Restrictions.eq(Constant.ID, subCategory.getId()));
            session.update(subCategory);
            transaction.commit();
        } catch (final Exception e) {
            transaction.rollback();
            throw new ApplicationException(StringUtil.concatenate(Constant.UPDATE_EX, Constant.SUB_CATEGORY,
                    String.valueOf(subCategory.getId())), e);
        } finally {
            session.close();
        }
        return Boolean.TRUE;
    }

    /**
     * @see - com.ideas2it.onlineshopping.dao.SubCategoryDao
     *      - retrieveSubCategory(Long, boolean)
     */
    @Override
    public SubCategory retrieveSubCategoryById(final Long id, final boolean isNeededActive)
            throws ApplicationException {
        Session session = null;
        SubCategory subCategory = null;
        try {
            session = sessionFactory.openSession();
            final Criteria criteria = session.createCriteria(SubCategory.class);
            criteria.add(Restrictions.eq(Constant.ID, id));
            if (isNeededActive) {
                criteria.add(Restrictions.eq(Constant.IS_ACTIVE, Boolean.TRUE));
            }
            subCategory = (SubCategory) criteria.uniqueResult();
        } catch (final HibernateException e) {
            throw new ApplicationException(StringUtil.concatenate(Constant.RETRIEVE_EX, Constant.SUB_CATEGORY,
                    String.valueOf(subCategory.getId())), e);
        } finally {
            session.close();
        }
        return subCategory;
    }

    /**
     * @see - com.ideas2it.onlineshopping.dao.SubCategoryDao
     *      - insertSubCategory(SubCategory)
     */
    @Override
    public SubCategory insertSubCategory(final SubCategory subCategory) throws ApplicationException {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            final Transaction transaction = session.beginTransaction();
            session.save(subCategory);
            transaction.commit();
        } catch (final Exception e) {
            throw new ApplicationException(StringUtil.concatenate(Constant.INSERT_EX, Constant.SUB_CATEGORY,
                    String.valueOf(subCategory.getId())), e);
        } finally {
            session.close();
        }
        return subCategory;
    }

    /**
     * @see - com.ideas2it.onlineshopping.dao.SubCategoryDao
     *      - retrieveSubCategoryByName(String)
     */
    @Override
    public SubCategory retrieveSubCategoryByName(final String name) throws ApplicationException {
        Session session = null;
        SubCategory subCategory = null;
        try {
            session = sessionFactory.openSession();
            final Criteria criteria = session.createCriteria(SubCategory.class);
            criteria.add(Restrictions.eq(Constant.NAME, name)).add(Restrictions.eq(Constant.IS_ACTIVE, Boolean.TRUE));
            subCategory = (SubCategory) criteria.uniqueResult();
        } catch (final HibernateException e) {
            throw new ApplicationException(StringUtil.concatenate(Constant.RETRIEVE_EX, Constant.SUB_CATEGORY, name),
                    e);
        } finally {
            session.close();
        }
        return subCategory;
    }

}
