package com.ideas2it.onlineshopping.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.dao.ProductDao;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Product;
import com.ideas2it.onlineshopping.model.ProductSeller;
import com.ideas2it.onlineshopping.model.Specification;
import com.ideas2it.onlineshopping.util.StringUtil;

/**
 * Implements the Product Management with the insert, in activate,
 * retrieve and update of the product.
 *
 * @author Durgadevi Ananthakrishnan
 */
@Repository(Constant.PRODUCT_DAO)
public class ProductDaoImpl implements ProductDao {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * @see - com.ideas2it.onlineshopping.dao.ProductDao
     * @method - retrieveAllProducts(boolean);
     */
    @Override
    public List<Product> retrieveAllProducts(final boolean isNeededAll) throws ApplicationException {
        Session session = null;
        List<Product> products = new ArrayList<>();
        try {
            session = sessionFactory.openSession();
            final Criteria criteria = session.createCriteria(Product.class);
            if (!isNeededAll) {
                criteria.add(Restrictions.eq(Constant.IS_ACTIVE, Boolean.TRUE));
            }
            products = criteria.list();
        } catch (final HibernateException e) {
            throw new ApplicationException(StringUtil.concatenate(Constant.RETRIEVE_EX, Constant.PRODUCT), e);
        }  finally {
            session.close();
        }
        return products;
    }

    /**
     * @see - com.ideas2it.onlineshopping.dao.ProductDao
     *      - isProductDetailExists(String, String);
     */
    @Override
    public boolean isProductDetailExists(final String columnName, final String detail) throws ApplicationException {
        Session session = null;
        Product product = null;
        boolean isDetailExisted = Boolean.FALSE;
        try {
            session = sessionFactory.getCurrentSession();
            final Criteria criteria = session.createCriteria(Product.class);
            criteria.add(Restrictions.eq(Constant.IS_ACTIVE, Boolean.TRUE));
            criteria.add(Restrictions.eq(columnName, detail));
            product = (Product) criteria.uniqueResult();
            if (null != product) {
                isDetailExisted = Boolean.TRUE;
            } else {
                isDetailExisted = Boolean.FALSE;
            }
        } catch (final HibernateException e) {
            throw new ApplicationException(StringUtil.concatenate(Constant.RETRIEVE_EX, Constant.PRODUCT, detail), e);
        } finally {
            session.close();
        }
        return isDetailExisted;
    }

    /**
     * @see - com.ideas2it.onlineshopping.dao.ProductDao
     *      - updateProduct(Product)
     */
    @Override
    public boolean updateProduct(final Product product) throws ApplicationException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            final Criteria criteria = session.createCriteria(Product.class);
            criteria.add(Restrictions.eq(Constant.ID, product.getId()));
            session.update(product);
            transaction.commit();
        } catch (final Exception e) {
            transaction.rollback();
            throw new ApplicationException(
                    StringUtil.concatenate(Constant.UPDATE_EX, Constant.PRODUCT, String.valueOf(product.getId())), e);
        } finally {
            session.close();
        }
        return Boolean.TRUE;
    }

    /**
     * @see - com.ideas2it.onlineshopping.dao.ProductDao
     *      - retrieveProduct(Long, boolean)
     */
    @Override
    public Product retrieveProductById(final Long id, final boolean isNeededActive) throws ApplicationException {
        Session session = null;
        Product product = null;
        try {
            session = sessionFactory.openSession();
            final Criteria criteria = session.createCriteria(Product.class);
            criteria.add(Restrictions.eq(Constant.ID, id));
            if (isNeededActive) {
                criteria.add(Restrictions.eq(Constant.IS_ACTIVE, Boolean.TRUE));
            }
            product = (Product) criteria.uniqueResult();
        } catch (final HibernateException e) {
            throw new ApplicationException(
                    StringUtil.concatenate(Constant.RETRIEVE_EX, Constant.PRODUCT, String.valueOf(product.getId())), e);
        } finally {
            session.close();
        }
        return product;
    }

    /**
     * @see - com.ideas2it.onlineshopping.dao.ProductDao
     *      - insertProduct(Product)
     */
    @Override
    public Product insertProduct(final Product product) throws ApplicationException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.save(product);
            transaction.commit();
        } catch (final Exception e) {
            if (null != transaction) {
                transaction.rollback();
            }
            throw new ApplicationException(
                    StringUtil.concatenate(Constant.INSERT_EX, Constant.PRODUCT, Long.toString(product.getId())), e);
        } finally {
            session.close();
        }
        return product;
    }

    /**
     * @see - com.ideas2it.onlineshopping.dao.ProductDao
     *      - retrieveProduct(Long, boolean)
     */
    @Override
    public List<ProductSeller> retrieveProductSellersBySellerId(final Long sellerId) throws ApplicationException {
        Session session = null;
        List<ProductSeller> productSellers = new ArrayList<>();
        try {
            session = sessionFactory.openSession();
            final Criteria criteria = session.createCriteria(ProductSeller.class);
            criteria.add(Restrictions.eq("seller.id", sellerId));
            productSellers = criteria.list();
        } catch (final HibernateException e) {
            throw new ApplicationException(
                    StringUtil.concatenate(Constant.RETRIEVE_EX, Constant.PRODUCT, String.valueOf(sellerId)), e);
        } finally {
            session.close();
        }
        return productSellers;
    }

   /**
    * @see - com.ideas2it.onlineshopping.dao.ProductDao
    * @method - retrieveAllProducts(boolean);
    */
   @Override
   public List<Product> retrieveAllSearchProducts(final String name) throws ApplicationException {
       Session session = null;
       List<Product> products = new ArrayList<>();
       try {
           session = sessionFactory.openSession();
           Criteria criteria = session.createCriteria(Product.class);
           Criteria specificationCriteria = criteria.createCriteria(Constant.SPECIFICATION);
           specificationCriteria.add(Restrictions.like(Constant.BRAND_NAME, name));
           products = criteria.list();
       } catch (final HibernateException e) {
           throw new ApplicationException(StringUtil.concatenate(Constant.RETRIEVE_EX, Constant.PRODUCT), e);
       }  finally {
           session.close();
       }
       return products;
   }
}
