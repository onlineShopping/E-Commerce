package com.ideas2it.onlineshopping.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.dao.OrderDao;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Order;
import com.ideas2it.onlineshopping.util.StringUtil;

@Repository(Constant.ORDER_DETAIL_DAO)
public class OrderDaoImpl implements OrderDao {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * @see - com.ideas2it.onlineshopping.service.OrderDao
     *        #insertOrder(Order order)
     */
    @Override
    public Order insertOrder(final Order order) throws ApplicationException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.save(order);
            transaction.commit();
        } catch (final HibernateException exception) {
            throw new ApplicationException(StringUtil.concatenate(Constant.EXCEPTION_WHILE, Constant.INSERTING,
                    Constant.ORDER_DETAIL_ID, String.valueOf(order.getId())), exception);
        } finally {
            session.close();
        }
        return order;
    }

    /**
     * @see - com.ideas2it.onlineshopping.service.OrderDao
     *      #getOrders(Long customerId)
     */
    @Override
    public List<Order> getOrders(final Long customerId) throws ApplicationException {
        Session session = null;
        List<Order> orders = new ArrayList<>();
        try {
            session = sessionFactory.openSession();
            final Criteria criteria = session.createCriteria(Order.class);
            criteria.add(Restrictions.eq("customer.id", customerId));
            orders = criteria.list();
        } catch (final HibernateException exception) {
            throw new ApplicationException(StringUtil.concatenate(Constant.EXCEPTION_WHILE, Constant.INSERTING,
                    Constant.CUSTOMER_ID, String.valueOf(customerId)), exception);
        } finally {
            session.close();
        }
        return orders;
    }
}
