package com.ideas2it.onlineshopping.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.dao.CategoryDao;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Category;
import com.ideas2it.onlineshopping.util.StringUtil;

/**
 * Implements the Category Management with the insert, in activate,
 * retrieve and update of the category.
 *
 * @author Durgadevi Ananthakrishnan
 */
@Repository(Constant.CATEGORY_DAO)
public class CategoryDaoImpl implements CategoryDao {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * @see - com.ideas2it.onlineshopping.dao.CategoryDao
     * @method - retrieveAllCategories(boolean);
     */
    @Override
    public List<Category> retrieveAllCategories(final boolean isNeededAll) throws ApplicationException {
        Session session = null;
        List<Category> categories = new ArrayList<>();
        try {
            session = sessionFactory.openSession();
            final Criteria criteria = session.createCriteria(Category.class);
            if (!isNeededAll) {
                criteria.add(Restrictions.eq(Constant.IS_ACTIVE, Boolean.TRUE));
            }
            categories = criteria.list();
        } catch (final HibernateException e) {
            throw new ApplicationException(StringUtil.concatenate(Constant.RETRIEVE_EX, Constant.CATEGORY), e);
        } finally {
            session.close();
        }
        return categories;
    }

    /**
     * @see - com.ideas2it.onlineshopping.dao.CategoryDao
     *      - isCategoryDetailExists(String, String);
     */
    @Override
    public boolean isCategoryDetailExists(final String columnName, final String detail) throws ApplicationException {
        Session session = null;
        Category category = null;
        boolean isDetailExisted = Boolean.FALSE;
        try {
            session = sessionFactory.openSession();
            final Criteria criteria = session.createCriteria(Category.class);
            criteria.add(Restrictions.eq(columnName, detail));
            category = (Category) criteria.uniqueResult();
            if (null != category) {
                isDetailExisted = Boolean.TRUE;
            } else {
                isDetailExisted = Boolean.FALSE;
            }
        } catch (final HibernateException e) {
            throw new ApplicationException(StringUtil.concatenate(Constant.RETRIEVE_EX, Constant.CATEGORY, detail), e);
        }  finally {
            session.close();
        }
        return isDetailExisted;
    }

    /**
     * @see - com.ideas2it.onlineshopping.dao.CategoryDao
     *      - updateCategory(Category)
     */
    @Override
    public boolean updateCategory(final Category category) throws ApplicationException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            final Criteria criteria = session.createCriteria(Category.class);
            criteria.add(Restrictions.eq(Constant.ID, category.getId()));
            session.update(category);
            transaction.commit();
        } catch (final Exception e) {
            transaction.rollback();
            throw new ApplicationException(
                    StringUtil.concatenate(Constant.UPDATE_EX, Constant.CATEGORY, String.valueOf(category.getId())), e);
        } finally {
            session.close();
        }
        return Boolean.TRUE;
    }

    /**
     * @see - com.ideas2it.onlineshopping.dao.CategoryDao
     *      - retrieveCategory(Long, boolean)
     */
    @Override
    public Category retrieveCategoryById(final Long id, final boolean isNeededActive) throws ApplicationException {
        Session session = null;
        Category category = null;
        try {
            session = sessionFactory.openSession();
            final Criteria criteria = session.createCriteria(Category.class);
            criteria.add(Restrictions.eq(Constant.ID, id));
            if (isNeededActive) {
                criteria.add(Restrictions.eq(Constant.IS_ACTIVE, Boolean.TRUE));
            }
            category = (Category) criteria.uniqueResult();
        } catch (final HibernateException e) {
            throw new ApplicationException(
                    StringUtil.concatenate(Constant.RETRIEVE_EX, Constant.CATEGORY, String.valueOf(category.getId())),
                    e);
        }  finally {
            session.close();
        }
        return category;
    }

    /**
     * @see - com.ideas2it.onlineshopping.dao.CategoryDao
     *      - insertCategory(Category)
     */
    @Override
    public Category insertCategory(final Category category) throws ApplicationException {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            final Transaction transaction = session.beginTransaction();
            session.save(category);
            transaction.commit();
        } catch (final Exception e) {
            throw new ApplicationException(
                    StringUtil.concatenate(Constant.INSERT_EX, Constant.CATEGORY, String.valueOf(category.getId())), e);
        } finally {
            session.close();
        }
        return category;
    }
}
