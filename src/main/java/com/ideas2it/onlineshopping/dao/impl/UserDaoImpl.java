package com.ideas2it.onlineshopping.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.dao.UserDao;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.User;
import com.ideas2it.onlineshopping.util.StringUtil;

/**
 * <p>
 * Performs authentication and updation of user credentials.
 * </p>
 *
 * @author Ajaykkumar R
 */
@Repository(Constant.USER_DAO)
public class UserDaoImpl implements UserDao {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    /**
     * @see     com.ideas2it.onlineshopping.dao.UserDao
     * @method  createUser
     */
    @Override
    public void createUser(final User user) throws ApplicationException {
        Transaction transaction = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.save(user);
            transaction.commit();
        } catch (final HibernateException e) {
            if(null != transaction) {
                transaction.rollback();
            }
            throw new ApplicationException(Constant.EX_INSERT, e);
        } finally {
            session.close();
        }
    }

    /**
     * @see     com.ideas2it.onlineshopping.dao.UserDao
     * @method  authenticateUser
     */
    @Override
    public User authenticateUser(final String userName, final String password) throws ApplicationException {
        User user = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            final Transaction transaction = session.beginTransaction();
            final Criteria criteria = session.createCriteria(User.class);
            criteria.add(Restrictions.eq(Constant.USERNAME, userName));
            criteria.add(Restrictions.eq(Constant.PASSWORD, password));
            user = (User) criteria.uniqueResult();
        } catch (final HibernateException e) {
            throw new ApplicationException(StringUtil.concatenate(Constant.EX_SEARCH, userName), e);
        } finally {
            session.close();
        }
        return user;
    }

    /**
     * @see     com.ideas2it.onlineshopping.dao.UserDao
     * @method  getUserDetail
     */
    @Override
    public User getUserDetail(final String key, final String value) throws ApplicationException {
        User user = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            final Transaction transaction = session.beginTransaction();
            final Criteria criteria = session.createCriteria(User.class);
            criteria.add(Restrictions.eq(key, value));
            user = (User) criteria.uniqueResult();
        } catch (final HibernateException e) {
            throw new ApplicationException(StringUtil.concatenate(Constant.EX_SEARCH, value), e);
        } finally {
            session.close();
        }
        return user;
    }
    
    /**
     * @see     com.ideas2it.onlineshopping.dao.UserDao
     * @method  getUser
     */
    @Override
    public User getUser(final String key, final Long value) throws ApplicationException {
        User user = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            final Transaction transaction = session.beginTransaction();
            final Criteria criteria = session.createCriteria(User.class);
            criteria.add(Restrictions.eq(key, value));
            user = (User) criteria.uniqueResult();
        } catch (final HibernateException e) {
            throw new ApplicationException(StringUtil.concatenate(Constant.EX_SEARCH, Long.toString(value)), e);
        } finally {
            session.close();
        }
        return user;
    }
    

    /**
     * @see     com.ideas2it.onlineshopping.dao.UserDao
     * @method  updateUser
     */
    @Override
    public User updateUser(final User user) throws ApplicationException {
        Transaction transaction = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.update(user);
            transaction.commit();
        } catch (final HibernateException e) {
            if(null != transaction) {
                transaction.rollback();
            }
            throw new ApplicationException(StringUtil.concatenate(Constant.EX_UPDATE, user.getUserName()), e);
        } finally {
            session.close();
        }
        return user;
    }
    
    /**
     * @see     com.ideas2it.onlineshopping.dao.UserDao
     * @method  retreiveAllUsers
     */
    @Override
    public List<User> retrieveAllUsers() throws ApplicationException {
        List<User> users = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            final Criteria criteria = session.createCriteria(User.class);
            users = criteria.list();
        } catch (final HibernateException e) {
            throw new ApplicationException(StringUtil.concatenate(Constant.EX_RETREIVE, Constant.USER), e);
        } finally {
            session.close();
        }
        return users;
    }
}
