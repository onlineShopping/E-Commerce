package com.ideas2it.onlineshopping.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.onlineshopping.common.Constant;
import com.ideas2it.onlineshopping.dao.CustomerDao;
import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Customer;
import com.ideas2it.onlineshopping.util.StringUtil;

/**
 * <p>
 * Performs customer CRUD operation in the database.
 * </p>
 *
 * @author Ajaykkumar R
 */
@Repository(Constant.CUSTOMER_DAO)
public class CustomerDaoImpl implements CustomerDao {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * @see com.ideas2it.onlineshopping.dao.CustomerDao
     * @method insertCustomer
     */
    @Override
    public Customer insertCustomer(final Customer customer) throws ApplicationException {
        Transaction transaction = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.save(customer);
            transaction.commit();
        } catch (final HibernateException e) {
            if (null != transaction) {
                transaction.rollback();
            }
            throw new ApplicationException(
                    StringUtil.concatenate(Constant.EX_INSERT, Constant.CUSTOMER, Long.toString(customer.getId())), e);
        } finally {
            session.close();
        }
        return customer;
    }

    /**
     * @see com.ideas2it.onlineshopping.dao.CustomerDao
     * @method getCustomer
     */
    @Override
    public Customer getCustomerByEmailId(final String emailId) throws ApplicationException {
        Customer customer = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            final Criteria criteria = session.createCriteria(Customer.class).add(Restrictions.eq(Constant.EMAIL_ID, emailId));
            customer = (Customer) criteria.uniqueResult();
        } catch (final HibernateException e) {
            throw new ApplicationException(StringUtil.concatenate(Constant.EX_SEARCH, Constant.CUSTOMER, emailId), e);
        } finally {
            session.close();
        }
        return customer;
    }

    /**
     * @see com.ideas2it.onlineshopping.dao.CustomerDao
     * @method getCustomerById
     */
    @Override
    public Customer getCustomer(final String key, final Long value) throws ApplicationException {
        Customer customer = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            final Criteria criteria = session.createCriteria(Customer.class)
                    .add(Restrictions.eq(Constant.IS_ACTIVE, Boolean.TRUE)).add(Restrictions.eq(key, value));
            customer = (Customer) criteria.uniqueResult();
        } catch (final HibernateException e) {
            throw new ApplicationException(StringUtil.concatenate(Constant.EX_SEARCH, Constant.CUSTOMER, Long.toString(value)), e);
        } finally {
            session.close();
        }
        return customer;
    }

    /**
     * @see com.ideas2it.onlineshopping.dao.CustomerDao
     * @method updateCustomer
     */
    @Override
    public Customer updateCustomer(final Customer customer) throws ApplicationException {
        Transaction transaction = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.update(customer);
            transaction.commit();
        } catch (final HibernateException e) {
            if (null != transaction) {
                transaction.rollback();
            }
            throw new ApplicationException(
                    StringUtil.concatenate(Constant.EX_UPDATE, Constant.CUSTOMER, Long.toString(customer.getId())), e);
        }
        return customer;
    }

    /**
     * @see com.ideas2it.onlineshopping.dao.CustomerDao
     * @method retrieveAllCustomer
     */
    @Override
    public List<Customer> retrieveAllCustomers() throws ApplicationException {
        List<Customer> customers = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            final Criteria criteria = session.createCriteria(Customer.class);
            customers = criteria.list();
        } catch (final HibernateException e) {
            throw new ApplicationException(StringUtil.concatenate(Constant.EX_RETREIVE, Constant.CUSTOMER), e);
        } finally {
            session.close();
        }
        return customers;
    }
}
