package com.ideas2it.onlineshopping.dao;

import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Cart;

/**
 * Implements the Cart Service with the insert, delete, retrieve and update of
 * the category.
 */
public interface CartDao {

    /**
     * Implemented to insert cart details into database
     *
     * @param cart
     *            - Cart, contains details of cart like id, quantity, status.
     * @return cart
     *            - returns details of cart like id, time, modified time, customer, cartDetail.
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    Cart insertCart(Cart cart) throws ApplicationException;

    /**
     * Gets the cart information from the database.
     *
     * @param customerId
     *            - Id of the customer is used to retrieve cart
     *            respect to the customer id
     * @return cart -
     *         Cart details of cart retrieved respect to customer id
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    Cart getCartByCustomer(Long customerId) throws ApplicationException;

    /**
     * Delete an cart from the carts
     *
     * @param cartId
     *            - Id of the cart is to be deleted
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    void deleteCart(Long cartId) throws ApplicationException;

    /**
     * Update an cart from the carts
     *
     * @param cart
     *            - Cart contains details like id, date, customer, cartDetail and order
     * @return cart
     *            - returns details of cart like id, time, modified time, customer, cartDetail.
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    Cart updateCart(Cart cart) throws ApplicationException;

    /**
     * Get the cart information respect to cart id from the database.
     *
     * @param cartId
     *            - Id of the cart is to be retrieved
     * @return cart -
     *         Cart details of cart retrieved respect to cart id
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    Cart getCart(Long cartId) throws ApplicationException;

}
