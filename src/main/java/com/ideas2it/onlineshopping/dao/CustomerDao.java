package com.ideas2it.onlineshopping.dao;

import java.util.List;

import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Customer;

/**
 * Customer dao interface which defines CRUD operations to be performed in database.
 *
 * @author Ajaykkumar R
 */
public interface CustomerDao {

    /**
     * <p>
     * Inserts the customer details into the database.
     * </p>
     *
     * @param customer
     *        Customer details to be inserted into the database.
     * @return customer
     *        Details of the customer.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    Customer insertCustomer(Customer customer) throws ApplicationException;

    /**
     * <p>
     * Updates the details of the customer.
     * </p>
     *
     * @param customer
     *        Details of the customer to be updated.
     * @return customer
     *        Details of the updated customer.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    Customer updateCustomer(Customer customer) throws ApplicationException;

    /**
     * <p>
     * Get the customer detail from the database.
     * </p>
     *
     * @param emailId
     *        Emailid of the customer.
     * @return customer
     *        Details of the customer for the corressponding name or emailid.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    Customer getCustomerByEmailId(String emailId) throws ApplicationException;

    /**
     * <p>
     * Get the customer details by id from the database.
     * </p>
     *
     * @param key
     *        Userid or id.
     * @param value
     *        Userid or id of the customer.
     * @return customer
     *        Details of the customer for the corresponding id.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    Customer getCustomer(final String key, final Long value) throws ApplicationException;

    /**
     * <p>
     * Get all the customer details from the database.
     * </p>
     *
     * @return customers
     *        List of all customer and their details.
     * @throws ApplicationException
     *        If anything went wrong like connection not established or
     *        problem occured while executing query.
     */
    List<Customer> retrieveAllCustomers() throws ApplicationException;
}
