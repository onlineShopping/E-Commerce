package com.ideas2it.onlineshopping.dao;

import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Invoice;

/**
 * Implements the invoice Service with the insert and retrieve of the invoice.
 */
public interface InvoiceDao {

    /**
     * Insert the invoice with the invoice details like id, name and description
     * into the invoice.
     *
     * @param invoice
     *            - the invoice, the invoice to be inserted with the user given
     *            details.
     * @return Invoice
     *         - Contains detail like id, invoice date, order date, orderDetail
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    Invoice insertInvoice(Invoice invoice) throws ApplicationException;

    /**
     * Retrieve the invoice from the invoice by providing the id of the invoice.
     *
     * @param id
     *            - id of the invoice, to retrieve the invoice from the invoice.
     * @return Boolean.TRUE
     *         - true if the invoice is retrieved successfully else throws
     *         exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    Invoice retrieveInvoiceById(Long id) throws ApplicationException;
}
