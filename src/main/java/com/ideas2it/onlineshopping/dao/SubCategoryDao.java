package com.ideas2it.onlineshopping.dao;

import java.util.List;

import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.SubCategory;

/**
 * Implements the Sub Category Management with the insert, in activate,
 * retrieve and update of the sub category.
 *
 * @author Durgadevi Ananthakrishnan
 */
public interface SubCategoryDao {

    /**
     * Retrieve all the categories with the sub category details from the sub
     * category.
     *
     * @param isNeededAll
     *            - true if all the sub categories are needed else false.
     * @return - sub categories, all sub categories available in the sub category.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    List<SubCategory> retrieveAllSubCategories(final boolean isNeededAll) throws ApplicationException;

    /**
     * Check whether the sub category detail like the id and name is exited or not.
     *
     * @param columnName
     *            - column name of the detail, to check the existence of detail.
     * @param detail
     *            - the detail of the subCategory, to check its existence.
     * @return boolean
     *         - true if the detail is already existed in the sub category else
     *         false.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    boolean isSubCategoryDetailExists(final String columnName, final String detail) throws ApplicationException;

    /**
     * Update the sub category with the user given updated details of the
     * subCategory.
     *
     * @param subCategory
     *            - the sub category, the sub category to be updated with the user
     *            given updated details.
     * @return Boolean.TRUE
     *         - true if the sub category is updated successfully else throws
     *         exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    boolean updateSubCategory(final SubCategory subCategory) throws ApplicationException;

    /**
     * Retrieve the sub category from the sub category by providing the id of the
     * sub category.
     *
     * @param id
     *            - id of the sub category, to retrieve the sub category from the
     *            sub category.
     * @param isNeededActive
     *            - true if active sub category is needed else false.
     * @return Boolean.TRUE
     *         - true if the sub category is retrieved successfully else throws
     *         exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    SubCategory retrieveSubCategoryById(final Long id, final boolean isNeededActive) throws ApplicationException;

    /**
     * Insert the sub category with the sub category details like id, name and
     * description into the sub category.
     *
     * @param subCategory
     *            - the sub category, the sub category to be inserted with the user
     *            given details.
     * @return SubCategory
     *         - sub category if the sub category is inserted successfully else throws
     *         exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    SubCategory insertSubCategory(final SubCategory subCategory) throws ApplicationException;

    /**
     * Retrieve the sub category from the sub category by providing the name of the
     * sub category.
     *
     * @param name
     *            - name of the sub category, to retrieve the sub category from the
     *            sub category.
     * @return Boolean.TRUE
     *         - true if the sub category is retrieved successfully else throws
     *         exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    SubCategory retrieveSubCategoryByName(final String name) throws ApplicationException;

}
