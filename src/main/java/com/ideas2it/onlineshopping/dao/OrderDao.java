package com.ideas2it.onlineshopping.dao;

import java.util.List;

import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Order;

public interface OrderDao {

    /**
     * Implemented to insert order detail into database
     *
     * @param order
     *            - contains details like id, delivery date, customer and order
     *            details
     * @return Order - returns the detail of order detail like id, orders and
     *         customer.
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    Order insertOrder(Order order) throws ApplicationException;

    /**
     * Implemeted to get the all the order information from the database.
     *
     * @param customerId
     *            - customerId as reference to get corresponding order details
     * @return Order - returns the detail of order detail like id, orders and
     *         customer.
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    List<Order> getOrders(Long customerId) throws ApplicationException;
}
