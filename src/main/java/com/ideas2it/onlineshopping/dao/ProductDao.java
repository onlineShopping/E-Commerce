package com.ideas2it.onlineshopping.dao;

import java.util.List;

import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Product;
import com.ideas2it.onlineshopping.model.ProductSeller;

/**
 * Implements the Product Management with the insert, in activate,
 * retrieve and update of the product.
 *
 * @author Durgadevi Ananthakrishnan
 */
public interface ProductDao {

    /**
     * Insert the product with the product details like id, name, description, image
     * and price into the product.
     *
     * @param product
     *            - the product, the product to be inserted with the user given
     *            details.
     * @return product
     *         - product if the product is added successfully else throws exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    Product insertProduct(Product product) throws ApplicationException;

    /**
     * Update the product with the user given updated details of the product.
     *
     * @param product
     *            - the product, the product to be updated with the user given
     *            updated details.
     * @return Boolean.TRUE
     *         - true if the product is updated successfully else throws exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    boolean updateProduct(Product product) throws ApplicationException;

    /**
     * Retrieve the product from the product by providing the id of the product.
     *
     * @param id
     *            - id of the product, to retrieve the product from the product.
     * @param isNeededActive
     *            - true if active product is needed else false.
     * @return Boolean.TRUE
     *         - true if the product is retrieved successfully else throws
     *         exception.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    Product retrieveProductById(Long id, boolean isNeededActive) throws ApplicationException;

    /**
     * Retrieve all the products with the product details from the product.
     *
     * @param isNeededAll
     *            - true if all products are needed else false.
     * @return - products, all products available in the product.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    List<Product> retrieveAllProducts(boolean isNeededAll) throws ApplicationException;

    /**
     * Check whether the product detail like the name is existed or not.
     *
     * @param columnName
     *            - column name of the detail, to check the existence of detail.
     * @param detail
     *            - the detail of the product, to check its existence.
     * @return boolean
     *         - true if the detail is already existed in the product else false.
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    boolean isProductDetailExists(String columnName, String detail) throws ApplicationException;

    /**
     * Get all the productsellers with the product and seller details from the 
     * seller id.
     *
     * @param id
     *            - id of the seller to retrieve all the product sellers.
     * @return - productsellers, all products with the sellers .
     * @throws ApplicationException
     *             - throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    List<ProductSeller> retrieveProductSellersBySellerId(Long sellerId) throws ApplicationException;

     /**
    * Retrieve all the products with the product details from the product.
    *
    * @param isNeededAll
    *            - true if all products are needed else false.
    * @return - products, all products available in the product.
    * @throws ApplicationException
    *             - throws the detailed message of the exception with the throwable
    *             cause of the exception
    */
   List<Product> retrieveAllSearchProducts(String name) throws ApplicationException;
}
