package com.ideas2it.onlineshopping.dao;

import com.ideas2it.onlineshopping.exception.ApplicationException;
import com.ideas2it.onlineshopping.model.Shipment;

/**
 * Implements the Shipment Dao with the insert of the shipment.
 */
public interface ShipmentDao {

    /**
     * Implemented to insert shipment details into database
     *
     * @param shipment
     *            - contains shipment details like id, delivery date, delivery
     *            status, delivery address, order.
     * @return shipment
     *         - returns shipment details like id, delivery date, delivery status,
     *         delivery address, order.
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception
     */
    Shipment insertShipment(Shipment shipment) throws ApplicationException;

    /**
     * Implements to get shipment details from shipment
     *
     * @param shipmentId - Id of the shipment
     * @return shipment
     *         - returns shipment details like id, delivery date, delivery status,
     *         delivery address, order.
     * @throws ApplicationException
     *             - Throws the detailed message of the exception with the throwable
     *             cause of the exception.
     */
    Shipment getShipment(Long shipmentId) throws ApplicationException;
}
