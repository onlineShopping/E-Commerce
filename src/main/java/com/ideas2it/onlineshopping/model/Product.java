package com.ideas2it.onlineshopping.model;

import java.util.List;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import javax.persistence.Lob;

import com.ideas2it.onlineshopping.common.Constant;
import com.mysql.jdbc.Blob;

/**
 * Product model entity.
 */
@Entity
@Table(name = Constant.PRODUCT)
public class Product {

    private static final String DESCRIPTION = "description";
    private static final String ID = "id";
    private static final String IMAGE = "image";
    private static final String IS_ACTIVE = "is_active";
    private static final String NAME = "name";
    private static final String PRICE = "price";
    private static final String PRODUCT = "product";
    private static final String PRODUCT_CART = "product_cart";
    private static final String PRODUCT_ID = "product_id";
    private static final String SUBCATEGORY_ID = "subcategory_id";
    private static final String CATEGORY_ID = "category_id";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ID)
    private Long id;

    @Column(name = PRICE)
    private double price;

    @Lob
    @Column(name = IMAGE)
    private Blob image;

    @Column(name = IS_ACTIVE)
    private boolean isActive = Boolean.TRUE;

    @Column(name = NAME)
    private String name;

    @Column(name = DESCRIPTION)
    private String description;

    @ManyToOne
    @JoinColumn(name = SUBCATEGORY_ID)
    private SubCategory subCategory;

    @OneToOne(mappedBy = Constant.PRODUCT)
    @Cascade(CascadeType.ALL)
    private Specification specification;

    @ManyToOne
    @JoinColumn(name = CATEGORY_ID)
    private Category category;

    @OneToMany(mappedBy = Constant.PRODUCT)
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<ProductSeller> productSellers = new ArrayList<>();

    public Product(final Long id, final String name, final String description, final Blob image, final double price) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.image = image;
        this.price = price;
    }

    public Product() {
    }

    /**
     * Getters and Setters
     */
    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(final double price) {
        this.price = price;
    }

    public Blob getImage() {
        return image;
    }

    public void setImage(final Blob image) {
        this.image = image;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(final boolean isActive) {
        this.isActive = isActive;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public SubCategory getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(final SubCategory subCategory) {
        this.subCategory = subCategory;
    }

    public Specification getSpecification() {
        return specification;
    }

    public void setSpecification(final Specification specification) {
        this.specification = specification;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(final Category category) {
        this.category = category;
    }

    public List<ProductSeller> getProductSellers() {
        return productSellers;
    }

    public void setProductSellers(final List<ProductSeller> productSellers) {
        this.productSellers = productSellers;
    }
}
