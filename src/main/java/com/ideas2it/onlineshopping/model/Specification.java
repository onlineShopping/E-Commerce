package com.ideas2it.onlineshopping.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import com.ideas2it.onlineshopping.common.Constant;
/**
 * Specification model entity
 */
@Entity
@Table(name = Constant.SPECIFICATION)
public class Specification {

    private static final String BRAND_NAME = "brand_name";
    private static final String COLOUR = "colour";
    private static final String ID = "id";
    private static final String MODEL_ID = "model_id";
    private static final String MODEL_NAME = "model_name";
    private static final String PRODUCT_ID = "product_id";
    private static final String SIZE = "size";
    private static final String TYPE = "type";
    private static final String WARRANTY = "warranty";

    private Long id;
    private int size;
    private String brandName;
    private String modelId;
    private String modelName;
    private String colour;
    private String type;
    private String warranty;
    private Product product;

    /**
     * Getters and Setters
     */
    public Specification() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ID)
    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }
    @Column(name = SIZE)
    public int getSize() {
        return size;
    }

    public void setSize(final int size) {
        this.size = size;
    }

    @Column(name = BRAND_NAME)
    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(final String brandName) {
        this.brandName = brandName;
    }

    @Column(name = MODEL_ID)
    public String getModelId() {
        return modelId;
    }

    public void setModelId(final String modelId) {
        this.modelId = modelId;
    }

    @Column(name = MODEL_NAME)
    public String getModelName() {
        return modelName;
    }

    public void setModelName(final String modelName) {
        this.modelName = modelName;
    }

    @Column(name = COLOUR)
    public String getColour() {
        return colour;
    }
    public void setColour(final String colour) {
        this.colour = colour;
    }

    @Column(name = TYPE)
    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    @Column(name = WARRANTY)
    public String getWarranty() {
        return warranty;
    }

    public void setWarranty(final String warranty) {
        this.warranty = warranty;
    }

    @OneToOne
    public Product getProduct() {
        return product;
    }

    public void setProduct(final Product product) {
        this.product = product;
    }
}
