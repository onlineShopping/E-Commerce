package com.ideas2it.onlineshopping.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ideas2it.onlineshopping.common.Constant;

/**
 * <p>
 * ProductSeller Model
 * </p>
 *
 * @author Ajaykkumar R
 */
@Entity
@Table(name = Constant.PRODUCT_SELLER)
public class ProductSeller {
    
    private static final String PRODUCT_ID = "product_id";
    private static final String SELLER_ID = "seller_id";
    private static final String QUANTITY = "quantity";
    
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = PRODUCT_ID)
    private Product product;

    @ManyToOne
    @JoinColumn(name = SELLER_ID)
    private Seller seller;

    @Column(name = QUANTITY)
    private int quantity;

    public ProductSeller(final Product product, final Seller seller, final int quantity) {
        this.product = product;
        this.seller = seller;
        this.quantity = quantity;
    }

    public ProductSeller() {
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(final Product product) {
        this.product = product;
    }

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(final Seller seller) {
        this.seller = seller;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(final int quantity) {
        this.quantity = quantity;
    }
}
