package com.ideas2it.onlineshopping.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.ideas2it.onlineshopping.common.Constant;

/**
 * Order Model
 */
@Entity @Table(name = Constant.CUSTOMER_ORDER)
public class Order {

    private static final String CUSTOMER_ID = "customer_id";
    private static final String ORDER_DETAILS = "orderDetail";
    private static final String ORDER = "order";

    private Long id;
    private List<OrderDetail> orderDetails;
    private Payment payment;
    private Customer customer;

    public Order() {
    }

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    @OneToMany(mappedBy = ORDER, fetch=FetchType.EAGER)
    @Cascade(CascadeType.SAVE_UPDATE)
    public List<OrderDetail> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(final List<OrderDetail> orderDetails) {
        this.orderDetails = orderDetails;
    }

    @ManyToOne
    @Cascade(CascadeType.SAVE_UPDATE)
    @JoinColumn(name = CUSTOMER_ID)
    public Customer getCustomer() {
        return customer;
    }
    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }

    @OneToOne(mappedBy = ORDER)
    @Cascade(CascadeType.SAVE_UPDATE)
    public Payment getPayment() {
        return payment;
    }
    public void setPayment(final Payment payment) {
        this.payment = payment;
    }
}
