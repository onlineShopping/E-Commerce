package com.ideas2it.onlineshopping.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.ideas2it.onlineshopping.common.Constant;

/**
 * getters and setters
 */
@Entity
@Table(name = Constant.PAYMENT)
public class Payment {

    private static final String PAYMENT_TYPE = "payment_type";
    private static final String CARD_NUMBER = "card_number";
    private static final String PAYMENT_TIME = "payment_time";
    private static final String AMOUNT = "amount";
    private static final String ORDER_ID = "order_id";

    private Long id;
    private String paymentType;
    private Calendar paymentTime;
    private Long cardNumber;
    private double amount;
    private Order order;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    @Column(name = PAYMENT_TYPE)
    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(final String paymentType) {
        this.paymentType = paymentType;
    }

    @Column(name = PAYMENT_TIME)
    public Calendar getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(final Calendar paymentTime) {
        this.paymentTime = paymentTime;
    }

    @Column(name = CARD_NUMBER)
    public Long getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(final Long cardNumber) {
        this.cardNumber = cardNumber;
    }

    @Column(name = AMOUNT)
    public double getAmount() {
        return amount;
    }

    public void setAmount(final double amount) {
        this.amount = amount;
    }

    @OneToOne
    @Cascade(CascadeType.SAVE_UPDATE)
    @JoinColumn(name = ORDER_ID)
    public Order getOrder() {
        return order;
    }

    public void setOrder(final Order order) {
        this.order = order;
    }
}
