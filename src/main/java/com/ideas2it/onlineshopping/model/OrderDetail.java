package com.ideas2it.onlineshopping.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.ideas2it.onlineshopping.common.Constant;

/**
 * OrderDetail model
 */
@Entity
@Table(name = Constant.ORDER_DETAIL)
public class OrderDetail {

    private static final String ID = "id";
    private static final String ACTIVE = "active";
    private static final String AMOUNT = "amount";
    private static final String QUANTITY = "quantity";
    private static final String ORDER_TIME = "order_time";
    private static final String STATUS = "delivery_status";
    private static final String ORDER_ID = "order_id";
    private static final String PRODUCT_ID = "product_id";
    private static final String ORDER_DETAIL = "orderDetail";
    private static final String SHIPMENT_ID = "shipment_id";
    private static final String CART_ID = "cartId";

    private Long id;
    private Long cartId;
    private Product product;
    private int quantity;
    private double amount;
    private Calendar orderTime;
    private Shipment shipment;
    private Order order;
    private Invoice invoice;
    private String status = Constant.FOR_DELIVERY;
    private boolean isActive = Boolean.TRUE;

    @ManyToOne
    @Cascade(CascadeType.SAVE_UPDATE)
    @JoinColumn(name = ORDER_ID)
    public Order getOrder() {
        return order;
    }

    public void setOrder(final Order order) {
        this.order = order;
    }

    @Id

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    @Column(name = AMOUNT)
    public double getAmount() {
        return amount;
    }

    public void setAmount(final double amount) {
        this.amount = amount;
    }

    @OneToOne(mappedBy = ORDER_DETAIL)
    @Cascade(CascadeType.SAVE_UPDATE)
    public Shipment getShipment() {
        return shipment;
    }

    public void setShipment(final Shipment shipment) {
        this.shipment = shipment;
    }

    @Column(name = QUANTITY)
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(final int quantity) {
        this.quantity = quantity;
    }

    @Column(name = ORDER_TIME)
    public Calendar getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(final Calendar orderTime) {
        this.orderTime = orderTime;
    }

    @Column(name = STATUS)
    public String getStatus() {
        return status;
    }

    public void setStatus(final String status) {
        this.status = status;
    }

    @Column(name = ACTIVE)
    public boolean isActive() {
        return isActive;
    }

    public void setActive(final boolean isActive) {
        this.isActive = isActive;
    }

    @ManyToOne
    @Cascade(CascadeType.SAVE_UPDATE)
    @JoinColumn(name = PRODUCT_ID)
    public Product getProduct() {
        return product;
    }

    public void setProduct(final Product product) {
        this.product = product;
    }

    @OneToOne(mappedBy = ORDER_DETAIL)
    @Cascade(CascadeType.SAVE_UPDATE)
    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(final Invoice invoice) {
        this.invoice = invoice;
    }

    @Column(name = CART_ID)
    public Long getCartId() {
        return cartId;
    }

    public void setCartId(final Long cartId) {
        this.cartId = cartId;
    }
}
