package com.ideas2it.onlineshopping.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.ideas2it.onlineshopping.common.Constant;
/**
 * Category model entity.
 */
@Entity
@Table(name = Constant.CATEGORY)
public class Category {

    private static final String CATEGORY = "category";
    private static final String DESCRIPTION = "description";
    private static final String ID = "id";
    private static final String IS_ACTIVE = "is_active";
    private static final String NAME = "name";

    private boolean isActive = Boolean.TRUE;
    private Long id;
    private String name;
    private String description;
    private List<SubCategory> subCategories = new ArrayList<SubCategory>();
    private List<Product> products = new ArrayList<Product>();

    public Category(final Long id, final String name, final String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Category() {
    }

    /**
     * Getters and Setters
     */
    @Column(name = IS_ACTIVE)
    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(final boolean isActive) {
        this.isActive = isActive;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ID)
    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    @Column(name = NAME)
    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    @Column(name = DESCRIPTION)
    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @OneToMany(mappedBy = CATEGORY)
    public List<SubCategory> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(final List<SubCategory> subCategories) {
        this.subCategories = subCategories;
    }

    @OneToMany(mappedBy = CATEGORY)
    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(final List<Product> products) {
        this.products = products;
    }
}
