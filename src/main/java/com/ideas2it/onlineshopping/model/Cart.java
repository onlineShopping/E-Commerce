package com.ideas2it.onlineshopping.model;

import java.util.Calendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.ideas2it.onlineshopping.common.Constant;

/**
 * Cart model
 */
@Entity
@Table(name = Constant.CART)
public class Cart {

    private static final String CARTS = "carts";
    private static final String CUSTOMER_ID = "customer_id";
    private static final String TIME = "time";
    private static final String MODIFIED_TIME = "modified_time";
    private static final String CART = "cart";


    private Long id;
    private Calendar time;
    private Calendar modifiedTime;
    private Customer customer;
    private List<CartDetail> cartDetails;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    @OneToOne
    @JoinColumn(name=CUSTOMER_ID)
    public Customer getCustomer() {
        return customer;
    }
    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }

    @OneToMany(mappedBy = CART)
    @Cascade(CascadeType.SAVE_UPDATE)
    public List<CartDetail> getCartDetails() {
        return cartDetails;
    }
    public void setCartDetails(final List<CartDetail> cartDetails) {
        this.cartDetails = cartDetails;
    }

    @Column(name = TIME)
    public Calendar getTime() {
        return this.time;
    }

    public void setTime(final Calendar time) {
        this.time = time;
    }

    @Column(name = MODIFIED_TIME)
    public Calendar getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(final Calendar modifiedTime) {
        this.modifiedTime = modifiedTime;
    }
}