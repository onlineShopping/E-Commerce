package com.ideas2it.onlineshopping.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.ideas2it.onlineshopping.common.Constant;
/**
 * CartDetail model
 */
@Entity
@Table(name = Constant.CART_DETAIL)
public class CartDetail {

    private static final String PRODUCT_ID = "product_id";
    private static final String CART_ID = "cart_Id";
    private static final String QUANTITY = "quantity";
    public static final String ACTIVE = "is_active";

    private Long id;
    private Product product;
    private int quantity = Constant.ONE;
    private Cart cart;
    private boolean isActive = Boolean.TRUE;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }
    public void setId(final Long id) {
        this.id = id;
    }

    @OneToOne
    @JoinColumn(name = PRODUCT_ID)
    public Product getProduct() {
        return product;
    }
    public void setProduct(final Product product) {
        this.product = product;
    }

    @Column(name = QUANTITY)
    public int getQuantity() {
        return quantity;
    }
    public void setQuantity(final int quantity) {
        this.quantity = quantity;
    }

    @ManyToOne
    @JoinColumn(name = CART_ID)
    public Cart getCart() {
        return cart;
    }
    public void setCart(final Cart cart) {
        this.cart = cart;
    }

    @Column(name = ACTIVE)
    public boolean getIsActive() {
        return isActive;
    }
    public void setIsActive(final boolean isActive) {
        this.isActive = isActive;
    }
}
