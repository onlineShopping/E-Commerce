package com.ideas2it.onlineshopping.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.ideas2it.onlineshopping.common.Constant;

/**
 * Invoice model entity
 */
@Entity
@Table(name = Constant.INVOICE)
public class Invoice {

    private static final String INVOICE_DATE = "invoice_date";
    private static final String ORDER_DATE = "order_date";
    private static final String ORDER_DETAIL_ID = "order_detail_id";

    private Long id;
    private Calendar invoiceDate;
    private Calendar orderDate;
    private OrderDetail orderDetail;

    public Invoice() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    @Column(name = INVOICE_DATE)
    public Calendar getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(final Calendar invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    @Column(name = ORDER_DATE)
    public Calendar getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(final Calendar orderDate) {
        this.orderDate = orderDate;
    }

    @OneToOne
    @Cascade(CascadeType.SAVE_UPDATE)
    @JoinColumn(name = ORDER_DETAIL_ID)
    public OrderDetail getOrderDetail() {
        return orderDetail;
    }

    public void setOrderDetail(final OrderDetail orderDetail) {
        this.orderDetail = orderDetail;
    }

}
