package com.ideas2it.onlineshopping.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.ideas2it.onlineshopping.common.Constant;

/**
 * <p>
 * Seller Model
 * </p>
 *
 * @author Ajaykkumar R
 */
@Entity
@Table(name = Constant.SELLER)
public class Seller {
    
    private static final String IS_ACTIVE = "is_active";
    
    @Column(name = IS_ACTIVE)
    private boolean isActive = Boolean.TRUE;

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @OneToOne
    @Cascade(CascadeType.ALL)
    @JoinColumn(name = Constant.USER_ID)
    private  User user;

    @OneToMany(mappedBy = Constant.SELLER)
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Address> addresses = new ArrayList<>();

    @OneToMany(mappedBy = Constant.SELLER, fetch=FetchType.EAGER)
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<ProductSeller> productSellers = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(final List<Address> addresses) {
        this.addresses = addresses;
    }

    public List<ProductSeller> getProductSellers() {
        return productSellers;
    }

    public void setProductSellers(final List<ProductSeller> productSellers) {
        this.productSellers = productSellers;
    }
    
    public User getUser() {
        return user;
    }

    public void setUser(final User user) {
        this.user = user;
    }
    
    public boolean getActive() {
        return isActive;
    }

    public void setActive(final boolean isActive) {
        this.isActive = isActive;
    }
}

