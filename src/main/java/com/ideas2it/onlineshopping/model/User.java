package com.ideas2it.onlineshopping.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.ideas2it.onlineshopping.common.Constant;

/**
 * <p>
 * User Model
 * </p>
 *
 * @author Ajaykkumar R
 */
@Entity
@Table(name = Constant.USER)
public class User {
    
    private static final String IS_ACTIVE = "is_active";
    private static final String NAME = "name";
    private static final String USER_NAME = "user_name";
    private static final String EMAIL_ID = "email_id";
    private static final String PASSWORD = "password";
    private static final String PHONE_NUMBER = "phone_number";
    private static final String ROLE = "role";
    
    @Column(name = IS_ACTIVE)
    private boolean isActive = Boolean.TRUE;
    
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = NAME)
    private String name;

    @Column(name = EMAIL_ID)
    private String emailId;

    @Column(name = USER_NAME)
    private String userName;

    @Column(name = PASSWORD)
    private String password;
    
    @Column(name = PHONE_NUMBER)
    private Long phoneNumber;

    @Column(name = ROLE)
    private String role;
    
    @OneToOne
    private Customer customer;
    
    @OneToOne
    private Seller seller;
    
    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(final String emailId) {
        this.emailId = emailId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(final String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }
    
    public void setPhoneNumber(final Long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    
    public Long getPhoneNumber() {
        return phoneNumber;
    }
    
    public String getRole() {
        return role;
    }

    public void setRole(final String role) {
        this.role = role;
    }
    
    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(final boolean isActive) {
        this.isActive = isActive;
    }
    
    public Seller getSeller() {
        return seller;
    }

    public void setSeller(final Seller seller) {
        this.seller = seller;
    }
    
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }
}

