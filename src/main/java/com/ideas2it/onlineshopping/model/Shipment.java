package com.ideas2it.onlineshopping.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.ideas2it.onlineshopping.common.Constant;

/**
 * getters and setters
 */
@Entity
@Table(name = Constant.SHIPMENT)
public class Shipment {

    private static final String DELIVER_ADDRESS = "delivery_address";
    private static final String DELIVER_DATE = "delivery_date";
    private static final String PRICE = "price";
    private static final String ORDER_DETAIL_ID = "order_detail_id";

    private Long id;
    private Calendar deliveryDate;
    private Address address;
    private double price;
    private OrderDetail orderDetail;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }
    public void setId(final Long id) {
        this.id = id;
    }

    @Column(name = DELIVER_DATE)
    public Calendar getDeliveryDate() {
        return deliveryDate;
    }
    public void setDeliveryDate(final Calendar deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    @OneToOne
    @JoinColumn(name = DELIVER_ADDRESS)
    public Address getAddress() {
        return address;
    }
    public void setAddress(final Address address) {
        this.address = address;
    }

    @Column(name = PRICE)
    public double getPrice() {
        return price;
    }
    public void setPrice(final double price) {
        this.price = price;
    }

    @OneToOne
    @JoinColumn(name = ORDER_DETAIL_ID)
    public OrderDetail getOrderDetail() {
        return orderDetail;
    }
    public void setOrderDetail(final OrderDetail orderDetail) {
        this.orderDetail = orderDetail;
    }
}

