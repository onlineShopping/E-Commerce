package com.ideas2it.onlineshopping.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.ideas2it.onlineshopping.common.Constant;

/**
 * Sub category model entity.
 */
@Entity
@Table(name = Constant.SUB_CATEGORY)
public class SubCategory {

    private static final String CATEGORY_ID = "category_id";
    private static final String DESCRIPTION = "description";
    private static final String ID = "id";
    private static final String IS_ACTIVE = "is_active";
    private static final String NAME = "name";
    private static final String SUB_CATEGORY = "subCategory";

    private boolean isActive = Boolean.TRUE;
    private Long id;
    private String name;
    private String description;
    private Category category;
    private List<Product> products = new ArrayList<>();

    public SubCategory(final Long id, final String name, final String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public SubCategory() {
    }

    /**
     * Getters and Setters
     */
    @Column(name = IS_ACTIVE)
    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(final boolean isActive) {
        this.isActive = isActive;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ID)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = NAME)
    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    @Column(name = DESCRIPTION)
    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @ManyToOne
    @JoinColumn(name = CATEGORY_ID)
    public Category getCategory() {
        return category;
    }

    public void setCategory(final Category category) {
        this.category = category;
    }

    @OneToMany(mappedBy = SUB_CATEGORY)
    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(final List<Product> products) {
        this.products = products;
    }
}
