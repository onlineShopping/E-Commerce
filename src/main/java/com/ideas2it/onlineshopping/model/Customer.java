package com.ideas2it.onlineshopping.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.ideas2it.onlineshopping.common.Constant;

/**
 * <p>
 * Customer Model
 * </p>
 *
 * @author Ajaykkumar R
 */
@Entity
@Table(name = Constant.CUSTOMER)
public class Customer {

    private static final String IS_ACTIVE = "isActive";
    private boolean isActive = Boolean.TRUE;
    private Long id;
    private List<Address> addresses = new ArrayList<>();
    private Cart cart;
    private User user;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    @OneToMany(mappedBy = Constant.CUSTOMER)
    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(final List<Address> addresses) {
        this.addresses = addresses;
    }

    @OneToOne(mappedBy = Constant.CUSTOMER)
    public Cart getCart() {
        return cart;
    }

    public void setCart(final Cart cart) {
        this.cart = cart;
    }

    @OneToOne
    @Cascade(CascadeType.ALL)
    @JoinColumn(name = Constant.USER_ID)
    public User getUser() {
        return user;
    }

    public void setUser(final User user) {
        this.user = user;
    }

    @Column(name = IS_ACTIVE)
    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(final boolean isActive) {
        this.isActive = isActive;
    }
}
