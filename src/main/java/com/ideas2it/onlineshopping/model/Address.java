package com.ideas2it.onlineshopping.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ideas2it.onlineshopping.common.Constant;

/**
 * <p>
 * Address Model
 * </p>
 *
 * @author Ajaykkumar R
 */
@Entity
@Table(name = Constant.ADDRESS)
public class Address {

    private static final String IS_ACTIVE = "is_active";
    private static final String DOOR_NUMBER = "door_number";
    private static final String STREET = "street";
    private static final String AREA = "area";
    private static final String CITY = "city";
    private static final String STATE = "state";
    private static final String PINCODE = "pincode";
    private static final String CUSTOMER_ID = "customer_id";
    private static final String SELLER_ID = "seller_id";

    @Column(name = IS_ACTIVE)
    boolean isActive = Boolean.TRUE;

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = DOOR_NUMBER)
    private String doorNumber;

    @Column(name = STREET)
    private String street;

    @Column(name = AREA)
    private String area;

    @Column(name = CITY)
    private String city;

    @Column(name = STATE)
    private String state;

    @Column(name = PINCODE)
    private String pincode;

    @ManyToOne
    @JoinColumn(name = CUSTOMER_ID)
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = SELLER_ID)
    private Seller seller;

    public Address(final String doorNumber, final String street, final String area, final String city, final String country, final String pincode,
            final Customer customer, final Seller seller) {
        this.doorNumber = doorNumber;
        this.street = street;
        this.area = area;
        this.city = city;
        this.state = state;
        this.pincode = pincode;
        this.customer = customer;
        this.seller = seller;
    }

    public Address() {
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getDoorNumber() {
        return doorNumber;
    }

    public void setDoorNumber(final String doorNumber) {
        this.doorNumber = doorNumber;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(final String street) {
        this.street = street;
    }

    public String getArea() {
        return area;
    }

    public void setArea(final String area) {
        this.area = area;
    }

    public String getCity() {
        return city;
    }

    public void setCity(final String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(final String state) {
        this.state = state;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(final String pincode) {
        this.pincode = pincode;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(final Seller seller) {
        this.seller = seller;
    }

    public boolean getActive() {
        return isActive;
    }

    public void setActive(final boolean isActive) {
        this.isActive = isActive;
    }
}
