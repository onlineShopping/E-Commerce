package com.ideas2it.onlineshopping.logger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ideas2it.onlineshopping.exception.ApplicationException;

/**
 * Log all the exceptions and store them in the log files.
 */
public class ApplicationLogger {

    private static Logger LOGGER;

    public ApplicationLogger(final String className) {
        LOGGER = LogManager.getLogger(className);
    }

    /**
     * Logs informational messages that highlight the progress
     * of the application.
     *
     * @param contextMessage
     *            - the message , givens the information.
     */
    public void info(final String contextMessage) {
        LOGGER.info(contextMessage);
    }

    /**
     * Logs fine-grained informational events that are most
     * useful to debug an application.
     *
     * @param contextMessage
     *            - the message, gives the information.
     */
    public void debug(final String contextMessage) {
        LOGGER.debug(contextMessage);
    }

    /**
     * Logs error events that might still allow the application to
     * continue running.
     *
     * @param contextMessage
     *            - the message, gives the information.
     * @param exception
     *            - the exception occured during the execution.
     */
    public void error(final String contextMessage, final Throwable cause) {
        LOGGER.error(contextMessage, cause);
    }

    /**
     * Logs potentially harmful situations.
     *
     * @param contextMessage
     *            - the message, gives the information.
     * @param exception
     *            - the exception occured during the execution.
     */
    public void warn(final String contextMessage, final ApplicationException exception) {
        LOGGER.warn(contextMessage, exception);
    }
}
