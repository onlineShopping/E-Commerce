<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

</head>
<c:choose>
                 <c:when test="${sessionScope.userId == '1'}">
                    <%@ include file = "headeradmin.jsp" %>
                 </c:when>
                 <c:otherwise>
                    <%@ include file = "headerSeller.jsp" %>
                 </c:otherwise>
              </c:choose>
<h1>Add Product</h1>
<body>
     <form:form method="post" action="Product" modelAttribute="product">
  <div class="form-group" style="
    align-content:  center;
    margin-left: 240px;
    width: 50%;">
    <form:label class="col-form-label" path="name">Product Name</form:label>
    <form:input type="text" class="form-control" path="name" id="name" value="${product.name}" maxlength="100" placeholder="Product Name" required="true"/>
  </div>
 <div class="form-group" style="
    align-content:  center;
    margin-left: 240px;
    width: 50%;">
    <form:label path="description">Product Description</form:label>
    <form:textarea class="form-control" id="description" path="description" value="${product.description}" maxlength="286" rows="3" required="true"></form:textarea>
  </div>
  <div class="form-group" style="
    align-content:  center;
    margin-left: 240px;
    width: 50%;">
    <form:label class="col-form-label" path="price">Product Price</form:label>
    <form:input type="double" class="form-control" path="price" id="price" value="${product.price}" maxlength="100" placeholder="Product Price" required="true"/>
  </div>
  <div class="form-group" style="
    align-content:  center;
    margin-left: 240px;
    width: 50%;">
    <form:label path="image">Upload Product Image</form:label>
    <form:input type="file" class="form-control-file" id="image" path="image"/>
    <button type="submit"  class="btn btn-primary">Submit</button>
  </div>
 <input type="hidden" name="isNeededActive" value="false"/>
 <input type="hidden" name="isNeededAll" value="true"/>
 <input type="hidden" name="action" value="update"/>
 <input type="hidden" name="id" value="${product.id}"/>
 <input type="hidden" name="seller_id" value="${sessionScope.seller_id}"/>
</form:form>
</body>
