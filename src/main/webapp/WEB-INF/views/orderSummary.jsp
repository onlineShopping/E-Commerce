<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
<%@ include file = "headercustomer.jsp" %>
</head>
	
    <div id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                  <li><a href="#">Home</a></li>
                  <li class="active">Order Summary</li>
                </ol>
            </div>
            <div class="table-responsive cart_info">
                <table class="table table-condensed">
                    <thead>
                        <tr class="cart_menu">
                            <td class="image">Item</td>
                            <td class="description"></td>
                            <td class="price">Price</td>
                            <td class="quantity">Quantity</td>
                            <td class="total">Total</td>
                            <td></td>
                        </tr>
                    </thead>
                    
          <!-- main table -->
                    <tbody>
                    <c:forEach items="${cartDetails}" var="cartDetail">
                        <tr>
                            <td class="cart_product">
                                <a href=""><img src="${pageContext.request.contextPath}/resources/images/${cartDetail.product.id}.jpeg" alt="" width="110" height="110"></a>
                            </td>
                            <td class="cart_description">
                                <h4>${cartDetail.product.name}</h4>
                                <p>Product ID: ${cartDetail.product.id}</p>
                            </td>
                            <td class="cart_price">
                                <p>${cartDetail.product.price}</p>
                            </td>
                            <td class="cart_quantity">
                                <div class="cart_quantity_button">
                                    <p>${cartDetail.quantity}</p>
                                </div>
                            </td>
                            <td class="cart_total">
                                <p class="cart_total_price">Rs ${cartDetail.product.price * cartDetail.quantity}</p>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div> 
              <!-- table entry end -->
  
  
  
  <!--/amount summary-->

    <div id="do_action">
        <div class="container">
            <div class="row">

                <div class="col-sm-12">
                    <div class="total_area">
                        <ul>
                        <c:forEach items="${cartDetails}" var="cartDetail">
                        <c:set var="amount" value="${cartDetail.product.price * cartDetail.quantity}"/>
                        <c:set var="totalAmount" value="${totalAmount + amount}"/>
                            
                         </c:forEach>
                         <li>Cart Sub Total <span>Rs ${totalAmount}</span></li>
                         <c:set var="gst" value="${totalAmount * 0.18}"/>
                            <li>GST Tax <span>Rs ${gst}</span></li>
                            <li>Shipping Cost <span>Free</span></li>
                            <c:set var="totalAmount" value="${totalAmount + gst}"/>
                            <li>Total <span>Rs ${totalAmount}</span></li>
                            <c:set var="totalAmount" value="${totalAmount}" scope="session"/>
                            <c:set var="price" value="${totalAmount}" scope="session"/>
                        
                        </ul>
                    </div>
                </div>
                <div class="col-sm-9"></div>
                <form action="Shipment" method="post">
                <input type="hidden" name="action" value="goToShipment"/>
                <input type="hidden" name="customerId" value="${sessionScope.customerId}"/>
                <input type="hidden" name="totalAmount" value="${totalAmount}"/>
					<button type="submit" class="btn btn-default check_out">Next  </a>
                </form>
                </div>
			</div>
	</section><!--/#do_action-->

    <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/jquery.scrollUp.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/jquery.prettyPhoto.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/main.js"></script>
</body>
</html>
