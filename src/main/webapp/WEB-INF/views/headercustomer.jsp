<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OnlineShop</title>
    <link href="resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="resources/css/font-awesome.min.css" rel="stylesheet">
    <link href="resources/css/prettyPhoto.css" rel="stylesheet">
    <link href="resources/css/price-range.css" rel="stylesheet">
    <link href="resources/css/animate.css" rel="stylesheet">
    <link href="resources/css/main.css" rel="stylesheet">
    <link href="resources/css/responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">   
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>
  
  <style>
    .btn-info {
    color: #000000 ;
    background-color:#D3D3D3 !important;
    border-color: #D3D3D3 !important;
    }
    .navbar-collapse {
    padding-right: 0px !important;
    }
    .navbar-collapse.collapse {
    padding-left: 102px;
    margin-left: 125px;
    }
    img {
    margin-bottom: -60px;
    }
    section 
    {
    background:#fff;
    width:600px;
    height:300px;
    margin:100px auto;
    position:relative
    }
    section> input 
    {
    margin:10px 25px;
    width:200px;
    height:20px;
    padding:5px;
    border:none;
    border-bottom:1px solid black;
    outline:0
    }
    input:first-of-type
    {
    /*   position:relative; */
    /*   top:30%; */
    margin-top: 1px;
    }
    .carousel-control.left span, .carousel-control.right span {
    background-color: #f !important;    
    }
    .carousel-indicators .active {
    height: 4px !important;
    background-color:transparent !important;
    }
    .carousel-control {
     width:3% !important;
     }
     .header-bottom {
    padding-bottom: 0px !important;
    padding-top: 0px !important;
}
  </style>
  <body>
    <!--/header_top-->
    <header id="header">
      <!--header-->
      <header id="header">
      <!--header-->
      <div class="header_top">
        <!--header_top-->
        <div class="container">
          <div class="row">
            <div class="col-sm-6">
              <div class="contactinfo">
              </div>
            </div>
            <div class="col-sm-6">
              <div class="social-icons pull-right">
                <ul class="nav navbar-#nav">
                  <li><a href="Order?action=displayOrderDetail&customerId=${sessionScope.customerId}" style="padding-top:8px;margin: 0 1em;">Orders</a></li>
                    <div id="signupmodal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Sign up Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Sign Up</h4>
                </div>
                <div class="modal-body" id="signupmodal" style="height:75%;">
                <form name="form" action="Customer" method="post" onsubmit="return validate()">
                    <p>Name:</p>
                    <input type="text" class="form-control modalinputs" id="firstname" name="name" placeholder="Enter your first name" maxlength="40" required/>
                    <p>User name:</p>
                    <input type="text" class="form-control modalinputs" id="lastname" name="userName" placeholder="Enter your last name" maxlength="40" required/>
                    <p>PhoneNumber:</p>
                    <input type="text" class="form-control modalinputs" id="signup-email" name="phoneNumber" placeholder="Enter your phonenumber" pattern="[0-9]{10}" title="Please type the correct input" maxlength="10" required/>
                    <p>Email:</p>
                    <input type="text" class="form-control modalinputs" id="signup-email" name="emailId" placeholder="Enter your email" pattern= "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$" maxlength="40" required/>
                    <p>Password:</p>
                    <input type="password" class="form-control modalinputs" id="txtPassword" name="password" placeholder="Enter your password" maxlength="40" required/>
                    <p>Confirm Password:</p>
                    <input type="password" class="form-control modalinputs" id="txtConfirmPassword" placeholder="Re-enter your password" maxlength="40" required/>
                    <button type="submit" id="btnSubmit" style="float:right;margin-top: 16px;" class="btn btn-default">Sign Up</button>
                  <input type="hidden" name="role" value="customer"/>
                  <input type="hidden" name="action" value="create"/>
                    </form>
                </div>
                
            </div>
        </div>
    </div>
  <script type="text/javascript">
    $(function () {
        $("#btnSubmit").click(function () {
            var password = $("#txtPassword").val();
            var confirmPassword = $("#txtConfirmPassword").val();
            if (password != confirmPassword) {
                alert("Passwords do not match.");
                return false;
            }
            return true;
        });
    });
</script>
    <!-- Log In Modal -->
    <div id="loginmodal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Log In Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Log In</h4>
                </div>
                <div class="modal-body" id="modal2">
                <form name="form" action="User" method="post" onsubmit="return validate()">
                    <p>Username:</p>
                    <input type="text" class="form-control modalinputs" id="login-email" name="userName" placeholder="Enter your username" maxlength="40" required>
                    <p>Password:</p>
                    <input type="password" class="form-control modalinputs" id="login-password" name="password" placeholder="Enter your password" maxlength="40" required>
                    <p>${requestScope.message}</p>
                
                    <button type="submit" class="btn btn-default" style="margin-left: 502px;
    margin-top: 20px;">Log In</button>
                  <input type="hidden" name="action" value="login"/>
                    </form>
                </div>
            </div>
        </div>
    </div>
                  <li><a href="Logout?role=customer" ><i class="fa fa-sign-out" aria-hidden="true">Logout</i> </li>
                  <li><a href="#"   style="  margin-top: 8px;
    margin-left: 5px;">Hi ${sessionScope.userName} !</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--/header_top-->
      <div class="header-middle" color="orange">
        <!--header-middle-->
        <div class="container">
          <div class="row">
              <div class="logo pull-left">
                <a href="User?action=redirect&name=home"><img src="resources/images/logo.png" alt="" /></a>
             <div class="search_box pull-right">
              <form action="Product" method="get">
            <input type="hidden" name="action" value="search">
                <input type="text" name="name" placeholder="Search" required/>
              <button type="submit" class="btn btn-info btn-lg" style="
    margin-left: 175px;
    margin-top: -9px;
">
              Search</button>
              </form>
             <form action="Cart" method="get">
             <input type="hidden" name="action" value="displayCart"/>
             <input type="hidden" name="customerId" value="${sessionScope.customerId}"/>
              <button type="submit" class="btn btn-info btn-lg" style="margin-top: -61px;margin-left: 817px;" >Cart</button>
              </a>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="header-bottom" style="    padding-bottom: -59px;
    padding-top: -55px;">
        <!--header-bottom-->
          <div class="container">
          <div class="row">
            <div class="col-sm-9">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
              </div>
              <div class="mainmenu pull-left">
                <ul class="nav navbar-nav collapse navbar-collapse">
                  <li class="dropdown">
                    <a href="#">Electronics<i class="fa fa-angle-down"></i></a>
                    <ul role="menu" class="sub-menu">
                      <li><a href="SubCategory?action=redirect&formName=display&name=Mobiles">Mobiles</a></li>
                      <li><a href="SubCategory?action=redirect&formName=display&name=Laptops">Laptops</a></li>
                      <li><a href="SubCategory?action=redirect&formName=redirect&name=null">Camera</a></li>
                      <li><a href="SubCategory?action=redirect&formName=redirect&name=null">Accessories</a></li>
                    </ul>
                  </li>
                  <li class="dropdown">
                    <a href="#">Appliances<i class="fa fa-angle-down"></i></a>
                    <ul role="menu" class="sub-menu">
                      <li><a href="SubCategory?action=redirect&formName=redirect&name=null">Televisions</a></li>
                      <li><a href="SubCategory?action=redirect&formName=redirect&name=null">Air Conditioners</a></li>
                      <li><a href="SubCategory?action=redirect&formName=redirect&name=null">Washing Machines</a></li>
                    </ul>
                  </li>
                  <li class="dropdown">
                    <a href="#">Men<i class="fa fa-angle-down"></i></a>
                    <ul role="menu" class="sub-menu">
                      <li><a href="SubCategory?action=redirect&formName=redirect&name=null">Footwear</a></li>
                      <li><a href="SubCategory?action=redirect&formName=redirect&name=null">Watches</a></li>
                    </ul>
                  </li>
                  <li class="dropdown">
                    <a href="#">Women<i class="fa fa-angle-down"></i></a>
                    <ul role="menu" class="sub-menu">
                      <li><a href="SubCategory?action=redirect&formName=redirect&name=null">KurtasAndKurtis</a></li>
                    </ul>
                  </li>
                  <li class="dropdown">
                    <a href="#">Baby & Kids<i class="fa fa-angle-down"></i></a>
                    <ul role="menu" class="sub-menu">
                      <li><a href="SubCategory?action=redirect&formName=redirect&name=null">Toys</a></li>
                      <li><a href="SubCategory?action=redirect&formName=redirect&name=null">Baby Boy Clothing</a></li>
                      <li><a href="SubCategory?action=redirect&formName=redirect&name=null">Baby Girl Clothing</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--/header-bottom-->
    </header>
    </html>
