<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="css/responsive.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
  <body>
    <style>
      .selector-for-some-widget {
      -webkit-box-sizing: border-box;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
      }
      .add-to-cart {
    margin-bottom: 0px !important;
}
    </style>
    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
    </head>
    <body>
      <h1>Popular Laptops!!!</h1>
      <div class="card-deck" align="center">
        <div class="card" style="padding-top:19px">
          <a href="login.html"><img class="card-img-top" src="images/products/laptop/laptop1.jpeg" alt="Card image cap" width="50" height="150"></a>
          <div class="card-block">
            <h4 class="card-title">Acer ES 11 Celeron Dual Core 4th Gen - (2 GB/500 GB HDD/Windows 10 Home) ES1-132 Laptop  (11.6 inch, Black, 1.25 kg)</h4>
            <p class="card-text">₹18990</p>
          </div>
          <div class="card-footer">
            <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>	
          </div>
        </div>
        <div class="card" align ="center" style="padding-top:19px">
          <img class="card-img-top" src="images/products/laptop/laptop2.jpeg" alt="Card image cap"  width="100" height="200">
          <div class="card-block">
            <h4 class="card-title">Lenovo Core i3 6th Gen - (4 GB/1 TB HDD/Windows 10 Home) Ideapad 110 Laptop  (15.6 inch, Black, 2.2 kg)</h4>
            <p class="card-text">₹13,999</p>
          </div>
          <div class="card-footer" align="center">
            <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>								
          </div>
        </div>
        <div class="card" align="center" style="padding-top:19px">
          <img class="card-img-top" src="images/products/laptop/laptop3.jpeg"  alt="Card image cap"  width="100" height="200">
          <div class="card-block">
            <h4 class="card-title">Dell Inspiron Core i3 6th Gen - (4 GB/1 TB HDD/Linux) 5559 Laptop  (15.6 inch, Black, 2.36 kg)</h4>
            <p class="card-text">₹69,999</p>
          </div>
          <div class="card-footer">
            <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>	
          </div>
        </div>
        <div class="card" align="center" style="padding-top:19px">
          <img class="card-img-top" src="images/products/laptop/laptop4.jpeg"  alt="Card image cap"  width="100" height="200">
          <div class="card-block">
            <h4 class="card-title">Apple MacBook Pro Core i7 7th Gen - (16 GB/256 GB SSD/Mac OS Sierra/2 GB Graphics) MPTU2HN/A  (15.4 inch, SIlver, 1.83 kg)</h4>
            <p class="card-text">₹80,000</p>
          </div>
          <div class="card-footer">
            <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>	
          </div>
        </div>
      </div>

<br><br>

  </body>
