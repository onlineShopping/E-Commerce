<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<head>
<c:choose>
                 <c:when test="${sessionScope.userId == '1'}">
                    <%@ include file = "headeradmin.jsp" %>
                 </c:when>
                 <c:otherwise>
                    <%@ include file = "headerSeller.jsp" %>
                 </c:otherwise>
              </c:choose>

</head>
<br>
<br>
<h2>Update Specification</h2>
<br>
<form:form method="post" action="Specification" modelAttribute="specification">
  
   <div class="form-group" style="
    align-content:  center;
    margin-left: 240px;
    width: 50%;">
    <form:label path="modelId">Model Id</form:label>
    <form:input type="text" class="form-control" path="modelId" id="modelId" value="${specification.modelId}" maxlength="30" placeholder="Model Id"/>
    </div>
 <div class="form-group" style="
    align-content:  center;
    margin-left: 240px;
    width: 50%;">
    <form:label path="modelName">Model Name</form:label>
    <form:input type="text" class="form-control" id="modelName" path="modelName" value="${specification.modelName}" maxlength="30" placeholder="Model Name"/>
  </div>

   <div class="form-group" style="
    align-content:  center;
    margin-left: 240px;
    width: 50%;">
    <form:label path="brandName">Brand Name</form:label>
      <form:input type="text" class="form-control" id="brandName" path="brandName" value="${specification.brandName}" maxlength="30" placeholder="Brand Name"/>
    </div>
 <div class="form-group" style="
    align-content:  center;
    margin-left: 240px;
    width: 50%;">
    <form:label path="type">Type</form:label>
<form:input type="text" class="form-control" id="type" path="type" value="${specification.type}" maxlength="30" placeholder="Type"/>
  </div>

   <div class="form-group" style="
    align-content:  center;
    margin-left: 240px;
    width: 50%;">
    <form:label path="size">Size</form:label>
       <form:input type="number" class="form-control" id="size" path="size" value="${specification.size}" max="1000" placeholder="Size"/>
    </div>
 <div class="form-group" style="
    align-content:  center;
    margin-left: 240px;
    width: 50%;">
    <form:label path="colour">Colour</form:label>
  <form:input type="text" class="form-control" id="colour"  path="colour" value="${specification.colour}" maxlength="30" placeholder="Colour"/>
  </div>


  <div class="form-group" style="
    align-content:  center;
    margin-left: 240px;
    width: 50%;">
    <form:label class="col-form-label" path="warranty">Warranty</form:label>
    <form:input type="text" class="form-control" id="warranty"  path="warranty" value="${specification.warranty}" maxlength="30" placeholder="Warranty" required="true"/>
  </div><br>
<button type="submit" class="btn btn-primary">Submit</button>
 <input type="hidden" name="action" value="update"/>
<input type="hidden" name="sellerId" value="${sessionScope.seller_id}"/>
 <input type="hidden" name="id" value="${specification.id}"/>
</form:form>
</body>
