<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
   <head>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
      <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/employee.css">
      <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/display.css">
      <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/js/display.js">
   </head>
   <%@ include file = "headeradmin.jsp" %>
   <body>
      <b>
         <h1>Users</h1>
      </b>
      <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Enter the name">
      <table id="myTable">
         <tr>
            <th>Id</th>
            <th>Name</th>
            <th>UserName</th>
            <th>EmailId</th>
            <th>PhoneNumber</th>
            <th>Role</th>
            <th>Active</th>
            <th>Action</th>
         </tr>
         <c:forEach items="${users}" var="user">
            <tr>
               <td>${user.id}</td>
               <td>${user.name}</td>
               <td>${user.userName}</td>
               <td>${user.emailId}</td>
               <td>${user.phoneNumber}</td>
               <td>${user.role}</td>
               <c:choose>
                  <c:when test="${user.isActive == Boolean.TRUE}">
                     <td>Yes</td>
                  </c:when>
                  <c:otherwise>
                     <td>No</td>
                  </c:otherwise>
               </c:choose>
               <td><a href="User?action=delete&id=${user.id}"><i class="material-icons"
                  onclick= "return confirm('Are you sure you want to delete?')">delete</a></i></td>
            </tr>
         </c:forEach>
      </table>
   </body>
</html>
