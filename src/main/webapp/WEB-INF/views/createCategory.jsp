<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<head>
<%@ include file="headeradmin.jsp"%>


</head>
<body>
  <form:form method="post" action="Category" modelAttribute="category">
    <h1>Create Category</h1>
    <br>
    <br>
    <div class="form-group"
      style="align-content: center; margin-left: 240px; width: 50%;">
      <form:label class="col-form-label" path="name">Name</form:label>
      <form:input type="text" class="form-control" id="name" path="name"
        value="${category.name}" maxlength="100"
        placeholder="Category Name" required="true" />
    </div>
    <div class="form-group"
      style="align-content: center; margin-left: 240px; width: 50%;">
      <form:label path="description">Description</form:label>
      <form:textarea class="form-control" id="description"
        path="description" value="${category.description}"
        maxlength="286" rows="3" required="true"></form:textarea>
    </div>
    <div>
      <span style="color: red"> <%=(request.getAttribute("message") == null) ? "" : request.getAttribute("message")%>
    </div>
    </div>
    <button type="submit" class="btn btn-primary"
      style="margin-left: 815px;">Submit</button>
    <input type="hidden" name="action" value="create" />
    <input type="hidden" name="isNeededAll" value="true" />
  </form:form>
</body>
