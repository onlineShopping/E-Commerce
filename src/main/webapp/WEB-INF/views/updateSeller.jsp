<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
<%@ include file = "headerSeller.jsp" %>
</head>
<body>
<h1>Profile</h1>
<br>
<br>
<form method="post" action="Seller">
   <div>
    <div class="form-group" style="
    align-content:  center;
    margin-left: 240px;
    width: 50%;">
      <label path="name">FullName</label>
      <input type="text" class="form-control" id="name" name="name" placeholder="FullName"
      value="${user.name}"/>
    </div>
</div>
  <div >
    <div class="form-group" style="
    align-content:  center;
    margin-left: 240px;
    width: 50%;">
      <label path="emailId">Email</label>
      <input type="email" class="form-control" id="emailId" name="email_id" placeholder="Email"
      value="${user.emailId}"/>
    </div>
</div>
    <div>
    <div class="form-group" style="
    align-content:  center;
    margin-left: 240px;
    width: 50%;">
      <label path="phoneNumber">PhoneNumber</label>
      <input type="txt" class="form-control" id="phoneNumber" name="phoneNumber" placeholder="PhoneNumber"
      value="${user.phoneNumber}"/>
<button align="right" type="submit" class="btn btn-primary" style="overflow:right;">Submit</button>
    </div>
   </div>
  
<input type="hidden" name="action" value="update"/>
<input type="hidden" name="id" value="${sessionScope.userId}"/>
</form>
</body>
</html>