<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<head>
<%@ include file="headeradmin.jsp"%>

</head>
<body>
  <form:form method="post" action="SubCategory"
    modelAttribute="subCategory">
    <h1>Create SubCategory</h1>
    <br>
    <br>
    <div class="form-group"
      style="align-content: center; margin-left: 240px; width: 50%;">
      <form:label class="col-form-label" path="name">Name</form:label>
      <form:input type="text" class="form-control" id="name" path="name"
        maxlength="100" placeholder="SubCategory Name" required="true" />
    </div>
    <div class="form-group"
      style="align-content: center; margin-left: 240px; width: 50%;">
      <form:label path="description">Description</form:label>
      <form:textarea class="form-control" id="description"
        path="description" maxlength="286" rows="3" required="true"></form:textarea>
    </div>
    </div>

    <div style="align-content: center; margin-left: 240px; width: 50%;">
      <label for="categoryId">Select Category</label> <select
        class="form-control form-control-lg" id="categoryId"
        name="categoryId">
        <c:forEach items="${categories}" var="category">
          <option value="${category.id}">${category.name}
          <option>
        </c:forEach>
      </select>
    </div>
    <div>
      <span style="color: red"> <%=(request.getAttribute("message") == null) ? "" : request.getAttribute("message")%>
    </div>
    <button type="submit" class="btn btn-primary"
      style="margin-left: 815px;">Submit</button>
    <input type="hidden" name="action" value="create" />
    <input type="hidden" name="isNeededAll" value="true" />
  </form:form>
</body>
