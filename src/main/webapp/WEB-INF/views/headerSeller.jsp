<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OnlineShop</title>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/prettyPhoto.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/price-range.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/animate.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/main.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/responsive.css" rel="stylesheet">      
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
    
    
  </head>
  <!--/head-->
  <style>
    .btn-info {
    color: #000000 ;
    background-color:#D3D3D3 !important;
    border-color: #D3D3D3 !important;
    }
    .navbar-collapse {
    padding-right: 0px !important;
    }
    .navbar-collapse.collapse {
    padding-left: 102px;
    margin-left: 125px;
    }
    img {
    margin-bottom: -60px;
    }
    section 
    {
    background:#fff;
    width:600px;
    height:300px;
    margin:100px auto;
    position:relative
    }
    section> input 
    {
    margin:10px 25px;
    width:200px;
    height:20px;
    padding:5px;
    border:none;
    border-bottom:1px solid black;
    outline:0
    }
    input:first-of-type
    {
    /*   position:relative; */
    /*   top:30%; */
    margin-top: 1px;
    }
    
    .center-block {
    max-width: 768px;
    padding: 0 20px;
}
homepage17v7.css:1
.center-block {
    display: block;
    margin-right: auto;
    margin-left: auto;
}
  </style>
  <body>
    <!--/header_top-->
    <!-- Just an image -->

    
      <!--/header_top-->
      <div class="header-top">
        <!--header-middle-->
        <div class="container">
          <div class="row">
            <div class="col-sm-4">
              <div class="logo pull-left">
                <a href="#"><img src="${pageContext.request.contextPath}/resources/images/logo.png" alt="" /></a>
              </div>
            </div>
            <div class="mainmenu pull-left" style="padding-top:16px;overflow:right;">
                <ul class="nav navbar-nav collapse navbar-collapse">
                  <li class="dropdown">
                    <a href="Seller?action=redirect&name=product&id=${sessionScope.seller_id}">Products</a>
                    
                  </li>
                  <li class="dropdown">
                    <a href="Seller?action=redirect&name=update&id=${sessionScope.seller_id}">Update Profile</a>
                    
                  </li>
                  <li class="dropdown" style="overflow:right;">
                  <a href="Logout">Logout</a>
                  </li>
                </ul>
              </div>
          </div>
        </div>
      </div>
      
      <!--/header-bottom-->
    </header>
    <!--/header-->
    
    
    
    
    
    
    
    
    <!--/Footer-->
</html>
