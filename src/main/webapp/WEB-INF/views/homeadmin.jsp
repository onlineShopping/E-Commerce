<html>
<%@ include file = "headeradmin.jsp" %>

    <div class="homepagecontent">
		<div class="logopndesc">
			
			<div>
				<h1 align="center">OnlineShop</h1>
			</div>
			<div class="center-block"><section class="page-bottom"><div class="region region-content-footer"><section id="block-block-5" class="block block-block clearfix"><p>OnlineShop marketplace is World’s leading platform for selling online. Be it a manufacturer, vendor or supplier, simply sell your products online on OnlineShop and become a top ecommerce player with minimum investment. Through a team of experts offering exclusive seller workshops, training, seller support and convenient seller portal, OnlineShop focuses on educating and empowering sellers across India.<br><br>Selling on OnlineShop.com is easy and absolutely free. All you need is to register, list your catalogue and start selling your products.<br><br>What's more! We have third party ‘Ecommerce Service Providers’ who provide logistics, cataloging support, product photo shoot and packaging materials. We have Seller Protection Program to safeguard sellers from losses via compensation from Seller Protection Fund (SPF). We provide OnlineShop Fulfilment services through which you can ensure faster delivery of your items, quality check by our experts and delightful packaging. Combine these with fastest payments in the industry and an excellent seller portal. No wonder OnlineShop is World’s favourite place to sell online.</p></section><!-- /.block--></div></section></div>
		</div>
		
	</div>
    <br>
    <br>
    <footer id="footer">
      <!--Footer-->  
      <div class="footer-bottom" style="
position: fixed;
right: 0;
bottom: 0;
left: 0;
padding: 1rem;
text-align: left;
color:white;
vertical-align: baseline !important;
">
        <div class="container">
          <div class="row">
            <p class="pull-left" style="align:center;">©OnlineShop.com</p>
            <p class="pull-right">
              <a href="#"><i class="fa fa-facebook" style="color:darkblue;"></i></a>
              <a href="#"><i class="fa fa-twitter" style="color:lightblue;"></i></a>
              <a href="#"><i class="fa fa-google-plus" style="color:red;"></i></a>
            </p>
          </div>
        </div>
      </div>
    </footer>
</html>
