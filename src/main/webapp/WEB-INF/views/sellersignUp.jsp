<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

<style>
.btn-primary {
    float: right !important;
}
</style>
</head>
<body>
<form:form method="post" action="Seller" modelAttribute="user">
   <div class="form-row">
    <div class="form-group col-md-6">
      <form:label path="name">FullName</form:label>
      <form:input type="text" class="form-control" id="name" path="name" placeholder="FullName"/>
    </div>
    <div class="form-group col-md-6">
      <form:label path="userName">UserName</form:label>
      <form:input type="text" class="form-control" id="userName" path="userName" placeholder="UserName"/>
    </div>
   </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <form:label path="emailId">Email</form:label>
      <form:input type="email" class="form-control" id="emailId" path="emailId" placeholder="Email"/>
    </div>
    <div class="form-group col-md-6">
      <form:label path="password">Password</form:label>
      <form:input type="password" class="form-control" id="password" path="password" placeholder="Password" />
    </div>
  </div>
    <div class="form-row">
    <div class="form-group col-md-6">
      <form:label path="phoneNumber">PhoneNumber</form:label>
      <form:input type="number" class="form-control" id="phoneNumber" path="phoneNumber" placeholder="PhoneNumber"/>
    </div>
   </div>
  <button align="right" type="submit" class="btn btn-primary">Sign in</button>
<input type="hidden" name="action" value="create"/>
<input type="hidden" name="role" value="seller"/>
</form:form>
</body>
