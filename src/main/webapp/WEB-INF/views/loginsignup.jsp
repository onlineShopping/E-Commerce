<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OnlineShop</title>
    <link href="resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="resources/css/font-awesome.min.css" rel="stylesheet">
    <link href="resources/css/prettyPhoto.css" rel="stylesheet">
    <link href="resources/css/price-range.css" rel="stylesheet">
    <link href="resources/css/animate.css" rel="stylesheet">
    <link href="resources/css/main.css" rel="stylesheet">
    <link href="resources/css/responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">   
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>
  
  <style>
    .btn-info {
    color: #000000 ;
    background-color:#D3D3D3 !important;
    border-color: #D3D3D3 !important;
    }
    .navbar-collapse {
    padding-right: 0px !important;
    }
    .navbar-collapse.collapse {
    padding-left: 102px;
    margin-left: 125px;
    }
    img {
    margin-bottom: -60px;
    }
    section 
    {
    background:#fff;
    width:600px;
    height:300px;
    margin:100px auto;
    position:relative
    }
    section> input 
    {
    margin:10px 25px;
    width:200px;
    height:20px;
    padding:5px;
    border:none;
    border-bottom:1px solid black;
    outline:0
    }
    input:first-of-type
    {
    /*   position:relative; */
    /*   top:30%; */
    margin-top: 1px;
    }
    .carousel-control.left span, .carousel-control.right span {
    background-color: #f !important;    
    }
    .carousel-indicators .active {
    height: 4px !important;
    background-color:transparent !important;
    }
    .carousel-control {
     width:3% !important;
     }
     .header-bottom {
    padding-bottom: 0px !important;
    padding-top: 0px !important;
}

  </style>
  <body>
    <!--/header_top-->
    <header id="header">
      <!--header-->
      
      <!--/header_top-->
      <div class="header-top" color="orange">
        <!--header-middle-->
        <div class="container">
          <div class="row">
              <div class="logo pull-left" >
                <a href="#"><img src="resources/images/logo.png" alt="" style="    margin-top: 11px;
    width: 200px;" /></a>
              <p class="acctparagraph">
				<button type="button" class="btn btn-default" data-toggle="modal" data-target="#loginmodal"     style="margin-left: 1024px;
    margin-top: 24px;"><span class = "accountbuttons">Log In</span></button>
				<button type="button" class="btn btn-default" data-toggle="modal" data-target="#signupmodal"
				style="margin-top: 24px;"><span class = "accountbuttons">Sign Up</span></button>
			</p>
          </div>
        </div>
      </div>

	<c:choose>
	
	<c:when test="${not empty requestScope.message}">
            <div class="alert alert-success" style="display:block;">
                <span class="closebtn"onclick="this.parentElement.style.display='none';" > &times;</span>
                <strong></strong>${requestScope.message}
            </div>
        </c:when>
        <c:otherwise>
        </c:otherwise>
        </c:choose>
<div>
    <a href="#"><img src="${pageContext.request.contextPath}/resources/images/homenew.png" alt="" style="padding-left: 280px;
    padding-top: 58px;
"/></a>
    </div>
	
	
	
	<footer>
      <!--Footer-->  
      <div class="footer-bottom" style="
position: fixed;
right: 0;
bottom: 0;
left: 0;
padding: 1rem;
text-align: left;
color:black;
    
vertical-align: baseline !important;
display:block;
">
        <div>
          <div class="row" style="background-color: lightgrey;     height: 40px;">
            <h4 style="text-align:left;"> &copy; OnlineShop.com</h4>
          </div>
        </div>
      </div>
    </footer>
  
	<!-- Sign up Modal -->
	<div id="signupmodal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- Sign up Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Sign Up</h4>
				</div>
				<div class="modal-body" id="signupmodal" style="height:75%;">
				<form name="form" action="Seller" method="post" onsubmit="return validate()">
					<p>Name:</p>
					<input type="text" class="form-control modalinputs" id="firstname" name="name" placeholder="Enter your first name" maxlength="40" required/>
					<p>User name:</p>
					<input type="text" class="form-control modalinputs" id="lastname" name="userName" placeholder="Enter your last name" maxlength="40" required/>
					<p>PhoneNumber:</p>
					<input type="text" class="form-control modalinputs" id="signup-email" name="phoneNumber" placeholder="Enter your phonenumber" pattern="[0-9]{10}" title="Please type the correct input" maxlength="10" required/>
					<p>Email:</p>
					<input type="email" class="form-control modalinputs" id="signup-email" name="emailId" placeholder="Enter your email"  maxlength="40" required/>
					<p>Password:</p>
					<input type="password" class="form-control modalinputs" id="txtPassword" name="password" placeholder="Enter your password" maxlength="40" required/>
					<p>Confirm Password:</p>
					<input type="password" class="form-control modalinputs" id="txtConfirmPassword" placeholder="Re-enter your password" maxlength="40" required/>
					<button type="submit" id="btnSubmit" style="float:right;margin-top: 16px;" class="btn btn-default">Sign Up</button>
                  <input type="hidden" name="role" value="seller"/>
                  <input type="hidden" name="action" value="create"/>
					</form>
				</div>
				
			</div>
		</div>
	</div>
  <script type="text/javascript">
    $(function () {
        $("#btnSubmit").click(function () {
            var password = $("#txtPassword").val();
            var confirmPassword = $("#txtConfirmPassword").val();
            if (password != confirmPassword) {
                alert("Passwords do not match.");
                return false;
            }
            return true;
        });
    });
</script>
	<!-- Log In Modal -->
	<div id="loginmodal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- Log In Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Log In</h4>
				</div>
				<div class="modal-body" id="modal2">
				<form name="form" action="User" method="post" onsubmit="return validate()">
					<p>Username:</p>
					<input type="text" class="form-control modalinputs" id="login-email" name="userName" placeholder="Enter your username" maxlength="40" required>
					<p>Password:</p>
					<input type="password" class="form-control modalinputs" id="login-password" name="password" placeholder="Enter your password" maxlength="40" required>
				
					<button type="submit" class="btn btn-default" style="margin-left: 502px;
    margin-top: 20px;">Log In</button>
                  <input type="hidden" name="action" value="login"/>
					</form>
				</div>
			</div>
		</div>
	</div>
  </body>
</html>
