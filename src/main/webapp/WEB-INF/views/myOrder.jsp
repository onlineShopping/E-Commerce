<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Cart | E-Shopper</title>
<link href="resources/css/bootstrap.min.css" rel="stylesheet">
<link href="resources/css/font-awesome.min.css" rel="stylesheet">
<link href="resources/css/prettyPhoto.css" rel="stylesheet">
<link href="resources/css/price-range.css" rel="stylesheet">
<link href="resources/css/animate.css" rel="stylesheet">
<link href="resources/css/main.css" rel="stylesheet">
<link href="resources/css/responsive.css" rel="stylesheet">


</head>
<!--/head-->
<%@ include file="headercustomer.jsp"%>
<body>

  <div id="cart_items">
    <div class="container">
      <div class="breadcrumbs">
        <ol class="breadcrumb">
          <li><a href="#">Home</a></li>
          <li class="active">my Order</li>
        </ol>
      </div>
      <div class="table-responsive cart_info">
        <table class="table table-condensed">
          <thead>
            <tr class="cart_menu">
              <td class="image">Item</td>
              <td class="description"></td>
              <td class="price">Price</td>
              <td class="quantity">Quantity</td>
              <td class="total">Total</td>
              <td></td>
            </tr>
          </thead>

          <!-- main table -->
          <tbody>
            <c:forEach items="${orders}" var="order">
              <c:forEach items="${order.orderDetails}" var="orderDetail">
                <tr>
                  <td class="cart_product"><a href=""><img
                      src="${pageContext.request.contextPath}/resources/images/${orderDetail.product.id}.jpeg"
                      alt="" width="11di0" height="110"></a></td>
                  <td class="cart_description">
                    <h4>${orderDetail.product.name}</h4>
                    <p>Product ID:${orderDetail.product.id}</p>
                    <p>Delivery status:${orderDetail.status}</p>
                    <p>Ordered date::${orderDetail.orderTime}</p>
                  </td>
                  <td class="cart_price">
                    <p>${orderDetail.product.price}</p>
                  </td>
                  <td class="cart_quantity">
                    <div class="cart_quantity_button">
                      <p>${orderDetail.quantity}</p>
                    </div>
                  </td>
                  <td class="cart_total">
                    <p class="cart_total_price">Rs
                      ${orderDetail.quantity * orderDetail.product.price}</p>
                  </td>
                </tr>
              </c:forEach>
            </c:forEach>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <!-- table entry end -->

  <script src="resources/js/jquery.js"></script>
  <script src="resources/js/bootstrap.min.js"></script>
  <script src="resources/js/jquery.scrollUp.min.js"></script>
  <script src="resources/js/jquery.prettyPhoto.js"></script>
  <script src="resources/js/main.js"></script>
</body>
</html>
