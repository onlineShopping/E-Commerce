<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<head>
<%@ include file = "headeradmin.jsp" %>
</head>
<h1>Update SubCategory</h1>
<body>
<form:form method="post" action="SubCategory" modelAttribute="subCategory">
  <div class="form-group" style="
    align-content:  center;
    margin-left: 240px;
    width: 50%;">
    <form:label class="col-form-label" path="name">Name</form:label>
    <form:input type="text" class="form-control" id="name" path="name" value="${subCategory.name}" maxlength="100" placeholder="Product Name" required="true"/>
  </div>
 <div class="form-group" style="
    align-content:  center;
    margin-left: 240px;
    width: 50%;">
    <form:label path="description">Description</form:label>
    <form:textarea class="form-control" id="description" path="description" value="${subCategory.description}" maxlength="286" rows="3" required="true"></form:textarea>
  </div>
</div>
<button type="submit" class="btn btn-primary" style="margin-left: 815px;">Submit</button>
 <input type="hidden" name="isNeededActive" value="false"/>
 <input type="hidden" name="isNeededAll" value="true"/>
 <input type="hidden" name="action" value="update"/>
 <input type="hidden" name="id" value="${subCategory.id}"/>
</form:form>
</body>
