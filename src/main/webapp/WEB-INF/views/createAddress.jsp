<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
    <head>
<%@ include file = "headeradmin.jsp" %>

</head>
    <body>
  
    <!--%@ include file="/WEB-INF/jsp/header.jsp" %-->
        <h1>Add Address</h1>
       <form:form method="POST" action="Address" modelAttribute="address">
           
                     <div class="form-group"  style="
    align-content:  center;
    margin-left: 240px;
    width: 50%;">
                         <form:label class="col-form-label" path="doorNumber">Door Number</form:label>
                         <form:input class="form-control" type="text" id="doorNumber" maxlength="30" path="doorNumber" placeholder="DoorNumber" required="true"/>
                   </div>
                     <div class="form-group"  style="
    align-content:  center;
    margin-left: 240px;
    width: 50%;">
                        <form:label class="col-form-label" path="street">Street</form:label>
                        <form:input  class="form-control" type="text" id="street" path="street" maxlength="30" placeholder="Street" required="true"/>
                    </div>
                     <div class="form-group"  style="
    align-content:  center;
    margin-left: 240px;
    width: 50%;">
                        <form:label class="col-form-label" path="area">Area</form:label>
                        <form:input  class="form-control" type="text" id="area" path="area" maxlength="30" placeholder="Area" required="true"/>
                    </div>
                     <div class="form-group"  style="
    align-content:  center;
    margin-left: 240px;
    width: 50%;">
                        <form:label class="col-form-label" path="city">City</form:label>
                        <form:input  class="form-control" type="text" id="city" path="city" maxlength="30" placeholder="City" pattern="[a-zA-Z]+\\.?" title="Enter alpha numeric characters" required="true"/>
                    </div>
                    <div class="form-group"  style="
    align-content:  center;
    margin-left: 240px;
    width: 50%;">
                        <form:label class="col-form-label" path="state">State</form:label>
                        <form:input   class="form-control" type="text" id="state" path="state" maxlength="30" placeholder="State" pattern="[a-zA-Z]+\\.?" title="Enter alpha numeric characters" required="true"/>
                    </div>
                     <div class="form-group"  style="
    align-content:  center;
    margin-left: 240px;
    width: 50%;">
                        <form:label class="col-form-label" path="pincode">Pincode</form:label>
                        <form:input  class="form-control" type="text" id="pincode" path="pincode" maxlength="6" placeholder="Pincode" pattern="\\d{6}" title="Enter the valid pincode" required="true"/>
                    </div>
                </fieldset>
                <input type="submit" />
                <input type="hidden" name="person" value="${person}"/>
                 <input type="hidden" name=personId value="${personId}"/>
        </form:form>
        
    </body>
    </div>
</html>
