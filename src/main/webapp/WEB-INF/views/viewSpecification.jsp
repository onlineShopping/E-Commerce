<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
   <head>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
      <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="resources/css/employee.css">
      <link rel="stylesheet" type="text/css" href="resources/css/display.css">
   </head>
  <c:choose>
                 <c:when test="${sessionScope.userName == 'admin'}">
                    <%@ include file = "headeradmin.jsp" %>
                 </c:when>
                 <c:otherwise>
                    <%@ include file = "headerSeller.jsp" %>
                 </c:otherwise>
              </c:choose>
   <body>
      <b>
         <h1>Specification</h1>
      </b>
      <table id="myTable">
         <tr>
            <th>Model Id</th>
            <th>Model Name</th>
            <th>Brand Name</th>
            <th>Type</th>
            <th>Size</th>
            <th>Colour</th>
            <th>Warranty</th>
            <th colspan="3">Action</th>
         </tr>
            <tr>
               <td>${specification.modelId}</td>
               <td>${specification.modelName}</td>
               <td>${specification.brandName}</td>
               <td>${specification.type}</td>
               <td>${specification.size}</td>
               <td>${specification.colour}</td>
                <td>${specification.warranty}</td>
               <td>
                  <a href="Specification?action=get&id=${specification.id}&methodName=update"><i 
                     class="material-icons">edit</i></a>
            </tr>
      </table>
   </body>
</html>
