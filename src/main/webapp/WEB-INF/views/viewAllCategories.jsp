<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
   <head>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
      <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/employee.css">
      <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/display.css">
      <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/js/display.js">
   </head>
   <%@ include file = "headeradmin.jsp" %>
   <body>
      <b>
         <h1>Categories</h1>
      </b>
      <div>
      <form action="Category" method="get">
        <input type="hidden" name="action" value="redirect"/>
        
  <input type="hidden" name="formName" value="create"/>
      <button type="submit" class="btn btn-primary" style="margin-left: 1150px;">Add Category</button>          
      </form>
      </div>
      <br>
      
      <table id="myTable">
         <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Description</th>
            <th>Active</th>
            <th colspan="3">Action</th>
         </tr>
         <c:forEach items="${categories}" var="category">
            <tr>
               <td>${category.id}</td>
               <td>${category.name}</td>
               <td>${category.description}</td>
               <c:choose>
                  <c:when test="${user.isActive == Boolean.TRUE}">
                     <td>Yes</td>
                  </c:when>
                  <c:otherwise>
                     <td>No</td>
                  </c:otherwise>
               </c:choose>
               <td>
                  <a href="Category?action=get&id=${category.id}&methodName=update&isNeededActive=false"><i 
                    class="material-icons">edit</i></a>
               <c:choose>
                 <c:when test="${category.isActive == Boolean.TRUE}">
                     <td><a href="Category?action=inActivate&id=${category.id}"><i class="material-icons"
                 onclick= "return confirm('Are you sure you want to delete?')">delete</a></i></td>
                 </c:when>
                 <c:otherwise>
                    <td><a href="Category?action=activate&id=${category.id}"><i class="fa fa-toggle-on" aria-hidden="true"></i></td>
                 </c:otherwise>
              </c:choose>
            </tr>
         </c:forEach>
      </table>
   </body>
</html>
