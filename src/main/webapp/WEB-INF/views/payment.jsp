<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
<link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
   <link href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css" rel="stylesheet">
   <link href="${pageContext.request.contextPath}/resources/css/prettyPhoto.css" rel="stylesheet">
   <link href="${pageContext.request.contextPath}/resources/css/price-range.css" rel="stylesheet">
   <link href="${pageContext.request.contextPath}/resources/css/animate.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/css/main.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/css/responsive.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">   
<link href="${pageContext.request.contextPath}/resources/css/payment.css" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<%@ include file = "headercustomer.jsp" %>
</head>

  <div class="checkout">
    <div class="credit-card-box">
      <div class="flip">
        <div class="front">
          <div class="chip"></div>
          <div class="logo">
            <svg version="1.1" id="visa"
              xmlns="http://www.w3.org/2000/svg"
              xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
              width="47.834px" height="47.834px"
              viewBox="0 0 47.834 47.834"
              style="enable-background: new 0 0 47.834 47.834;">
          </svg>
          </div>
          <div class="number"></div>
          <div class="card-holder">
            <label>Card holder</label>
            <div></div>
          </div>
          <div class="card-expiration-date">
            <label>Expires</label>
            <div></div>
          </div>
        </div>
        <div class="back">
          <div class="strip"></div>
          <div class="logo">
            <svg version="1.1" id="visa"
              xmlns="http://www.w3.org/2000/svg"
              xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
              width="47.834px" height="47.834px"
              viewBox="0 0 47.834 47.834"
              style="enable-background: new 0 0 47.834 47.834;">
          </svg>

          </div>
          <div class="ccv">
            <label>CCV</label>
            <div></div>
          </div>
        </div>
      </div>
    </div>
    <form class="form" action="Payment" method="post" autocomplete="off">
      
        <input type="hidden" name="action" value="insertPayment"/>
        <input type="hidden" name="totalAmount" value="${totalAmount}"/>
        <input type="hidden" name="shipmentId" value="${shipmentId}"/>
            <input type="hidden" name="customerId" value="${sessionScope.customerId}"/>
            <input type="hidden" name="cartId" value="${sessionScope.cartId}"/>
      <fieldset>
        <label for="card-number">Card Number</label> <input type="text" name="cardNumber"
          id="card-number" pattern="[0-9]{16}" maxlength="16" required/>
      </fieldset>
      <fieldset>
        <label for="card-holder">Card holder</label> <input type="text"
          id="card-holder" required/>
      </fieldset>
      <fieldset class="fieldset-expiration">
        <label for="card-expiration-month">Expiration date</label>
        <div class="select">
          <select id="card-expiration-month" required>
            <option></option>
            <option>01</option>
            <option>02</option>
            <option>03</option>
            <option>04</option>
            <option>05</option>
            <option>06</option>
            <option>07</option>
            <option>08</option>
            <option>09</option>
            <option>10</option>
            <option>11</option>
            <option>12</option>
          </select>
        </div>
        <div class="select">
          <select id="card-expiration-year" required>
            <option></option>
            <option>2016</option>
            <option>2017</option>
            <option>2018</option>
            <option>2019</option>
            <option>2020</option>
            <option>2021</option>
            <option>2022</option>
            <option>2023</option>
            <option>2024</option>
            <option>2025</option>
          </select>
        </div>
      </fieldset>
      <fieldset class="fieldset-ccv">
        <label for="card-ccv">CCV</label> <input type="text"
          id="card-ccv" required maxlength="3" required/>
      </fieldset>
      <button class="btn">
        <i class="fa fa-lock"></i> Pay
      </button>
    </form>
  </div>
  <script src="${pageContext.request.contextPath}/resources/js/payment.js"></script>
<body>
</html>
