<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>OnlineShop</title>

<link
  href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"
  rel="stylesheet">
<link
  href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css"
  rel="stylesheet">
<link
  href="${pageContext.request.contextPath}/resources/css/prettyPhoto.css"
  rel="stylesheet">
<link
  href="${pageContext.request.contextPath}/resources/css/price-range.css"
  rel="stylesheet">
<link
  href="${pageContext.request.contextPath}/resources/css/animate.css"
  rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/css/main.css"
  rel="stylesheet">
<link
  href="${pageContext.request.contextPath}/resources/css/responsive.css"
  rel="stylesheet">
<link rel="stylesheet"
  href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet"
  href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
  src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
  src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<style>
.btn-info {
  color: #000000;
  background-color: #D3D3D3 !important;
  border-color: #D3D3D3 !important;
}

.navbar-collapse {
  padding-right: 0px !important;
}

.navbar-collapse.collapse {
  padding-left: 102px;
  margin-left: 125px;
}

img {
  margin-bottom: -60px;
}

section {
  background: #fff;
  width: 600px;
  height: 300px;
  margin: 100px auto;
  position: relative
}

section>input {
  margin: 10px 25px;
  width: 200px;
  height: 20px;
  padding: 5px;
  border: none;
  border-bottom: 1px solid black;
  outline: 0
}

input:first-of-type {
  /*   position:relative; */
  /*   top:30%; */
  margin-top: 1px;
}

.carousel-control.left span, .carousel-control.right span {
  background-color: #f !important;
}

.carousel-indicators .active {
  height: 4px !important;
  background-color: transparent !important;
}

.carousel-control {
  width: 3% !important;
}

.selector-for-some-widget {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}

.add-to-cart {
  margin-bottom: 0px !important;
}

.card {
  border: none !important;
  width: 50px;
  float: left;
  padding: 171px;
}

.card-footer {
  border: none !important;
}

.card-block {
  padding: 6.25rem;
}

.btn {
  padding: -0.5rem 1rem !important;
  font-size: 2rem !important;
}

img {
  border-radius: 4px;
  border: 1px solid;
  padding: 5px;
  width: 150px;
  border: white;
}

.h4, h4 {
  font-size: 18px;
  padding: 7px;
  padding-left: 1px;
}

.card {
  position: relative;
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
  -webkit-flex-direction: column;
  -ms-flex-direction: column;
  flex-direction: column;
  background-color: #fff;
  border: 1px solid rgba(0, 0, 0, .125);
  border-radius: .25rem;
}

.card-block {
  padding: 4.25rem !important;
}

_card.scss: 17
.card-block {
  -webkit-box-flex: 1;
  -webkit-flex: 1 1 auto;
  -ms-flex: 1 1 auto;
  flex: 1 1 auto;
  padding: 1.25rem;
}
</style>
<body>
  <c:choose>
    <c:when test="${sessionScope.userName == null}">
      <header id="header">
        <!--header-->
        <header id="header">
          <!--header-->
          <div class="header_top">
            <!--header_top-->
            <div class="container">
              <div class="row">
                <div class="col-sm-6">
                  <div class="contactinfo"></div>
                </div>
                <div class="col-sm-6">
                  <div class="social-icons pull-right">
                    <ul class="nav navbar-nav">
                      <li><a
                        href="User?action=redirect&name=create"
                        style="padding-top: 8px; margin: 0 1em;">Sell
                          on OnlineShop</a></li>

                      <li><a href="#" data-toggle="modal"
                        data-target="#signupmodal"><i
                          class="fa fa-sign-in" aria-hidden="true"></i>Sign
                          up</a></li>
                      <li><a href="#" data-toggle="modal"
                        data-target="#loginmodal"><i
                          class="fa fa-lock"></i> Login</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
    </c:when>
    <c:otherwise>
      <!--/header_top-->
      <header id="header">
        <!--header-->
        <div class="header_top">
          <!--header_top-->
          <div class="container">
            <div class="row">
              <div class="col-sm-6">
                <div class="contactinfo"></div>
              </div>
              <div class="col-sm-6">
                <div class="social-icons pull-right">
                  <ul class="nav navbar-nav">
                    <li><a href="User?action=redirect&name=create"
                      style="padding-top: 8px; margin: 0 1em;">Orders</a></li>
                    <li><a href="Logout"><i
                        class="fa fa-sign-out" aria-hidden="true">Logout</i></li>
                    <li><a href="#"
                      style="margin-top: 8px; margin-left: 5px;">Hi
                        ${sessionScope.userName} !</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--/header_top-->
    </c:otherwise>
  </c:choose>
  <div class="header-middle" color="orange">
    <!--header-middle-->
           <div class="container">
          <div class="row">
              <div class="logo pull-left">
                <a href="home.jsp"><img src="resources/images/logo.png" alt="" /></a>
              <div class="search_box pull-right">
              <form action="Product" method="get">
            <input type="hidden" name="action" value="search">
                <input type="text" name="name" placeholder="Search" required/>
              <button type="submit" class="btn btn-info btn-lg" style="
    margin-left: 175px;
    margin-top: -9px;
">
              Search</button>
              </form>
            
            <a href="Cart?action=displayCart&customerId=${sessionScope.customerId}"
            class="btn btn-info btn-lg" style="margin-left: 800px;margin-top: -62px;">
            <span class="glyphicon glyphicon-shopping-cart"></span> Cart
          </a>
        </div>
      </div>
    </div>
  </div>
  <div class="header-bottom">
    <!--header-bottom-->
        <div class="container">
          <div class="row">
            <div class="col-sm-9">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
              </div>
           <div class="mainmenu pull-left">
                <ul class="nav navbar-nav collapse navbar-collapse">
                  <li class="dropdown">
                    <a href="#">Electronics<i class="fa fa-angle-down"></i></a>
                    <ul role="menu" class="sub-menu">
                      <li><a href="SubCategory?action=redirect&formName=display&name=Mobiles">Mobiles</a></li>
                      <li><a href="SubCategory?action=redirect&formName=display&name=Laptops">Laptops</a></li>
                      <li><a href="SubCategory?action=redirect&formName=display&name=Camera">Camera</a></li>
                      <li><a href="SubCategory?action=redirect&formName=display&name=Accessories">Accessories</a></li>
                    </ul>
                  </li>
                  <li class="dropdown">
                    <a href="#">Appliances<i class="fa fa-angle-down"></i></a>
                    <ul role="menu" class="sub-menu">
                      <li><a href="SubCategory?action=redirect&formName=display&name=Televisions">Televisions</a></li>
                      <li><a href="SubCategory?action=redirect&formName=display&name=Air Conditioners">Air Conditioners</a></li>
                      <li><a href="SubCategory?action=redirect&formName=display&name=Washing Machines">Washing Machines</a></li>
                    </ul>
                  </li>
                  <li class="dropdown">
                    <a href="#">Men<i class="fa fa-angle-down"></i></a>
                    <ul role="menu" class="sub-menu">
                      <li><a href="SubCategory?action=redirect&formName=display&name=Footwear">Footwear</a></li>
                      <li><a href="SubCategory?action=redirect&formName=display&name=Watches">Watches</a></li>
                    </ul>
                  </li>
                  <li class="dropdown">
                    <a href="#">Women<i class="fa fa-angle-down"></i></a>
                    <ul role="menu" class="sub-menu">
                      <li><a href="SubCategory?action=redirect&formName=display&name=KurtasAndKurtis">KurtasAndKurtis</a></li>
                    </ul>
                  </li>
                  <li class="dropdown">
                    <a href="#">Baby & Kids<i class="fa fa-angle-down"></i></a>
                    <ul role="menu" class="sub-menu">
                      <li><a href="SubCategory?action=redirect&formName=display&name=Toys">Toys</a></li>
                      <li><a href="SubCategory?action=redirect&formName=display&name=Baby Boy Clothing">Baby Boy Clothing</a></li>
                      <li><a href="SubCategory?action=redirect&formName=display&name=Baby Girl Clothing">Baby Girl Clothing</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
  <!--/header-bottom-->
  </header>
  <!--/header-->
  <script src="resources/js/jquery.js"></script>
  <script src="resources/js/bootstrap.min.js"></script>
  <script src="resources/js/jquery.scrollUp.min.js"></script>
  <script src="resources/js/price-range.js"></script>
  <script src="resources/js/jquery.prettyPhoto.js"></script>
  <script src="resources/js/main.js"></script>
<body>

  <div class="card-deck" align="center"
    style="border: 1px solid rgba(0, 0, 0, .125); size: 50%">
    <c:forEach items="${products}" var="product">
      <c:choose>
       <c:when test="${product.isActive == true}">
      <div class="card" style="padding-top: 19px">
        <a
          href="Product?methodName=view&id=${product.id}&action=get&isNeededActive=false"><img
          class="card-img-top"
          src="${pageContext.request.contextPath}/resources/images/${product.id}.jpeg"
          alt="Card image cap" width="100" height="200"></a>
        <div class="card-block">
          <h4 class="card-title">${product.name}</h4>
          <p class="card-text">${product.price}</p>
        </div>
        <div class="card-footer">
          <c:choose>
            <c:when test="${sessionScope.userName == null}">
       <a href="#" data-toggle="modal" data-target="#loginmodal"
                class="btn btn-info btn-lg">
                <button class="btn btn-default add-to-cart"
                  data-toggle="modal" data-target="#loginmodal"
                  type="submit">
                  <i class="fa fa-shopping-cart"></i>Add to cart
                </button>
              </a>
            </c:when>
            <c:otherwise>
              <form action="Cart" method="post">
                <input type="hidden" name="action" value="insertCart" />
                <input type="hidden" name="productId"
                  value="${product.id}" /> <input type="hidden"
                  name="customerId" value="${sessionScope.customerId}" />
                <input type="hidden" name="cartId"
                  value="${sessionScope.cartId}" />
                <button class="btn btn-default add-to-cart"
                  type="submit">
                  <i class="fa fa-shopping-cart"></i>Add to cart
                </button>
              </form>
            </c:otherwise>
          </c:choose>

        </div>
      </div>
      </c:when>
      </c:choose>
    </c:forEach>
  </div>
  
   <div id="signupmodal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Sign up Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Sign Up</h4>
        </div>
        <div class="modal-body" id="signupmodal" style="height: 75%;">
          <form name="form" action="Customer" method="post"
            onsubmit="return validate()">
            <p>Name:</p>
            <input type="text" class="form-control modalinputs"
              id="firstname" name="name"
              placeholder="Enter your first name" maxlength="40"
              required />
            <p>User name:</p>
            <input type="text" class="form-control modalinputs"
              id="lastname" name="userName"
              placeholder="Enter your last name" maxlength="40" required />
            <p>PhoneNumber:</p>
            <input type="text" class="form-control modalinputs"
              id="signup-phoneNumber" name="phoneNumber"
              placeholder="Enter your phonenumber" pattern="[0-9]{10}"
              title="Please type the correct input" maxlength="10"
              required />
            <p>Email:</p>
            <input type="text" class="form-control modalinputs"
              id="signup-email" name="emailId"
              placeholder="Enter your email"
              pattern="^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
              maxlength="40" required />
            <p>Password:</p>
            <input type="password" class="form-control modalinputs"
              id="txtPassword" name="password"
              placeholder="Enter your password" maxlength="40" required />
            <p>Confirm Password:</p>
            <input type="password" class="form-control modalinputs"
              id="txtConfirmPassword"
              placeholder="Re-enter your password" maxlength="40"
              required />
            <button type="submit" id="btnSubmit"
              style="float: right; margin-top: 16px;"
              class="btn btn-default">Sign Up</button>
            <input type="hidden" name="role" value="customer" /> <input
              type="hidden" name="action" value="create" />
          </form>
        </div>

      </div>
    </div>
  </div>
  <script type="text/javascript">
      $(function() {
        $("#btnSubmit").click(function() {
          var password = $("#txtPassword").val();
          var confirmPassword = $("#txtConfirmPassword").val();
          if (password != confirmPassword) {
            alert("Passwords do not match.");
            return false;
          }
          return true;
        });
      });
    </script>
  <!-- Log In Modal -->
  <div id="loginmodal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Log In Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Log In</h4>
        </div>
        <div class="modal-body" id="modal2">
          <form name="form" action="User" method="post"
            onsubmit="return validate()">
            <p>Username:</p>
            <input type="text" class="form-control modalinputs"
              id="login-email" name="userName"
              placeholder="Enter your username" maxlength="40" required>
            <p>Password:</p>
            <input type="password" class="form-control modalinputs"
              id="login-password" name="password"
              placeholder="Enter your password" maxlength="40" required>
            <p>${requestScope.message}</p>

            <button type="submit" class="btn btn-default"
              style="margin-left: 502px; margin-top: 20px;">Log
              In</button>
            <input type="hidden" name="action" value="login" />
          </form>
        </div>
      </div>
    </div>
  </div>
<body>
