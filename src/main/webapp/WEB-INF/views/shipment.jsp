<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
<%@ include file = "headercustomer.jsp" %>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/prettyPhoto.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/price-range.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/animate.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/main.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/shipment.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/responsive.css" rel="stylesheet">
</head>
	
	<div id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Shipment</li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
                <form action="Shipment" method="post">
                    <input type="hidden" name="action" value="insertShipment"/>
                    <input type="hidden" name="price" value="${totalAmount}"/>
                    <input type="hidden" name="totalAmount" value="${totalAmount}"/>
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image"><div class="col-sm-11">Address</div><div class="col-sm-1"><p data-toggle="modal" data-target="#addressmodal" class="add-button">+</p></div></td>
							<td></td>
						</tr>
					</thead>
					<tbody>
                    <c:forEach items="${addresses}" var="address">
						<tr>
							<td>
                            <div class="col-sm-1">
                            <input type="radio" name="addressId" value="${address.id}">
                            </div>
                            <div class="col-sm-11">
								<p>${address.doorNumber}</p>
								<p>${address.street}</p>
								<p>${address.area}</p>
								<p>${address.city}</p>
                                <p>${address.pincode}</p>
                            </div>
							</td>
						</tr>
                     </c:forEach>
						</tbody>
				</table>
				<input class="btn btn-default check_in" type="submit" value="Next>" />
        </form>
			</div>
		</div>
	</div> 
    <div class="col-sm-9"></div>
    <div>
        
    </div>
    
<div id="addressmodal" class="modal fade" role="dialog">
<div class="modal-dialog">
<!-- Sign up Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Address</h4>
</div>
<div class="modal-body" id="addressmodal" style="height:80%;">
<form name="form" action="Address" method="post" onsubmit="return validate()">
<p>Door number:</p>
<input type="text" class="form-control modalinputs" id="firstname" name="doorNumber" maxlength="30" placeholder="Enter your door number"/>
<p>Street:</p>
<input type="text" class="form-control modalinputs" id="firstname" name="street" maxlength="30" placeholder="Enter your street "/>
<p>Area:</p>
<input type="text" class="form-control modalinputs" id="firstname" name="area" maxlength="30" placeholder="Enter your area "/>
<p>City:</p>
<input type="text" class="form-control modalinputs" id="firstname" name="city" maxlength="30" placeholder="Enter your city "/>
<p>State:</p>
<input type="text" class="form-control modalinputs" id="firstname" name="state" maxlength="30" placeholder="Enter your state "/>
<p>Pincode:</p>
<input type="number" class="form-control modalinputs" id="firstname" name="pincode" maxlength="6" max="999999" placeholder="Enter your pincode"/>
<button type="submit" style="float:right" class="btn btn-default">Submit</button>
                 <input type="hidden" name="personId" value="${sessionScope.customerId}"/>
                 <input type="hidden" name="role" value="customer"/>
                 <input type="hidden" name="action" value="create"/>
</form>
</div>

</div>
</div>
</div>
    <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/jquery.scrollUp.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/jquery.prettyPhoto.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/main.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/shipment.js"></script>
</body>
</html>
