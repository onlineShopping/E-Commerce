<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<head>
<c:choose>
  <c:when test="${sessionScope.userName == 'admin'}">
    <%@ include file="headeradmin.jsp"%>
  </c:when>
  <c:otherwise>
    <%@ include file="headerSeller.jsp"%>
  </c:otherwise>
</c:choose>


</head>
<style>
element.style {
  padding-top: 0px;
}
</style>
<h1>Create Product</h1>
<body>
  <form:form method="post" action="Product" modelAttribute="product">
    <h3 style="margin-left: 120px;">Product</h3>
    <div class="form-group"
      style="align-content: center; margin-left: 240px; width: 50%;">
      <form:label class="col-form-label" path="name">Name</form:label>
      <form:input type="text" class="form-control" path="name" id="name"
        maxlength="100" placeholder="Product Name" required="true" />
    </div>
    <div class="form-group"
      style="align-content: center; margin-left: 240px; width: 50%;">
      <form:label path="description">Description</form:label>
      <form:textarea class="form-control" id="description"
        path="description" maxlength="286" rows="3" required="true"></form:textarea>
    </div>
    <div class="form-group"
      style="align-content: center; margin-left: 240px; width: 50%;">
      <form:label path="price">Price</form:label>
      <form:input type="double" class="form-control" path="price"
        id="price" placeholder="Product Price" required="true" />
    </div>
    <h3 style="margin-left: 120px;">Specification</h3>

    <div class="form-group"
      style="align-content: center; margin-left: 240px; width: 50%;">
      <form:label path="specification.modelId">Model Id</form:label>
      <form:input type="text" class="form-control"
        path="specification.modelId" id="specification.modelId"
        maxlength="30" placeholder="Model Id" />
    </div>
    <div class="form-group"
      style="align-content: center; margin-left: 240px; width: 50%;">
      <form:label path="specification.modelName">Model Name</form:label>
      <form:input type="text" class="form-control"
        id="specification.modelName" path="specification.modelName"
        maxlength="30" placeholder="Model Name" />
    </div>


    <div class="form-group"
      style="align-content: center; margin-left: 240px; width: 50%;">
      <form:label path="specification.brandName">Brand Name</form:label>
      <form:input type="text" class="form-control"
        id="specification.brandName" path="specification.brandName"
        maxlength="30" placeholder="Brand Name" />
    </div>
    <div class="form-group"
      style="align-content: center; margin-left: 240px; width: 50%;">
      <form:label path="specification.type">Type</form:label>
      <form:input type="text" class="form-control"
        id="specification.type" path="specification.type" maxlength="30"
        placeholder="Type" />
    </div>


    <div class="form-group"
      style="align-content: center; margin-left: 240px; width: 50%;">
      <form:label path="specification.size">Size</form:label>
      <form:input type="number" class="form-control"
        id="specification.size" path="specification.size" max="1000"
        placeholder="Size" />
    </div>
    <div class="form-group"
      style="align-content: center; margin-left: 240px; width: 50%;">
      <form:label path="specification.colour">Colour</form:label>
      <form:input type="text" class="form-control"
        id="specification.colour" path="specification.colour"
        maxlength="30" placeholder="Colour" />
    </div>


    <div class="form-group"
      style="align-content: center; margin-left: 240px; width: 50%;">
      <form:label class="col-form-label" path="specification.warranty">Warranty</form:label>
      <form:input type="text" class="form-control"
        id="specification.warranty" path="specification.warranty"
        maxlength="30" placeholder="Warranty" required="true" />
    </div>
    <div class="form-group"
      style="align-content: center; margin-left: 240px; width: 50%;">
      <label class="col-form-label" for="quantity">Quantity</label> <input
        type="number" class="form-control" id="quantity" name="quantity"
        max="1000" placeholder="quantity" required="true" />
    </div>
    <h3 style="margin-left: 120px;">Category</h3>
    <div class="form-group">
      <div
        style="align-content: center; margin-left: 240px; width: 50%;">


        <select class="form-control form-control-lg" id="categoryId"
          name="categoryId">
          <c:forEach items="${categories}" var="category">
            <option value="${category.id}">${category.name}
            <option>
          </c:forEach>
        </select>
      </div>
      <br>
      <h3 style="margin-left: 120px;">SubCategory</h3>
      <div
        style="align-content: center; margin-left: 240px; width: 50%;">

        <select class="form-control form-control-lg" id="subCategoryId"
          name="subCategoryId">
          <c:forEach items="${subCategories}" var="subCategory">
            <option value="${subCategory.id}">${subCategory.name}
            <option>
          </c:forEach>

        </select>
      </div>
    </div>
    <h3 style="margin-left: 120px;">Upload Image</h3>
    <div class="form-group"
      style="align-content: center; margin-left: 240px; width: 50%;">
      <form:input type="file" class="form-control-file" id="image"
        path="image" />
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>

    <input type="hidden" name="isNeededAll" value="true" />
    <input type="hidden" name="action" value="create" />
    <input type="hidden" name="seller_id" value="${id}" />
  </form:form>
</body>
