<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
   <head>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
      <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/employee.css">
      <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/display.css">
      <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/js/display.js">
   </head>
   <c:choose>
                 <c:when test="${sessionScope.userName == 'admin'}">
                    <%@ include file = "headeradmin.jsp" %>
                 </c:when>
                 <c:otherwise>
                    <%@ include file = "headerSeller.jsp" %>
                 </c:otherwise>
              </c:choose>
   
   <br>
  <body>
     <b>
        <h1>Products</h1>
     </b>
     <div>
     <form action="Product" method="get">
       <input type="hidden" name="action" value="redirect"/>
       <input type="hidden" name="id" value="${seller.id}"/>
<button type="submit" class="btn btn-primary" style="margin-left: 1156px; margin-top: -75px;">Add Product</button>       
     </form>
     </div>
     <br>
     
     <table id="myTable">
        <tr>
           <th>Id</th>
           <th>Name</th>
           <th>Description</th>
           <th>Price</th>
           <th>Category</th>
           <th>SubCategory</th>
           <th>Active</th>
           <th colspan="3">Action</th>
        </tr>
        <c:forEach items="${products}" var="product">
           <tr>
              <td>${product.id}</td>
              <td>${product.name}</td>
              <td>${product.description}</td>
              <td>${product.price}</td>
              <td>${product.category.name}</td>
              <td>${product.subCategory.name}</td>
              <c:choose>
                 <c:when test="${product.isActive == true}">
                    <td>Yes</td>
                 </c:when>
                 <c:otherwise>
                    <td>No</td>
                 </c:otherwise>
              </c:choose>
              <td><a href="Specification?action=get&id=${product.specification.id}&methodName=view">
                 <i class="fa fa-eye" aria-hidden="true"></i></a>
              <td>
                 <a href="Product?action=get&id=${product.id}&methodName=update&isNeededActive=true"><i 
                    class="material-icons">edit</i></a>         
              <c:choose>
                 <c:when test="${product.isActive == Boolean.TRUE}">
                      <td><a href="Product?action=inActivate&id=${product.id}&seller_id=${sessionScope.seller_id}"><i class="material-icons"
                 onclick= "return confirm('Are you sure you want to delete?')">delete</a></i></td>
                 </c:when>
                 <c:otherwise>
                    <td><a href="Product?action=activate&id=${product.id}&seller_id=${sessionScope.seller_id}"><i class="fa fa-toggle-on" aria-hidden="true"></i></td>
                 </c:otherwise>
              </c:choose>
           </tr>
        </c:forEach>
     </table>
  </body>
</html>
