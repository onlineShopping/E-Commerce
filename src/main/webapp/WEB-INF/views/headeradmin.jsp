<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OnlineShop</title>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/prettyPhoto.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/price-range.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/animate.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/main.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
    
    
  </head>
  <!--/head-->
  
  <body>
    <!--/header_top-->
    <!-- Just an image -->

    
      <!--/header_top-->
      <div class="header-top">
        <!--header-middle-->
        <div>
          <div class="row">
            <div class="col-sm-4">
              <div class="logo pull-left">
                <a href="#"><img src="${pageContext.request.contextPath}/resources/images/logotrans.png" alt="" style="    width: 278px;
    height: 198px;
    
    margin-top: -63px;"/></a>
              </div>
            </div>
            <div class="mainmenu pull-left" style="padding-top:16px;">
                <ul class="nav navbar-nav collapse navbar-collapse">
                  <li class="dropdown">
                    <a href="${pageContext.request.contextPath}/User/display">Users</a>
                    
                  </li>
                  
                  <li class="dropdown">
                    <a href="${pageContext.request.contextPath}/Category?action=getAll&isNeededAll=true">Category</a>
                    
                  </li>
                  <li class="dropdown">
                    <a href="${pageContext.request.contextPath}/SubCategory/getAll?isNeededAll=true">SubCategory</a>
                    
                  </li>
                  <li class="dropdown">
                    <a href="${pageContext.request.contextPath}/Product?action=getAll&isNeededAll=true">Products</a>
                    
                  </li>
                  <li class="dropdown" style="overflow:right;">
                    <a href="${pageContext.request.contextPath}/Logout" >Logout</a>
                    
                  </li>
                </ul>
              </div>
          </div>
        </div>
      </div>
      
      <!--/header-bottom-->
    </header>
    <!--/header-->
    
    
    
    <!--/Footer-->
</html>
